package snopes;

import java.util.ArrayList;

public class CategoryPage
{
	protected String url;
	protected String title;
	protected String fullTitle;
	protected String description;
	protected String imgURL;
	protected ArrayList<SubcategoryPage> subPages;
	protected ArrayList<RumorPage> rumorPages;
	
	public CategoryPage(String url, String title, String imgURL)
	{
		super();
		this.url = url;
		this.title = title;
		this.fullTitle = null;
		this.description = null;
		this.imgURL = imgURL;
		this.subPages = new ArrayList<SubcategoryPage>();
		this.rumorPages = new ArrayList<RumorPage>();
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public String getFullTitle()
	{
		return fullTitle;
	}

	public void setFullTitle(String fullTitle)
	{
		this.fullTitle = fullTitle;
	}

	public ArrayList<RumorPage> getRumorPages()
	{
		return rumorPages;
	}

	public void setRumorPages(ArrayList<RumorPage> rumorPages)
	{
		this.rumorPages = rumorPages;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getImgURL()
	{
		return imgURL;
	}

	public void setImgURL(String imgURL)
	{
		this.imgURL = imgURL;
	}

	public ArrayList<SubcategoryPage> getSubPages()
	{
		return subPages;
	}

	public void setSubPages(ArrayList<SubcategoryPage> subPages)
	{
		this.subPages = subPages;
	}

	public String toFileString()
	{

		return title + "\t" + fullTitle + "\t" + description + "\t" + url + "\t"+imgURL;
		
	}
	
	
	
}
