package snopes;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Scrape pages for individual rumors from Snopes.com
 * 
 * Start with the list of categories on the front page, then the subcategories
 * listed for each category.
 * 
 * There doesn't seem to be a third category, so assume links under
 * subcategories lead to individual rumors.
 * 
 * For each rumor, write out a file in the following format Line 1: category
 * name [tab] category description line 2: subcategory [tab] subcategory
 * description line 3: classification (if available, unknown if not) line 4...:
 * page HTML
 * 
 * @author Sam
 * 
 */

public class SnopeScraper
{
	//private static String outputDirectory = "C:/Users/Sam/workspace2/ScrapeSnopes/test_output";

	private static String outputDirectory = "/storage2/projects/snopes/pages";
	
	private static String mainURL = "http://www.snopes.com/";
	
	private static HashSet<String> truthImages = enumerateTruthImages();
	
	private static HashMap<String, Document> docCache;
	
	static String[] debugCats = {"Business"};
	static String[] debugSubs = {"Consumer Relations"};
	static boolean debug = false;
	static boolean subDebug = false;
	static int timeOutMS = 30000;

	public static void main(String[] args) throws Exception
	{
		docCache = new HashMap<String, Document>();
		
		System.out.println("Reading list of top-level categories from main page: " + mainURL);
		ArrayList<CategoryPage> cPages = extractCategoriesFromMainPage(mainURL);
		System.out.println(cPages.size() + " top-level categories found. Parsing them one by one.");
		System.out.println();
		
		
		ArrayList<RumorPage> rPages = new ArrayList<RumorPage>();
		for (CategoryPage cPage : cPages)
		{
			if (debug && !Arrays.asList(debugCats).contains(cPage.getTitle())) continue;
			
			System.out.println(cPage.getTitle()+": ");
			System.out.println("\tURL: "+cPage.getUrl());
			try
			{
				ArrayList<RumorPage> rs = processCategory(cPage);
				rPages.addAll(rs);
				System.out.println("\t" + rs.size() + " rumors extracted from this top-level category");
				System.out.println();
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				continue;
			}

		}
		
		System.out.println(rPages.size() + " rumor pages found total. Writing them all to " + outputDirectory + " using their urls as file names.");
		File dirFile = new File(outputDirectory);
		dirFile.mkdirs();
		for (RumorPage rPage : rPages)
		{
			writeRumorPageToFile(rPage, outputDirectory);
		}
		System.out.println("Done.");
	}

	/**
	 * There are two sets (old-style and newer) of images that Snopes uses to indicate whether a listed rumor is true or false. Return them in a set
	 * @return
	 */
	private static HashSet<String> enumerateTruthImages()
	{
		HashSet<String> rSet = new HashSet<String>();
		
		//Older style
		rSet.add("/common/red.gif");
		rSet.add("/common/green.gif");
		rSet.add("/common/multi.gif");
		rSet.add("/common/yellow.gif");
		rSet.add("/common/white.gif");
		
		//Newer style
		rSet.add("/images/green.gif");
		rSet.add("/images/mostlytrue.gif");
		rSet.add("/images/legend.gif");
		rSet.add("/images/red.gif");
		rSet.add("/images/yellow.gif");
		rSet.add("/images/mixture.gif");
		
		return rSet;
	}

	private static void writeRumorPageToFile(RumorPage rPage, String dir) throws IOException
	{
		String fileName = dir + "/" + rPage.getUrl().replace("/","_").replace(":","-");

		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		
		writer.write(rPage.getCategory().toFileString());
		writer.newLine();
		
		if (rPage.getSubCategory() != null)
			writer.write(rPage.getSubCategory().toFileString());
		else
			writer.write("");
		
		writer.newLine();
		writer.write(rPage.toFileString());
		writer.newLine();
		writer.write(rPage.getHtml());
		writer.close();
		
	}

	/**
	 * Look for rumor pages in a top-level category. If the category has subcategories, add them to the 
	 * CategoryPage object and look for rumors in them.
	 * 
	 * As examples, the "autos" category has subcategories (http://www.snopes.com/autos/autos.asp) while
	 * the "toxins" category does not (http://www.snopes.com/medical/toxins/toxins.asp)
	 * @param cPage
	 * @return
	 */
	private static ArrayList<RumorPage> processCategory(CategoryPage cPage) throws Exception
	{
		boolean hasTitle = augmentCategoryPage(cPage,"\t",true);
		ArrayList<RumorPage> rPages = new ArrayList<RumorPage>();
		if (hasTitle)
		{
			if (!hasRumorPages(cPage))
			{
				
				System.out.println("\tPage does not have rumor pages. Mining it for subcategories instead");
				ArrayList<SubcategoryPage> sPages = extractSubcategoryPagesFromCategoryPage(cPage);
				System.out.println("\tFound " + sPages.size() + " subcategories in this category");
				
				cPage.setSubPages(sPages);
				for (SubcategoryPage sPage : sPages)
				{
					if (subDebug && !Arrays.asList(debugSubs).contains(sPage.getTitle())) continue;

					System.out.println("\n\t\t"+sPage.getTitle()+":");
					System.out.println("\t\t"+sPage.getUrl());
					boolean validSub = augmentSubCategoryPage(sPage);
					try
					{
						ArrayList<RumorPage> foundPages = extractRumorPagesFromSubcategoryPage(cPage, sPage);
						rPages.addAll(foundPages);
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
						continue;
					}
				}
			}
			else
			{
				System.out.println("\tPage has rumor pages. Extracting these pages.");
				ArrayList<RumorPage> foundPages = extractRumorPagesFromCategoryPage(cPage);
				rPages.addAll(foundPages);
	
			}
			
		}
		else
		{
			System.out.println("\tNo title found. Page assumed to be stub or redirect. Not looking for rumors or subcategories.");
		}
		
		return rPages;
	}
	
	private static boolean augmentSubCategoryPage(SubcategoryPage sPage) throws Exception
	{
		return augmentCategoryPage(sPage, "\t\t",true);
		
	}

	/**
	 * Populate full title and description fields of CategoryPage by looking at its HTML. 
	 * Warning: category pages come in at least two styles:
	 * older: http://www.snopes.com/holidays/holidays.asp
	 * newer: http://www.snopes.com/food/food.asp
	 * @param cPage
	 * @param indent 
	 * @param verbose 
	 */
	private static boolean augmentCategoryPage(CategoryPage cPage, String indent, boolean verbose) throws Exception
	{
		Document doc;
		try
		{
			doc = getHTML(cPage.getUrl());
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			return false;
		}
		
		//Find full title by looking for <h1> or <font size=6 ... > tags 
		Element tElement = extractTitleElement(doc);
	
		if (tElement == null)
		{
			if (verbose) System.out.println(indent+"Could not augment category page)");
			return false;
		}
		boolean oldStyle = true;
		if (tElement.tagName().equals("h1"))
		{
			oldStyle = false;
			if (verbose) System.out.println(indent+"Category page is newer-style: " + tElement.tag());
		}
		else
			if (verbose) System.out.println(indent+"Category page is older-style: " + tElement.tag());
		
		cPage.setFullTitle(tElement.text());
		System.out.println(indent+"Full title: " + cPage.getFullTitle());
		
		//Find article description by looking for an element that is in the right place relative to the full title
		String description = "";
		if (oldStyle)
		{
			Element td = tElement.parent().parent();
			assert(td.tag().equals("td"));
			Elements fonts = td.getElementsByTag("font");
			fonts.retainAll(td.getElementsByAttributeValue("size", "3"));
			
			//Look for first font element following title element that does not have any font elements as children
			for (Element font :fonts)
			{
				if (font.getElementsByTag("font").size() == 1)
				{
					description = font.text();
					break;
				}
			}
		}
		else
		{
			Element dElement = tElement.nextElementSibling().nextElementSibling().child(0);
			if (dElement.getElementsByTag("div").size() > 0) description = dElement.text();
			cPage.setDescription(description);
		}
		if (verbose) System.out.println(indent+"Description: "+ sizeToFit(description,100) );
		
		return true;
	}

	private static Element extractTitleElement(Document doc)
	{
		Elements h1 = doc.getElementsByTag("h1");
		Elements largeFont = doc.getElementsByTag("font");
		largeFont.retainAll(doc.getElementsByAttributeValue("size", "6"));
		Element tElement= null; 
		
		if (h1.size() + largeFont.size() == 0)
		{
			System.out.println("\tWarning: missing title element");
			return null;
		}
		else if (h1.size() + largeFont.size() == 0)
		{
			System.out.println("\tWarning: too many title elements found");
			return null;

		}
		else
		{
			if (h1.size() == 1)
			{
				tElement = h1.first();
			}
			else
			{
				tElement = largeFont.first();
			}
			
		}
		return tElement;
	}

	private static String sizeToFit(String str, int i)
	{
		if (str.length() <= i)
			return str;
		else
		{
			String first = str.substring(0,i/2);
			String second = str.substring(str.length()-i/2,str.length());
			return first + " ... " + second;
		}
	}

	private static ArrayList<RumorPage> extractRumorPagesFromSubcategoryPage(CategoryPage cPage, SubcategoryPage sPage) throws Exception
	{
		ArrayList<RumorPage> rPages = new ArrayList<RumorPage>();
		
		CategoryPage page;
		if (sPage != null)
			page = sPage;
		else
			page = cPage;
		
		Document doc = getHTML(page.getUrl());
		//doc = removeStupidATagsFromDocument(doc);
		
		boolean olderStyle = isOlderStyle(doc);
		int end = page.getUrl().lastIndexOf('/')+1;
		if (olderStyle)
		{
			Element tElement = extractTitleElement(doc);
			Element td = findEnclosingTD(tElement);
			//The first table in the TD should be the ratings key
			Element table = findRumorTableWithinEnclosingTD(td);
			rPages = extractRumorPagesFromTable(table,cPage,sPage,olderStyle);
		}
		else
		{
			//In the case of rumor pages, we seem to be able to use the "article_text" div
			//as a guide to where the relevant content is
			
			Elements articleTexts = doc.getElementsByAttributeValue("class", "article_text");
			sassert(articleTexts.size() == 1);
			Element articleText = articleTexts.first();
			
			Element table = articleText.getElementsByTag("table").first();
			rPages = extractRumorPagesFromTable(table, cPage, sPage, olderStyle);
		}
		return rPages;
	}
	
	/**
	 * Once we have the <td> element that contains the main title, description,
	 * rating explanation box, and table of rumors, find the element that is the 
	 * table of rumors. This is nontrivial because the description may or may 
	 * not be enclosed in a table of its own.
	 * 
	 * So, we start with the second table in the collection, and then keep looking
	 * till we find one with as many images as links
	 * @param td
	 * @return
	 */
	private static Element findRumorTableWithinEnclosingTD(Element td) throws Exception
	{
		Elements tables = td.getElementsByTag("table");
		for (int i = 1; i < tables.size(); i++)
		{
			Elements imgs = tables.get(i).getElementsByTag("img");
			Elements as = tables.get(i).getElementsByTag("a");
			Elements hasHREFs = tables.get(i).getElementsByAttribute("href");
			as.retainAll(hasHREFs);
			
			//Make sure we can match each <img> up with one <a href ="">
			if (imgs.size() > 0 && imgs.size() == as.size())
			{
				return tables.get(i);
			}
			
		}
		
		throw new Exception("Could not find rumors table within enclosing <TD> element");

	}

	private static String removeStupidATagsFromHtml(String docHtml)
	{
		String fixedHtml = docHtml.replaceAll("<a name=\"[a-zA-Z ]+\">[ ]*(</a>)?", "");
		return fixedHtml;
	}

	private static ArrayList<RumorPage> extractRumorPagesFromTable(Element table, CategoryPage cPage, SubcategoryPage sPage, boolean olderStyle) throws Exception
	{
		ArrayList<RumorPage> rPages = new ArrayList<RumorPage>();
		CategoryPage page;
		if (sPage != null)
			page = sPage;
		else
			page = cPage;
		
		int end = page.getUrl().lastIndexOf('/')+1;
		//System.out.println(descriptions);
		//System.out.println(table.text());
		//Elements imgs = table.getElementsByTag("img");
		
		String[] htmlParts = table.html().split("(<br( /)?>)|(<p( /)?>)");

/*		try
		{
		sassert(imgs.size() <= htmlParts.length);
		}
		catch (Exception ex)
		{
			System.err.println(page.getUrl());
			System.err.println(imgs.size() + " vs " + htmlParts.length);
			System.err.println(table.html());
			throw ex;
		}*/


		
		for (int i = 0; i < htmlParts.length-1; i++)
		{
			Document lineDoc = Jsoup.parse(htmlParts[i]);
			Elements imgs = lineDoc.getElementsByTag("img");
			if (imgs.size() > 1)
			{
				throw new Exception("Found more than one image one a line");
			}
			else if (imgs.size() == 1)
			{
				//In new-style pages, the listing for a rumor is spread across
				//two or more lines
				StringBuilder fullHtmlBuilder = null;
				String nextLine = null;
				if (!olderStyle)
				{
					fullHtmlBuilder = new StringBuilder(htmlParts[i]);
					nextLine = htmlParts[i+1];
					
					while (!nextLine.contains("<img"))
					{
						fullHtmlBuilder.append(nextLine);
						i++;
						if (i >= htmlParts.length-1) break;
						nextLine = htmlParts[i+1];
					}
					lineDoc = Jsoup.parse(fullHtmlBuilder.toString());
				}
				Element img = imgs.get(0);
				String imgUrl = mainURL+img.attr("src");
				
		
				Elements as = lineDoc.getElementsByTag("a");
				Elements hrefs = lineDoc.getElementsByAttribute("href");
				as.retainAll(hrefs);
				
				if (as.size() == 0)
				{
					throw new Exception ("No link found for image");
				}
				else if (as.size() > 1)
				{
					throw new Exception("Too many links found for image");
				}
	
				 
				Element a = as.get(0);
		
				String url = page.getUrl().substring(0,end)+a.attr("href");
				String title = a.text();
				System.out.println("\t\t\t"+title+": " + url);
				
				String description = lineDoc.text();
				System.out.println("\t\t\t"+description);
				Document rDoc = getHTML(url);
				if (rDoc != null) rPages.add(new RumorPage(url,imgUrl,title,description,cPage,sPage,rDoc.html()));
				else System.out.println("\t\t\tCould not retrieve rumor page. Probably a redirect.");
				System.out.println();
		
			}

		}
		
		return rPages;
	}

	private static Element searchTreeForNextTag(Element img, String tag)
	{
		Element focus = img;
		int sibNum = focus.siblingIndex();
		
		while (focus != null)
		{
			Elements siblings = img.siblingElements();
			for (int i = sibNum+1 ; i < siblings.size(); i++)
			{
				Element sibling = siblings.get(i);
				Elements found = sibling.getElementsByTag(tag);
				if (found.size() > 0) return found.first();
			}
			focus = focus.parent();
			sibNum = focus.siblingIndex();
		}
		
		return null;
	}

	private static ArrayList<RumorPage> extractRumorPagesFromCategoryPage(CategoryPage cPage) throws Exception
	{
		return extractRumorPagesFromSubcategoryPage(cPage, null);
	}

	private static ArrayList<SubcategoryPage> extractSubcategoryPagesFromCategoryPage(CategoryPage cPage) throws Exception
	{
		ArrayList<SubcategoryPage> sPages = null;
		Document doc = getHTML(cPage.getUrl());
		
		boolean olderStyle = isOlderStyle(doc);
		Element tElement = extractTitleElement(doc);
		
		if (olderStyle)
		{
			//Find the TD that contains the title element. It should also contain the table of
			//subcategory links
			Element td = findEnclosingTD(tElement);
			//Then look for the first table element in the TD that doesn't contain another table
			Elements tables = td.getElementsByTag("table");
			for (Element table : tables)
			{
				if (table.getElementsByTag("img").size() > 0)
				{
					sPages = extractSubcategoryPagesFromTable(table, cPage);
					break;
				}
			}

			
		}
		else
		{
			//New articles have a <div> of class "article_text" that contains the relevant stuff
			Elements as = doc.getElementsByTag("a");
			as.retainAll(doc.getElementsByAttribute("name"));
			
			sassert(as.size() == 1);
			
			Element table = as.first().nextElementSibling().getElementsByTag("table").first();

			sPages = extractSubcategoryPagesFromTable(table,cPage);

		}
		
		//System.out.println(num);
		
		return sPages;
	}

	private static Element findEnclosingTD(Element tElement)
	{
		Element td = tElement.parent();
		while(!td.tagName().equals("td") && td != null)
		{
			td = td.parent();
			
		}
		return td;
	}

	private static ArrayList<SubcategoryPage> extractSubcategoryPagesFromTable(Element table, CategoryPage cPage) throws Exception
	{
		ArrayList<SubcategoryPage> sPages = new ArrayList<SubcategoryPage>();

		int end = cPage.getUrl().lastIndexOf('/')+1;
		Elements imgs = table.getElementsByTag("img");
		//TDs should come in pairs: one image, one link with a short description
		//sassert(tds.size() % 2 == 0);
		for (int i = 0; i < imgs.size(); i++)
		{
			try
			{
			String imgSrc = imgs.get(i).attr("src");

			Element td = imgs.get(i).parent().nextElementSibling();
			Element a = td.getElementsByTag("a").first();
			String url = cPage.getUrl().substring(0,end)+a.attr("href");
			String title = a.text();
			String shortDescription = td.ownText();
			//System.out.println("\t\t"+title);
			SubcategoryPage sPage =  new SubcategoryPage(url, title, imgSrc, shortDescription);
			sPages.add(sPage);
			
			
			}
			catch (NullPointerException ex)
			{
				//ex.printStackTrace();
				System.out.println(imgs.get(i).parent().html());
				throw ex;
			}
			
		}
		return sPages;
	}

	/**
	 * Look for title element. If it is a <h1>, then it is newer style. If it is <font size=6 ... >, then it is older style
	 * @param doc
	 * @return
	 */
	private static boolean isOlderStyle(Document doc)
	{
		Element tElement = extractTitleElement(doc);
		if (tElement.tagName().equals("h1"))
			return false;
		else 
			return true;
	}

	private static void sassert(boolean b) throws Exception
	{
		if (!b) throw new Exception("Sassert fail");
		
	}

	private static String getURLTitle(String url)
	{
		return url.substring(url.lastIndexOf('/')+1,url.lastIndexOf('.'));
	}

	/**
	 * Figure out whether a page is a list of rumors or a list of subcategories by looking for the images
	 * that snopes uses to indicate rumor truth or falsehood
	 * @param cPage
	 * @return
	 * @throws Exception
	 */
	private static boolean hasRumorPages(CategoryPage cPage) throws Exception
	{
		Document doc = getHTML(cPage.getUrl());
		
		boolean hasTruthImage = false;
		//System.out.println(cPage.getUrl());
		//System.out.println(doc.html());

		//System.out.println("----------------------------------------------------");
		//System.out.println("\t"+doc.getElementsByAttribute("SRC"));
		for (String imgSrc : truthImages)
		{
//			/System.out.println("\t"+imgSrc + ": "+ doc.getElementsByAttributeValue("src", imgSrc).size());
			
			if (doc.getElementsByAttributeValue("SRC", imgSrc).size() > 0)
				hasTruthImage = true;
		}
		
		return hasTruthImage;
	}

	/**
	 * Read categories from main Snopes page and return an arraylist of them
	 * @param url
	 * @return
	 * @throws Exception
	 */
	private static ArrayList<CategoryPage> extractCategoriesFromMainPage(String url) throws Exception
	{
		ArrayList<CategoryPage> pages = new ArrayList<CategoryPage>();
		Document doc = getHTML(url);
		
		//As of May 7th, 2014, category links are in the 11th <table> element on the page. 
		//There is one commented table that JSoup evidently ignores, so we look for the 10th
		Elements tables = doc.getElementsByTag("table");
		Element table = tables.get(9);
		//Every <a> element in the table should be a link to a category page. Every pair of two <a>'s links to the same
		//category page, the first as an image and the second as a text hyperlink
		Elements links = table.getElementsByTag("a");
		for (int i =0; i < links.size(); i +=2)
		{
			Element imgLink = links.get(i);
			Element txtLink = links.get(i+1);
			String pageURL = mainURL + imgLink.attr("HREF").substring(1);
			String imgURL = mainURL + imgLink.getElementsByTag("img").first().attr("src").substring(1);
			String title = txtLink.getElementsByTag("b").text().replaceAll("[\\s\\u00A0]+", " "); //For some dumb reason, JSoup outputs unicode chatacter #160 spaces in addition to normal ascii spaces
			pages.add(new CategoryPage(pageURL,title,imgURL));
		}
		
		
		return pages;
	}
	
	private static Document getHTML(String url) throws Exception
	{
		if (docCache.containsKey(url))
		{
			return docCache.get(url);
		}
		else
		{
			try
			{
				Connection connection = Jsoup.connect(url);
				connection.timeout(timeOutMS);
				Document doc = connection.get();
				docCache.put(url,doc);
				return doc;
			}
			catch (HttpStatusException ex)
			{
				return null;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
	}
}
