package snopes;

public class RumorPage
{
	private String imgUrl;
	private String title;
	private String description;
	private String html;
	private CategoryPage category;
	private SubcategoryPage subCategory;
	private String url;
	
	public RumorPage(String url, String imgUrl, String title, String description, CategoryPage category, SubcategoryPage subCategory,String html)
	{
		super();
		this.url = url;
		this.imgUrl = imgUrl;
		this.title = title;
		this.description = description;
		this.category = category;
		this.subCategory = subCategory;
		this.html = html;
	}

	public String getImgUrl()
	{
		return imgUrl;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}

	public void setImgUrl(String imgUrl)
	{
		this.imgUrl = imgUrl;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getHtml()
	{
		return html;
	}

	public void setHtml(String html)
	{
		this.html = html;
	}

	public CategoryPage getCategory()
	{
		return category;
	}

	public void setCategory(CategoryPage category)
	{
		this.category = category;
	}

	public SubcategoryPage getSubCategory()
	{
		return subCategory;
	}

	public void setSubCategory(SubcategoryPage subCategory)
	{
		this.subCategory = subCategory;
	}

	public String toFileString()
	{
		return title + "\t" + description + "\t"+url + "\t" + imgUrl;
	}
	
	
}
