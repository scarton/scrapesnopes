package snopes;

public class SubcategoryPage extends CategoryPage
{
	private String shortDescription;

	public SubcategoryPage(String url, String title, String imgURL, String shortDescription)
	{
		super(url, title, imgURL);
		this.shortDescription = shortDescription;
		
	}

	public String getShortDescription()
	{
		return shortDescription;
	}

	public void setShortDescription(String shortDescription)
	{
		this.shortDescription = shortDescription;
	}
	
	public String toFileString()
	{
		return title + "\t" +shortDescription + "\t" + fullTitle + "\t" + description + "\t" + url + "\t"+imgURL;

	}
	

}
