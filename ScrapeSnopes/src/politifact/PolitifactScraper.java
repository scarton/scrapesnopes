package politifact;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class PolitifactScraper
{
	public static String allStatementsPageURL = "http://www.politifact.com/truth-o-meter/statements/";
	public static String urlPrefix = "http://www.politifact.com";
	public static int numberOfListPages = 161; //You can see this by going to the url and looking at the bottom
	//private static String outputDirectory = "C:/Users/Sam/workspace2/ScrapeSnopes/test_output";
	private static String outputDirectory = "/storage2/projects/politifact/pages";
	static int timeOutMS = 30000;
	
	
	public static void main(String[] args) throws Exception
	{
		System.out.println("Scraping Politifact.com");
		ArrayList<PolitifactStatement> allStatements = new ArrayList<PolitifactStatement>();

		int count = 0;
		for (int i = 1; i <= numberOfListPages; i++)
		{
			try
			{
				
			
			String listPageURL = allStatementsPageURL + "?page="+i;
			System.out.println(i+": "+listPageURL);
			Document listDoc = getHTML(listPageURL);
			
			Elements statementEls = listDoc.getElementsByClass("scoretableStatement");
			System.out.println(statementEls.size() + " statements found on this page");
			
			for (Element statementEl : statementEls)
			{
				try
				{
					Element personEl = statementEl.getElementsByClass("quoteSource").get(0).getElementsByAttribute("href").get(0);
					
					String personName = personEl.text();
					String personURL = urlPrefix+personEl.attr("href");
					
					Element descriptionEl = statementEl.getElementsByTag("h2").get(0);
					String description = descriptionEl.text();
					
					Element meterEl = statementEl.getElementsByClass("meter").get(0);
					Element imageEl = meterEl.getElementsByTag("img").get(0);
					String imgURL = imageEl.attr("src");
					String truthRating = imageEl.attr("alt");
					
					Element articleEl = meterEl.getElementsByClass("quote").get(0).getElementsByTag("a").get(0);
					String articleURL = urlPrefix + articleEl.attr("href");
					String articleBlurb = articleEl.text();
					
					Document articleDoc = getHTML(articleURL);
					String articleHTML = articleDoc.html();
					
					PolitifactStatement pStatement = new PolitifactStatement(personName,personURL,description,imgURL,truthRating,articleURL,articleBlurb,articleHTML,listPageURL);
					System.out.println("\t"+count+": " + pStatement.toShortString());
					writeStatementToFile(outputDirectory, pStatement);
					count++;
				}
				catch (Exception ex)
				{
					ex.printStackTrace();
					System.err.println("Discarding single statement");
					continue;
				}
			}
			}
			catch (Exception ex)
			{
				ex.printStackTrace();
				System.err.println("Discarding whole listPage: " + i);
				continue;
			}
			
		}
		System.out.println("Done. " + count + " statements written to directory " + outputDirectory);

	}
	
	private static void writeStatementToFile(String dir, PolitifactStatement pStatement) throws Exception
	{
		String fileName = dir + "/" + pStatement.getArticleURL().replace("/","_").replace(":", ";");
		
//		/System.out.println(fileName);
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		writer.write(pStatement.toFileString());
		writer.close();
		
	}

	private static Document getHTML(String url) throws Exception
	{

		try
		{
			Connection connection = Jsoup.connect(url);
			connection.timeout(timeOutMS);
			Document doc = connection.get();
			return doc;
		}
		catch (HttpStatusException ex)
		{
			throw ex;
		}
		catch (Exception ex)
		{
			throw ex;
		}
	}

}
