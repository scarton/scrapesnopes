package politifact;

public class PolitifactStatement
{
	private String personName;
	private String personPageURL;
	private String statementDescription;
	private String truthImageURL;
	private String truthImageRating;
	private String articleURL;
	private String articleBlurb;
	private String articleHTML;
	private String linkingPageURL;
	


	public PolitifactStatement(String personName, String personPageURL, String statementDescription, String truthImageURL, String truthImageRating,
			String articleURL, String articleBlurb, String articleHTML, String linkingPageURL)
	{
		super();
		this.personName = personName;
		this.personPageURL = personPageURL;
		this.statementDescription = statementDescription;
		this.truthImageURL = truthImageURL;
		this.truthImageRating = truthImageRating;
		this.articleURL = articleURL;
		this.articleBlurb = articleBlurb;
		this.articleHTML = articleHTML;
		this.linkingPageURL = linkingPageURL;
	}

	public String getPersonName()
	{
		return personName;
	}

	public String getPersonPageURL()
	{
		return personPageURL;
	}

	public String getStatementDescription()
	{
		return statementDescription;
	}

	public String getTruthImageURL()
	{
		return truthImageURL;
	}

	public String getTruthImageRating()
	{
		return truthImageRating;
	}

	public String getArticleURL()
	{
		return articleURL;
	}

	public String getArticleBlurb()
	{
		return articleBlurb;
	}

	public String getArticleHTML()
	{
		return articleHTML;
	}

	public String toFileString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append(linkingPageURL);
		builder.append("\n");
		builder.append(personName+"\t"+personPageURL);
		builder.append("\n");
		builder.append(statementDescription);
		builder.append("\n");
		builder.append(truthImageRating + "\t" + truthImageURL);
		builder.append("\n");
		builder.append(articleBlurb + "\t"+articleURL);
		builder.append("\n");
		builder.append(articleHTML);
		return builder.toString();
	}

	public String toShortString()
	{
		return personName + " " + truthImageRating;
	}
	

	
}
