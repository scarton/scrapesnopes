Cokelore	Cokelore	More than a century after the creation of Coca-Cola, we're still as much in love with this most famous of soft drinks as our great-grandparents were. Hold up a Coke, and you proclaim all that's best about the American way of life: Coca-Cola is first dates and shy kisses, a platoon of war-weary GIs getting letters from home, and an old, rusted sign creaking in the wind outside the hometown diner. Coca-Cola is also one of the most successful companies the world has ever known; nothing can be that big and popular, so much a part of everyday life, without having legends spring up around it. Coca-Cola's uniquely influential position in our culture has led to a special set of legends we call 'Cokelore'; a collection of Coke trivia and tall tales sure to refresh even the most informationally-parched reader.	http://www.snopes.com/cokelore/cokelore.asp	http://www.snopes.com/graphics/icons/main/coke.gif

Have a Cloak and a Smile	FALSE Have a Cloak and a Smile Only two people in the company know Coca-Cola's formula, and each of them only knows half of it.	http://www.snopes.com/cokelore/formula.asp	http://www.snopes.com//images/red.gif
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <title>snopes.com: Coca-Cola's Secret Formula</title> 
  <link href="/style.css" rel="stylesheet" type="text/css" /> 
  <meta name="robots" content="INDEX,NOFOLLOW,NOARCHIVE" /> 
  <meta name="description" content="Is Coca-Cola's formula so much a secret that only two men each know half of it?" /> 
  <meta name="keywords" content="Coca-Cola, Coke, secret, formula, Pemberton, Robinson, Candler, Woodruff, urban legends archive, hoaxes, scams, rumors, urban myths, folklore, hoax slayer, mythbusters, misinformation, snopes" /> 
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> 
  <link rel="alternate" type="application/rss+xml" href="http://www.snopes.com/info/whatsnew.xml" title="RSS feed for snopes.com" /> 
  <link rel="image_src" href="http://www.snopes.com/logo-small.gif" /> 
  <script src="http://Q1MediaHydraPlatform.com/ads/video/unit_desktop_slider.php?eid=19503"></script>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script language="JavaScript">
if (window!=top){top.location.href=location.href;}
</script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40468225-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
 </head>     
 <body> 
  <script type="text/javascript">
<!--
var casaleD=new Date();var casaleR=(casaleD.getTime()%8673806982)+Math.random();
var casaleU=escape(window.location.href);
var casaleHost=' type="text/javascript" src="http://as.casalemedia.com/s?s=';
document.write('<scr'+'ipt'+casaleHost+'81847&amp;u=');
document.write(casaleU+'&amp;f=1&amp;id='+casaleR+'"><\/scr'+'ipt>');
//-->
</script> 
  <div class="siteWrapper"> 
   <table class="siteHeader" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="logo"><a href="http://www.snopes.com"><img src="/images/template/snopeslogo.gif" alt="snopes.com" /></a></td> 
      <td> 
       <table class="features"> 
        <tbody>
         <tr> 
          <td class="topAdvertisement"> 
           <center> 
            <iframe src="http://www.snopes.com/common/include/adsdaqbanner_a.asp" width="728" height="90" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" name="I2"></iframe> 
           </center> </td> 
         </tr> 
         <tr> 
          <td class="searchArea"> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <!--
                    <TD VALIGN=TOP><IMG SRC="/images/search-hdr.gif" ALT="Search"></TD>
                    <TD VALIGN=TOP>

                      <FORM METHOD="GET" ACTION="http://search.atomz.com/search/"><INPUT TYPE=TEXT WIDTH=50 SIZE=60 ID="searchbox" NAME="sp-q">
                    </TD>
                    <TD>
                      <INPUT TYPE=IMAGE SRC="/images/search-btn-go.gif">
                      <INPUT TYPE=HIDDEN NAME="sp-a" VALUE="00062d45-sp00000000">
                      <INPUT TYPE=HIDDEN NAME="sp-advanced" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-p" VALUE="all">
                      <INPUT TYPE=HIDDEN NAME="sp-w-control" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-w" VALUE="alike">
                      <INPUT TYPE=HIDDEN NAME="sp-date-range" VALUE=-1>
                      <INPUT TYPE=HIDDEN NAME="sp-x" VALUE="any">
                      <INPUT TYPE=HIDDEN NAME="sp-c" VALUE=100>
                      <INPUT TYPE=HIDDEN NAME="sp-m" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-s" VALUE=0>
                      </FORM>
-->  
             </tr> 
            </tbody>
           </table> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <table class="siteMenu" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td><a href="/info/whatsnew.asp" onmouseover="window.status='What\'s New';return true" onmouseout="window.status='';return true"><img src="/images/menu-whats-new.gif" alt="What's New" /></a><a href="/info/random/random.asp"><img src="/images/menu-randomizer.gif" alt="Randomizer" /></a><a href="/info/top25uls.asp"><img src="/images/menu-hot-25.gif" alt="Hot 25" /></a><a href="/info/faq.asp"><img src="/images/menu-faq.gif" alt="FAQ" /></a><a href="/daily/"><img src="/images/menu-odd-news.gif" alt="Odd News" /></a><a href="/info/glossary.asp"><img src="/images/menu-glossary.gif" alt="Glossary" /></a><a href="/info/newsletter.asp"><img src="/images/menu-newsletter.gif" alt="Newsletter" /></a><a href="http://message.snopes.com" target="message"><img src="/images/menu-message-board.gif" alt="Message Board" /></a></td> 
     </tr> 
    </tbody>
   </table> 
   <noindex> 
    <table class="siteSubMenu" cellpadding="0" cellspacing="0"> 
     <tbody>
      <tr>
       <td> </td>
       <td width="534" class="breadcrumbs"><a href="/snopes.asp">Home</a> &gt; <a href="/cokelore/cokelore.asp#formula">Cokelore</a>  &gt; Have a Cloak and a Smile</td> 
       <td width="420" class="links"><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Contact Us';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-contact-us.gif" alt="Contact Us" /></a><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Submit a Rumor';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-submit-rumor.gif" alt="Submit a Rumor" /></a><a href="http://www.snopes.com/cgi-bin/comments/photovideo.asp" onmouseover="window.status='Submit a Photo/Video';return true" onmouseout="window.status='';return true" target="photo"><img src="/images/sub-menu-submit-photo.gif" alt="Submit a Photo/Video" /></a> </td> 
      </tr> 
     </tbody>
    </table> 
   </noindex> 
   <table class="siteContent" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="contentArea"> 
       <table class="threeColumn" cellpadding="0" cellspacing="0"> 
        <tbody>
         <tr valign="TOP"> 
          <td class="navColumn"> <a href="https://twitter.com/snopes" class="twitter-follow-button" data-show-count="false">Follow @snopes</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script> <p></p> 
           <div class="navHeader">
            CATEGORIES:
           </div> 
           <ul> 
            <li><a href="/autos/autos.asp">Autos</a></li> 
            <li><a href="/business/business.asp">Business</a></li> 
            <li><a href="/cokelore/cokelore.asp">Cokelore</a></li> 
            <li><a href="/college/college.asp">College</a></li> 
            <li><a href="/computer/computer.asp">Computers</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/crime/crime.asp">Crime</a></li> 
            <li><a href="/critters/critters.asp">Critter Country</a></li> 
            <li><a href="/disney/disney.asp">Disney</a></li> 
            <li><a href="/embarrass/embarrass.asp">Embarrassments</a></li> 
            <li><a href="/photos/photos.asp">Fauxtography</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/food/food.asp">Food</a></li> 
            <li><a href="/fraud/fraud.asp">Fraud &amp; Scams</a></li> 
            <li><a href="/glurge/glurge.asp">Glurge Gallery</a></li> 
            <li><a href="/history/history.asp">History</a></li> 
            <li><a href="/holidays/holidays.asp">Holidays</a></li> 
            <li><a href="/horrors/horrors.asp">Horrors</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/humor/humor.asp">Humor</a></li> 
            <li><a href="/inboxer/inboxer.asp">Inboxer Rebellion</a></li> 
            <li><a href="/language/language.asp">Language</a></li> 
            <li><a href="/legal/legal.asp">Legal</a></li> 
            <li><a href="/lost/lost.asp">Lost Legends</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/love/love.asp">Love</a></li> 
            <li><a href="/luck/luck.asp">Luck</a></li> 
            <li><a href="/media/media.asp">Media Matters</a></li> 
            <li><a href="/medical/medical.asp">Medical</a></li> 
            <li><a href="/military/military.asp">Military</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/movies/movies.asp">Movies</a></li> 
            <li><a href="/music/music.asp">Music</a></li> 
            <li><a href="/oldwives/oldwives.asp">Old Wives' Tales</a></li> 
            <li><a href="/politics/politics.asp">Politics</a></li> 
            <li><a href="/pregnant/pregnant.asp">Pregnancy</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/quotes/quotes.asp">Quotes</a></li> 
            <li><a href="/racial/racial.asp">Racial Rumors</a></li> 
            <li><a href="/radiotv/radiotv.asp">Radio &amp; TV</a></li> 
            <li><a href="/religion/religion.asp">Religion</a></li> 
            <li><a href="/risque/risque.asp">Risqu&eacute; Business</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/science/science.asp">Science</a></li> 
            <li><a href="/rumors/rumors.asp">September 11</a></li> 
            <li><a href="/sports/sports.asp">Sports</a></li> 
            <li><a href="/travel/travel.asp">Travel</a></li> 
            <li><a href="/weddings/weddings.asp">Weddings</a></li> 
           </ul> </td> 
          <td class="contentColumn" align="LEFT"> <br /> <a href="mailto:Insert address(es) here?subject=Coca-Cola's Secret Formula&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/formula.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><img src="/images/envelope.gif" alt="E-mail this page" /></a> <a href="mailto:Insert address(es) here?subject=Coca-Cola's Secret Formula&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/formula.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><font size="2" color="#0000FF">E-mail this</font></a> 
           <!--
 &nbsp; &nbsp; &nbsp; &nbsp; <div class="fb-like" data-href="https://www.facebook.com/pages/snopescom/241061082705085" data-send="false" data-width="110" data-show-faces="false"></div>
--> <br /><br /> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <td valign="TOP"><img src="/images/search-hdr.gif" alt="Search" /></td> 
              <td valign="TOP"> 
               <form method="GET" action="http://search.atomz.com/search/">
                <input type="TEXT" width="50" size="60" id="searchbox" name="sp-q" /> 
               </form></td> 
              <td> <input type="IMAGE" src="/images/search-btn-go.gif" /> <input type="HIDDEN" name="sp-a" value="00062d45-sp00000000" /> <input type="HIDDEN" name="sp-advanced" value="1" /> <input type="HIDDEN" name="sp-p" value="all" /> <input type="HIDDEN" name="sp-w-control" value="1" /> <input type="HIDDEN" name="sp-w" value="alike" /> <input type="HIDDEN" name="sp-date-range" value="-1" /> <input type="HIDDEN" name="sp-x" value="any" /> <input type="HIDDEN" name="sp-c" value="100" /> <input type="HIDDEN" name="sp-m" value="1" /> <input type="HIDDEN" name="sp-s" value="0" />  </td> 
             </tr> 
            </tbody>
           </table> <br /><br /> 
           <div class="utilityLinks"> 
            <!-- code goes here --> 
            <div class="pw-widget pw-counter-vertical"> 
             <a class="pw-button-googleplus pw-look-native"></a> 
             <a class="pw-button-tumblr pw-look-native"></a> 
             <a class="pw-button-pinterest pw-look-native"></a> 
             <a class="pw-button-reddit pw-look-native"></a> 
             <a class="pw-button-email pw-look-native"></a> 
             <a class="pw-button-facebook pw-look-native"></a> 
             <a class="pw-button-twitter pw-look-native"></a> 
            </div> 
            <script src="http://i.po.st/static/v3/post-widget.js#publisherKey=snopescom1152" type="text/javascript"></script> 
            <!-- code goes here --> 
           </div> <br /> <h1>Have a Cloak and a Smile</h1> <style type="text/css">
<!--
 A:link, A:visited, A:active { text-decoration: underline; }
 A:link {color: #0000FF;}
 A:visited {color: #5fa505;}
 A:hover {color: #FF0000;} 
 .article_text {text-align: justify;} 
-->
</style> 
           <div class="article_text"> 
            <font color="#5FA505"><b>Claim:</b></font> &nbsp; Only two Coca-Cola executives know Coke's formula, and each of them knows only half of it. 
            <br />
            <br /> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <noindex>
             <table>
              <tbody>
               <tr>
                <td valign="CENTER"><img src="/images/red.gif" /></td>
                <td valign="TOP"><font size="5" color="#37628D"><b>FALSE</b></font></td>
               </tr>
              </tbody>
             </table>
            </noindex> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <br /> 
            <font color="#5FA505"><b>Origins:</b></font> &nbsp; For three quarters of a century, rumors about the measures the 
            <nobr>
             Coca-Cola
            </nobr> Company has employed to keep the formula of its flagship product a secret have been used to enhance consumer perception of 
            <nobr>
             Coca-Cola
            </nobr>'s specialness. The company has 
            <img height="123" width="177" alt="Coca-Cola sign" src="graphics/cokesign.jpg" align="LEFT" border="1" hspace="16" vspace="16" /> courted the media on this issue, establishing through repetition the belief that anything so closely guarded must be special indeed. 
            <br />
            <br /> Well, lollipops to that notion. What's special here isn't the formula; it's how the hulaballoo raised over it has been turned into yet another way to enhance the product's cachet. 
            <br />
            <br /> 
            <nobr>
             Coca-Cola
            </nobr> does have a rule about only two executives' being privy to the formula, but each of those men knows how to formulate the syrup independent of the other, not just half of an ingredients list. Perhaps this particular rumor about two executives, each knowing only part of the secret and thus incapable of concocting a batch of syrup on his own, results from confusion with the business practices of another Southern company famous for guarding its secrets, Kentucky Fried Chicken. (KFC's security measures include its secret blend of 
            <nobr>
             11 herbs
            </nobr> and spices being mixed at two different locations and combined at a third location.) 
            <br />
            <br /> Coca-Cola's two executives rule to the contrary, the whole notion is simply part of a media 
            <nobr>
             circus &#x2014; other
            </nobr> than for the publicity value, there's no need to go to any lengths to keep the Coke recipe secret. Anyone who could reproduce the drink couldn't market the product as 
            <nobr>
             Coca-Cola,
            </nobr> and without that brand name the beverage would be close to worthless. As the 
            <a href="/cokelore/newcoke.asp" onmouseover="window.status='Knew Coke';return true" onmouseout="window.status='';return true" target="newcoke">New Coke</a> fiasco proved, the public's devotion to 
            <nobr>
             Coca-Cola
            </nobr> has little to do with how it tastes. 
            <br />
            <br /> Moreover, at least one of the ingredients called for in the recipe would be next to impossible to secure in the U.S. (or to bring into the country): decocainized flavor 
            <table align="RIGHT" cellpadding="12" border="0">
             <tbody>
              <tr>
               <td> 
                <center> 
                 <script type="text/javascript"><!--
    kmn_size = '300x250';
    kmn_placement = '1c8834441f20e22a0aef009c095a6cab';
//--></script> 
                 <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
                </center> </td>
              </tr>
             </tbody>
            </table> essence of the coca leaf. As it now stands, only Stepan Co.'s New Jersey plant possesses the necessary DEA permit to import the leaves and remove the cocaine from them. Anyone looking to reproduce the drink would have to go to Stepan to get one of the key ingredients, and Stepan would refuse to sell to them. 
            <br />
            <br /> Okay, so keeping a tight lid on the recipe isn't so vitally important. Where, then, did all this tap dancing about a secret formula come from? 
            <br />
            <br /> Ernest Woodruff (he who was 
            <nobr>
             Coca-Cola
            </nobr> from 1916 through about 1931) reveled in the secrecy of the formula, knowing that making a big to-do about it would convince the 
            <nobr>
             media &#x2014; and
            </nobr> thus the general 
            <nobr>
             public &#x2014; that
            </nobr> they were getting something really special when they bought a Coke. In 1925, the only written copy of its formula 
            <nobr>
             Coca-Cola
            </nobr> admits to having was retrieved from a New York bank (where it had been held as collateral on a sugar loan) and reverently laid in safe deposit box in Woodruff's Atlanta bank, the Trust Company of Georgia (which later merged with Sun Bank of Florida, creating SunTrust Bank). 
            <br />
            <br /> But that was only the first step. That same year the company set a policy whereby no one could view the formula without written permission from the Board, and then only in the presence of the President, Chairman, or Corporate Secretary. Furthermore, the rule dictated that only two company officials would be allowed to know the recipe at any given time, and their identities were never to be disclosed for any reason. In keeping with the spirit of things, company policy was amended once air travel became the norm to preclude those two officers from ever flying on the same plane. 
            <br />
            <br /> 'Twas all smoke and mirrors, 
            <nobr>
             though &#x2014; even
            </nobr> as Woodruff's people were communicating these security measures to the media, the company employed at least four men who were known to be capable of producing Coke syrup in their sleep and a handful of others who were strongly rumored to have this knowledge. 
            <br />
            <br /> These days the 
            <nobr>
             Coca-Cola
            </nobr> Company is quite close-mouthed about who knows how to make the syrup that makes the world go around. It is reasonable to assume, however, that no matter what the publicly stated policy is, realities on the manufacturing floor regarding syrup production haven't changed from the Woodruff days, with a number of people in syrup production knowing the formula by heart. Official policy, after all, is for the 
            <nobr>
             media &#x2014; it's
            </nobr> not meant for everyday use. 
            <br />
            <br /> Throughout the years, a number of handwritten formulas have surfaced and have been presented to 
            <nobr>
             Coca-Cola.
            </nobr> The company routinely waves them off as &quot;not authentic,&quot; and that usually ends the matter. After all, no one can authenticate his tattered copy of the recipe for the Holy Grail of soft drinks unless 
            <nobr>
             Coca-Cola
            </nobr> shows theirs, and 
            <nobr>
             Coca-Cola
            </nobr> never does. 
            <br />
            <br /> Even so, that Holy Grail may already be in the public's hands. In 1993, Mark Pendergrast published what he believed to be Coke's original formula in 
            <i>For God, Country and Coca-Cola</i>. He'd come across the following among John Pemberton's papers: 
            <br />
            <br /> 
            <table width="300" align="CENTER" border="1" bordercolor="#CC0000" cellspacing="0">
             <tbody>
              <tr>
               <td bgcolor="#F8F8F8"> 
                <blockquote>
                 <font color="#000000" face="Verdana" size="2"> 
                  <ul type="Disc"> 
                   <li>Citrate Caffein, 1 oz.</li> 
                   <li>Ext. Vanilla, 1 oz.</li> 
                   <li>Flavoring, 2.5 oz.</li> 
                   <li>F.E. Coco, 4 oz.</li> 
                   <li>Citric Acid, 3 oz.</li> 
                   <li>Lime Juice, 1 Qt.</li> 
                   <li>Sugar, 30 lbs.</li> 
                   <li>Water, 2.5 Gal.</li> 
                   <li>Caramel sufficient</li> 
                  </ul> Mix Caffeine Acid and Lime Juice in 1 Qt Boiling water add vanilla and flavoring when cool. <br /> 
                  <ul type="Disc"> 
                   <u>Flavoring</u> 
                   <li>Oil Orange, 80</li> 
                   <li>Oil Lemon, 120</li> 
                   <li>Oil Nutmeg, 40</li> 
                   <li>Oil Cinnamon, 40</li> 
                   <li>Oil Coriander, 40</li> 
                   <li>Oil Neroli, 40</li> 
                   <li>Alcohol, 1 Qt.</li> 
                  </ul> Let stand 24 hours. </font>
                </blockquote> <p></p> </td>
              </tr>
             </tbody>
            </table> 
            <br /> ('F.E. Coco' stands for fluid extract of coca. The 
            <a href="/cokelore/bottle.asp" onmouseover="window.status='Design Err Shape';return true" onmouseout="window.status='';return true" target="bottle">cola</a> part of the product's name comes from the kola nut, an ingredient that appears on the above list as 'Citrate Caffein.') 
            <br />
            <br /> The 
            <nobr>
             Coca-Cola
            </nobr> Company was quick to label Pendergast's published find as &quot;not accurate&quot; and &quot;the latest in a long line of previous, unsuccessful attempts to reveal a 107-year-old mystery.&quot; Mind you, given how much 
            <nobr>
             Coca-Cola
            </nobr> has invested in proclaiming its formula to be a carefully-guarded secret, it is never expected to react in any other fashion even if it is someday handed the real formula. 
            <br />
            <br /> In a disingenuous way, even if the Pendergrast version were the original, 
            <nobr>
             Coca-Cola
            </nobr> would still be right about the &quot;not accurate&quot; part. Changes were made to the recipe between the time Pemberton marketed it in 1886 and Woodruff in the 1920s made it company canon that the formula would hereafter not be tinkered with: glycerin was added as a preservative, cocaine was eliminated, caffeine was greatly reduced, and citric acid was replaced with phosphoric acid, to name the changes we know about. Therefore, even if the Pendergrast version were dead on, it still would not be the formulation currently in use, because important changes were later made to it. (Visit our 
            <a href="/cokelore/newcoke.asp" onmouseover="window.status='Knew Coke';return true" onmouseout="window.status='';return true" target="newcoke">
             <nobr>
              Knew Coke
             </nobr></a> page to find out about what happened to the company the one time it broke Woodruff's &quot;no tinkering&quot; rule by reformulating the product as New Coke.) 
            <br />
            <br /> Is the Pendergrast version The Real Thing? Chances are it is (or perhaps, more accurately, it 
            <nobr>
             <u>was</u> &#x2014; remember
            </nobr> what we said about changes made to the formula decades later). It certainly fits with what lab analysis has for years been telling Coke-hunters had to be in the formula, and with what 
            <nobr>
             Coca-Cola
            </nobr> outsiders who claim to have seen or been told about the formula now remember of it. 
            <br />
            <br /> A bit of a stir was created in 1996 during the divorce proceedings of Frank and Patti Robinson. 
            <nobr>
             Mrs. Robinson
            </nobr> laid claim to several sheets of 
            <nobr>
             Mr. Robinson's
            </nobr> great-grandfather's handwriting, notes which might well have been a record of Pemberton's earliest attempts to come up with 
            <nobr>
             Coca-Cola.
            </nobr> These papers had been passed down through the Robinson family for generations. In 1997 these jotted notes were awarded to the husband, but the matter of who owns them is still very much in contention, as the matter is currently under appeal. 
            <br />
            <br /> This great-grandfather who did the scribbling was Frank Mason Robinson, who was a partner of pharmacist John Pemberton. Robinson named the drink in 1886 and wrote the famous label in flowing script. 
            <br />
            <br /> Barbara &quot;extract(ing) information&quot; Mikkelson 
            <br />
            <br /> 
            <font color="#5FA505"><b>Last updated:</b></font> &nbsp; 4 June 2011 
            <!--
18 November 1999 - original
13 March 2007 - reformatted
--> 
            <br />
            <br /> 
            <center>
             <font face="Verdana" size="2" color="#5FA505">Urban Legends Reference Pages &copy; 1995-2014 by snopes.com. <br /> This material may not be reproduced without permission. <br /> snopes and the snopes.com logo are registered service marks of snopes.com.</font>
            </center>
            <br /> 
           </div> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <table>
            <tbody>
             <tr>
              <td><img src="/images/template/icon-sources.gif" /></td>
              <td><h2>Sources:</h2></td>
             </tr>
            </tbody>
           </table> <font size="-1"> 
            <dl> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Allen, Frederick. &nbsp; 
               <i>Secret Formula.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; New York: HarperCollins, 1994. &nbsp; ISBN 0-88730-672-1 &nbsp; (pp. 162, 446).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Croft, Jay. &nbsp; &quot;Battle Goes on Over Coca-Cola Papers.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; 
               <i>The Atlanta Journal and Constitution.</i> &nbsp; 13 March 1997 &nbsp; (p. H12).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Miller, Michael. &nbsp; &quot;Things Go Better with Coca Extract.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; 
               <i>Rocky Mountain News.</i> &nbsp; 22 November 1994 &nbsp; (p. A28).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Pendergrast, Mark. &nbsp; 
               <i>For God, Country, and Coca-Cola.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; New York: Charles Scribner's Sons, 1993. &nbsp; ISBN 0-684-19347-7 &nbsp; (pp. 421-425).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Rochell, Anne. &nbsp; &quot;Has Writer Cracked Big Secret of Coke?&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; 
               <i>The Atlanta Journal and Constitution.</i> &nbsp; 25 April 1993 &nbsp; (p. H1).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Sisler, David. &nbsp; &quot;Revealing a Secret Truth That's Worth Knowing.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; 
               <i>The Augusta Chronicle.</i> &nbsp; 28 February 1998 &nbsp; (p. A4).
              </nobr>
             </dd> 
            </dl> </font> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <center>
            <center> 
             <script type="text/javascript">
<!--
google_ad_client = "pub-6608306193529351";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_type = "text";
google_ad_channel = "0054321535";
google_color_border = "000000";
//-->
</script> 
             <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            </center> 
           </center> </td> 
          <td class="adColumn"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '160x600';
    kmn_placement = 'f829c9263630394d4aa365109f5e0c0c';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <div class="siteBottom">
     &nbsp; 
   </div> 
  </div> 
  <div class="siteFooterGradient">
   &nbsp;
  </div>   
 </body>
</html>