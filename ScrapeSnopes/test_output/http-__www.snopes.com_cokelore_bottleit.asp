Cokelore	Cokelore	More than a century after the creation of Coca-Cola, we're still as much in love with this most famous of soft drinks as our great-grandparents were. Hold up a Coke, and you proclaim all that's best about the American way of life: Coca-Cola is first dates and shy kisses, a platoon of war-weary GIs getting letters from home, and an old, rusted sign creaking in the wind outside the hometown diner. Coca-Cola is also one of the most successful companies the world has ever known; nothing can be that big and popular, so much a part of everyday life, without having legends spring up around it. Coca-Cola's uniquely influential position in our culture has led to a special set of legends we call 'Cokelore'; a collection of Coke trivia and tall tales sure to refresh even the most informationally-parched reader.	http://www.snopes.com/cokelore/cokelore.asp	http://www.snopes.com/graphics/icons/main/coke.gif

Bottle It!	FALSE Bottle It! Coca-Cola came to be bottled when a stranger sold a two-word remarkable idea to the company: "Bottle it."	http://www.snopes.com/cokelore/bottleit.asp	http://www.snopes.com//images/red.gif
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <title>snopes.com: Coca-Cola in Bottles</title> 
  <link href="/style.css" rel="stylesheet" type="text/css" /> 
  <meta name="robots" content="INDEX,NOFOLLOW,NOARCHIVE" /> 
  <meta name="description" content="Did Coca-Cola come to be bottled only because a stranger sold that remarkable idea to the company?" /> 
  <meta name="keywords" content="diet coke, coke, coca cola, bottle, bottled, urban legends archive, hoaxes, scams, rumors, urban myths, folklore, hoax slayer, mythbusters, misinformation, snopes" /> 
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> 
  <link rel="alternate" type="application/rss+xml" href="http://www.snopes.com/info/whatsnew.xml" title="RSS feed for snopes.com" /> 
  <link rel="image_src" href="http://www.snopes.com/logo-small.gif" /> 
  <script src="http://Q1MediaHydraPlatform.com/ads/video/unit_desktop_slider.php?eid=19503"></script>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script language="JavaScript">
if (window!=top){top.location.href=location.href;}
</script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40468225-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
 </head>     
 <body> 
  <script type="text/javascript">
<!--
var casaleD=new Date();var casaleR=(casaleD.getTime()%8673806982)+Math.random();
var casaleU=escape(window.location.href);
var casaleHost=' type="text/javascript" src="http://as.casalemedia.com/s?s=';
document.write('<scr'+'ipt'+casaleHost+'81847&amp;u=');
document.write(casaleU+'&amp;f=1&amp;id='+casaleR+'"><\/scr'+'ipt>');
//-->
</script> 
  <div class="siteWrapper"> 
   <table class="siteHeader" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="logo"><a href="http://www.snopes.com"><img src="/images/template/snopeslogo.gif" alt="snopes.com" /></a></td> 
      <td> 
       <table class="features"> 
        <tbody>
         <tr> 
          <td class="topAdvertisement"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '728x90';
    kmn_placement = 'cad674db7f73589c9a110884ce73bb72';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
         <tr> 
          <td class="searchArea"> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <!--
                    <TD VALIGN=TOP><IMG SRC="/images/search-hdr.gif" ALT="Search"></TD>
                    <TD VALIGN=TOP>

                      <FORM METHOD="GET" ACTION="http://search.atomz.com/search/"><INPUT TYPE=TEXT WIDTH=50 SIZE=60 ID="searchbox" NAME="sp-q">
                    </TD>
                    <TD>
                      <INPUT TYPE=IMAGE SRC="/images/search-btn-go.gif">
                      <INPUT TYPE=HIDDEN NAME="sp-a" VALUE="00062d45-sp00000000">
                      <INPUT TYPE=HIDDEN NAME="sp-advanced" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-p" VALUE="all">
                      <INPUT TYPE=HIDDEN NAME="sp-w-control" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-w" VALUE="alike">
                      <INPUT TYPE=HIDDEN NAME="sp-date-range" VALUE=-1>
                      <INPUT TYPE=HIDDEN NAME="sp-x" VALUE="any">
                      <INPUT TYPE=HIDDEN NAME="sp-c" VALUE=100>
                      <INPUT TYPE=HIDDEN NAME="sp-m" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-s" VALUE=0>
                      </FORM>
-->  
             </tr> 
            </tbody>
           </table> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <table class="siteMenu" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td><a href="/info/whatsnew.asp" onmouseover="window.status='What\'s New';return true" onmouseout="window.status='';return true"><img src="/images/menu-whats-new.gif" alt="What's New" /></a><a href="/info/random/random.asp"><img src="/images/menu-randomizer.gif" alt="Randomizer" /></a><a href="/info/top25uls.asp"><img src="/images/menu-hot-25.gif" alt="Hot 25" /></a><a href="/info/faq.asp"><img src="/images/menu-faq.gif" alt="FAQ" /></a><a href="/daily/"><img src="/images/menu-odd-news.gif" alt="Odd News" /></a><a href="/info/glossary.asp"><img src="/images/menu-glossary.gif" alt="Glossary" /></a><a href="/info/newsletter.asp"><img src="/images/menu-newsletter.gif" alt="Newsletter" /></a><a href="http://message.snopes.com" target="message"><img src="/images/menu-message-board.gif" alt="Message Board" /></a></td> 
     </tr> 
    </tbody>
   </table> 
   <noindex> 
    <table class="siteSubMenu" cellpadding="0" cellspacing="0"> 
     <tbody>
      <tr>
       <td> </td>
       <td width="534" class="breadcrumbs"><a href="/snopes.asp">Home</a> &gt; <a href="/cokelore/cokelore.asp#bottleit">Cokelore</a>  &gt; Bottle It!</td> 
       <td width="420" class="links"><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Contact Us';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-contact-us.gif" alt="Contact Us" /></a><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Submit a Rumor';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-submit-rumor.gif" alt="Submit a Rumor" /></a><a href="http://www.snopes.com/cgi-bin/comments/photovideo.asp" onmouseover="window.status='Submit a Photo/Video';return true" onmouseout="window.status='';return true" target="photo"><img src="/images/sub-menu-submit-photo.gif" alt="Submit a Photo/Video" /></a> </td> 
      </tr> 
     </tbody>
    </table> 
   </noindex> 
   <table class="siteContent" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="contentArea"> 
       <table class="threeColumn" cellpadding="0" cellspacing="0"> 
        <tbody>
         <tr valign="TOP"> 
          <td class="navColumn"> <a href="https://twitter.com/snopes" class="twitter-follow-button" data-show-count="false">Follow @snopes</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script> <p></p> 
           <div class="navHeader">
            CATEGORIES:
           </div> 
           <ul> 
            <li><a href="/autos/autos.asp">Autos</a></li> 
            <li><a href="/business/business.asp">Business</a></li> 
            <li><a href="/cokelore/cokelore.asp">Cokelore</a></li> 
            <li><a href="/college/college.asp">College</a></li> 
            <li><a href="/computer/computer.asp">Computers</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/crime/crime.asp">Crime</a></li> 
            <li><a href="/critters/critters.asp">Critter Country</a></li> 
            <li><a href="/disney/disney.asp">Disney</a></li> 
            <li><a href="/embarrass/embarrass.asp">Embarrassments</a></li> 
            <li><a href="/photos/photos.asp">Fauxtography</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/food/food.asp">Food</a></li> 
            <li><a href="/fraud/fraud.asp">Fraud &amp; Scams</a></li> 
            <li><a href="/glurge/glurge.asp">Glurge Gallery</a></li> 
            <li><a href="/history/history.asp">History</a></li> 
            <li><a href="/holidays/holidays.asp">Holidays</a></li> 
            <li><a href="/horrors/horrors.asp">Horrors</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/humor/humor.asp">Humor</a></li> 
            <li><a href="/inboxer/inboxer.asp">Inboxer Rebellion</a></li> 
            <li><a href="/language/language.asp">Language</a></li> 
            <li><a href="/legal/legal.asp">Legal</a></li> 
            <li><a href="/lost/lost.asp">Lost Legends</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/love/love.asp">Love</a></li> 
            <li><a href="/luck/luck.asp">Luck</a></li> 
            <li><a href="/media/media.asp">Media Matters</a></li> 
            <li><a href="/medical/medical.asp">Medical</a></li> 
            <li><a href="/military/military.asp">Military</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/movies/movies.asp">Movies</a></li> 
            <li><a href="/music/music.asp">Music</a></li> 
            <li><a href="/oldwives/oldwives.asp">Old Wives' Tales</a></li> 
            <li><a href="/politics/politics.asp">Politics</a></li> 
            <li><a href="/pregnant/pregnant.asp">Pregnancy</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/quotes/quotes.asp">Quotes</a></li> 
            <li><a href="/racial/racial.asp">Racial Rumors</a></li> 
            <li><a href="/radiotv/radiotv.asp">Radio &amp; TV</a></li> 
            <li><a href="/religion/religion.asp">Religion</a></li> 
            <li><a href="/risque/risque.asp">Risqu&eacute; Business</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/science/science.asp">Science</a></li> 
            <li><a href="/rumors/rumors.asp">September 11</a></li> 
            <li><a href="/sports/sports.asp">Sports</a></li> 
            <li><a href="/travel/travel.asp">Travel</a></li> 
            <li><a href="/weddings/weddings.asp">Weddings</a></li> 
           </ul> </td> 
          <td class="contentColumn" align="LEFT"> <br /> <a href="mailto:Insert address(es) here?subject=Coca-Cola in Bottles&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/bottleit.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><img src="/images/envelope.gif" alt="E-mail this page" /></a> <a href="mailto:Insert address(es) here?subject=Coca-Cola in Bottles&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/bottleit.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><font size="2" color="#0000FF">E-mail this</font></a> 
           <!--
 &nbsp; &nbsp; &nbsp; &nbsp; <div class="fb-like" data-href="https://www.facebook.com/pages/snopescom/241061082705085" data-send="false" data-width="110" data-show-faces="false"></div>
--> <br /><br /> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <td valign="TOP"><img src="/images/search-hdr.gif" alt="Search" /></td> 
              <td valign="TOP"> 
               <form method="GET" action="http://search.atomz.com/search/">
                <input type="TEXT" width="50" size="60" id="searchbox" name="sp-q" /> 
               </form></td> 
              <td> <input type="IMAGE" src="/images/search-btn-go.gif" /> <input type="HIDDEN" name="sp-a" value="00062d45-sp00000000" /> <input type="HIDDEN" name="sp-advanced" value="1" /> <input type="HIDDEN" name="sp-p" value="all" /> <input type="HIDDEN" name="sp-w-control" value="1" /> <input type="HIDDEN" name="sp-w" value="alike" /> <input type="HIDDEN" name="sp-date-range" value="-1" /> <input type="HIDDEN" name="sp-x" value="any" /> <input type="HIDDEN" name="sp-c" value="100" /> <input type="HIDDEN" name="sp-m" value="1" /> <input type="HIDDEN" name="sp-s" value="0" />  </td> 
             </tr> 
            </tbody>
           </table> <br /><br /> 
           <div class="utilityLinks"> 
            <!-- code goes here --> 
            <div class="pw-widget pw-counter-vertical"> 
             <a class="pw-button-googleplus pw-look-native"></a> 
             <a class="pw-button-tumblr pw-look-native"></a> 
             <a class="pw-button-pinterest pw-look-native"></a> 
             <a class="pw-button-reddit pw-look-native"></a> 
             <a class="pw-button-email pw-look-native"></a> 
             <a class="pw-button-facebook pw-look-native"></a> 
             <a class="pw-button-twitter pw-look-native"></a> 
            </div> 
            <script src="http://i.po.st/static/v3/post-widget.js#publisherKey=snopescom1152" type="text/javascript"></script> 
            <!-- code goes here --> 
           </div> <br /> <h1>Bottle It!</h1> <style type="text/css">
<!--
 A:link, A:visited, A:active { text-decoration: underline; }
 A:link {color: #0000FF;}
 A:visited {color: #5fa505;}
 A:hover {color: #FF0000;} 
 .article_text {text-align: justify;} 
-->
</style> 
           <div class="article_text"> 
            <font color="#5FA505"><b>Claim:</b></font> &nbsp; Coca-Cola came to be bottled when a stranger sold a remarkable two-word idea to the company: &quot;Bottle it.&quot; 
            <br />
            <br /> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <noindex>
             <table>
              <tbody>
               <tr>
                <td valign="CENTER"><img src="/images/red.gif" /></td>
                <td valign="TOP"><font size="5" color="#37628D"><b>FALSE</b></font></td>
               </tr>
              </tbody>
             </table>
            </noindex> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <br /> 
            <font color="#5FA505"><b>Example:</b> &nbsp; <i>[de Vos, 1996]</i></font> 
            <br />
            <br /> 
            <div class="quoteBlock">
              Coke was first sold only at soda fountains. A man approached the executive of the company and told them that for $500 he would reveal the secret of untold corporate riches. His secret, after he pocketed the money, was &quot;Bottle it!&quot; 
            </div> 
            <div class="quoteBlockEnd">
              &nbsp; 
            </div> 
            <br /> 
            <font color="#5FA505"><b>Origins:</b></font> &nbsp; Of all the &quot;Low-level employee provides a simple yet brilliant 
            <a href="/business/genius/innovate.asp" target="innovate">innovation</a> that increases the profits of a successful company many times over&quot; legends, this 
            <img alt="Coke Bottle" title="Coke Bottle" height="114" width="65" src="graphics/handbottle.gif" align="LEFT" border="0" hspace="16" vspace="16" /> one about how Coca-Cola came to be served in bottles rather than solely dispensed from fountains is without question the best known. Akin to the &quot;carbonated by mistake&quot; legend, it postulates that at least some of Coca-Cola's remarkable success was the result of dumb luck rather than intelligent business design. Just as the &quot;
            <a href="/cokelore/carbonate.asp" onmouseover="window.status='Fortuitous Fizz';return true" onmouseout="window.status='';return true" target="carbonate">accidental carbonation</a>&quot; chestnut hypothesizes that if it hadn't been for an addlepated soda jerk the beverage would always have lacked fizz, this story about a stranger who fortuitously turned up with a million-dollar idea advances the notion that Coca-Cola wouldn't otherwise have been vended in bottles. 
            <br />
            <br /> While it is difficult to pin down precisely when the idea of selling Coke in this form was first kited, we can tell it occurred near the product's 1885 debut thanks to one of the participant's memories about a dispute over the notion. Sam Dobbs, nephew of Asa Candler (who bought up the rights to Coca-Cola in 1888), got himself in trouble with his uncle by secretly vending bottles of the soft drink syrup to entrepreneurs manning back-alley bottling machines. His uncle's objections were not based on a failure to comprehend the potential sales value of providing the buying public with the beverage in this fashion, but sprang from then-reasonable concerns about unsanitary bottling conditions adulterating the drink and injuring the product's standing. &quot;There are too many folks [bottlers] who are not responsible, who care nothing about the reputation of what they put up, and I am afraid the name [of Coca-Cola] will be 
            <table align="RIGHT" cellpadding="12" border="0">
             <tbody>
              <tr>
               <td> 
                <center> 
                 <script type="text/javascript"><!--
    kmn_size = '300x250';
    kmn_placement = '1c8834441f20e22a0aef009c095a6cab';
//--></script> 
                 <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
                </center> </td>
              </tr>
             </tbody>
            </table> injured,&quot; he said. 
            <br />
            <br /> Ergo, legend to the contrary, Asa Candler (who 
            <u>was</u> Coca-Cola back in those early days) gained the &quot;Bottle it&quot; advice not from a mysterious stranger who struck a hard bargain in exchange for the intelligence, but for free from his 
            <nobr>
             go-getter
            </nobr> of a teenage nephew. Candler chose not to immediately implement the suggestion, though, which perhaps contributes to belief in the legend. (The fact that Coca-Cola was not widely available in bottled form until well after it was established as a fountain drink does not reflect, as legend would have it, the Coca-Cola Company's lack of awareness of the potential market for bottled drinks, but rather Coca-Cola's recognition that the bottling process at that time was notoriously difficult, expensive, and unreliable, and that the fast-growing company had neither the money nor the physical resources to be establishing and operating bottling plants of its own.) 
            <br />
            <br /> Yet with or without Candler's blessing, Coca-Cola was being sold in bottles by the mid-1890s. Collectors of the product's memorabilia know to be on the lookout for the rare Hutchinson bottles the fledgling company used from that time to the early 1900s. (Between the discontinuation of the Hutchinson vessels and the introduction of the 
            <nobr>
             <a href="/cokelore/bottle.asp" onmouseover="window.status='Design Err Shape';return true" onmouseout="window.status='';return true" target="bottle">contour bottle</a>
            </nobr> in 1915, a variety of straight-sided bottles were used.) The stoppered Hutchinson bottles were exceedingly difficult to clean (in those days, bottles were washed and 
            <nobr>
             re-used
            </nobr> rather than used once and discarded), an additional fact that serves to make Candler's reluctance to bottle the product a bit more understandable. 
            <br />
            <br /> Joe Biedenharn of Vicksburg, Mississippi, was one of the soda's earliest bottlers. He went from peddling Coke syrup to druggists for the company to, in 1894, bottling and then selling the carbonated beverage itself. Another early bottler was the Valdosta Bottling Works of Valdosta, Georgia, which began putting up Coke in bottles in 1897. 
            <br />
            <br /> The legend attached to how Coca-Cola came to be bottled is indeed an old one. In the 1934 film 
            <a href="http://us.imdb.com/title/tt0025301/" onmouseover="window.status='Imitation of Life';return true" onmouseout="window.status='';return true" target="imitate"><i>Imitation of Life</i></a>, a bum Beatrice 'Bea' Pullman (Claudette Colbert) feeds a free breakfast of pancakes announces by way of negotiating another helping that he will tell Beatrice &quot;in two words how to make a million dollars.&quot; Before making his simple-yet-brilliant suggestion about a way to transform her from a mere cafe owner into a pancake mogul, he regales her with the tale of an unnamed fellow who once approached the president of Coca-Cola with a similar proposition, saying, &quot;For $100,000, I'll tell you how to make millions.&quot; But of course the advice of the unnamed fellow was &quot;Bottle it.&quot; 
            <br />
            <br /> The legend didn't begin with that acclaimed film, though, because mention of it appears in a 1935 note penned by 
            <i>New Yorker</i> editor Harold Ross. Ross was in the habit of typing out idea sheets (story suggestions) for potential &quot;Talk of the Town&quot; pieces, and his 
            <nobr>
             23 September
            </nobr> 1935 entry reads as follows: 
            <br /> 
            <font face="Verdana" size="2"> 
             <div style="text-align: justify; margin-top: 20px; margin-bottom: 20px; margin-left: 40px; margin-right: 40px">
               There's a legend that a man once went into the Coca-Cola offices and said he had an idea which would make millions for them and would reveal it on payment of $50,000. After a solemn agreement had been drawn up he gave them the idea in two words: &quot;Bottle it.&quot; I've heard this legend for years. Any truth in it? There are a lot of other legends of the kind that could be bunched with this one, if only to explode them all. 
             </div> </font> Notice that even in 1935 Ross refers to the story as a legend he'd been hearing for years, so while that note was dated a year after the release of 
            <i>Imitation of Life</i>, the knowledge behind it predates the film. (It also evinces a surely modern version of the legend: In 1896 the Coca-Cola Company's entire surplus amounted to less than $50,000 
            <nobr>
              &#x2014; to
            </nobr> have thrown all that cash at a stranger in exchange for nothing more than the nebulous promise of a &quot;great idea&quot; would have been madness indeed.) 
            <br />
            <br /> It may never prove possible to isolate the origin of this well-traveled innovation tale, but we can say with great assurance that it entered the canon of popular lore before the majority of our readers came into the world. 
            <br />
            <br /> Barbara &quot;we're the young whippersnappers, not it&quot; Mikkelson 
            <br />
            <br /> 
            <font color="#5FA505"><b>Last updated:</b></font> &nbsp; 20 September 2013 
            <!--
10 October 2005 - original
21 April 2011: reformatted
--> 
            <br />
            <br /> 
            <center>
             <font face="Verdana" size="2" color="#5FA505">Urban Legends Reference Pages &copy; 1995-2014 by snopes.com. <br /> This material may not be reproduced without permission. <br /> snopes and the snopes.com logo are registered service marks of snopes.com.</font>
            </center>
            <br /> 
           </div> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <table>
            <tbody>
             <tr>
              <td><img src="/images/template/icon-sources.gif" /></td>
              <td><h2>Sources:</h2></td>
             </tr>
            </tbody>
           </table> <font size="-1"> 
            <dl> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Allen, Frederick. &nbsp; 
               <i>Secret Formula.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; New York: HarperCollins, 1994. &nbsp; ISBN 0-88730-672-1 &nbsp; (pp. 69-70).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; de Vos, Gail. &nbsp; 
               <i>Tales, Rumors and Gossip</i>.
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; Englewood: Libraries Unlimited, 1996. &nbsp; ISBN 1-56308-190-3 &nbsp; (p. 142).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Pendergrast, Mark. &nbsp; 
               <i>For God, Country, and Coca-Cola.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; New York: Charles Scribner's Sons, 1993. &nbsp; ISBN 0-684-19347-7 &nbsp; (p. 73)
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Yagoda, Ben. &nbsp; 
               <i> About Town</i>.
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; New York: Scribner, 2000. &nbsp; ISBN 0-684-81605-9 &nbsp; (p. 132).
              </nobr>
             </dd> 
            </dl> </font> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <center>
            <center> 
             <script type="text/javascript">
<!--
google_ad_client = "pub-6608306193529351";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_type = "text";
google_ad_channel = "0054321535";
google_color_border = "000000";
//-->
</script> 
             <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            </center> 
           </center> </td> 
          <td class="adColumn"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '160x600';
    kmn_placement = 'f829c9263630394d4aa365109f5e0c0c';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <div class="siteBottom">
     &nbsp; 
   </div> 
  </div> 
  <div class="siteFooterGradient">
   &nbsp;
  </div>   
 </body>
</html>