Cokelore	Cokelore	More than a century after the creation of Coca-Cola, we're still as much in love with this most famous of soft drinks as our great-grandparents were. Hold up a Coke, and you proclaim all that's best about the American way of life: Coca-Cola is first dates and shy kisses, a platoon of war-weary GIs getting letters from home, and an old, rusted sign creaking in the wind outside the hometown diner. Coca-Cola is also one of the most successful companies the world has ever known; nothing can be that big and popular, so much a part of everyday life, without having legends spring up around it. Coca-Cola's uniquely influential position in our culture has led to a special set of legends we call 'Cokelore'; a collection of Coke trivia and tall tales sure to refresh even the most informationally-parched reader.	http://www.snopes.com/cokelore/cokelore.asp	http://www.snopes.com/graphics/icons/main/coke.gif

Derisive Snort	FALSE Derisive Snort The cursive script of the Coca-Cola label includes an image of a person snorting cocaine.	http://www.snopes.com/cokelore/snort.asp	http://www.snopes.com//images/red.gif
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <title>snopes.com: Coca-Cola Cocaine Snorting Logo</title> 
  <link href="/style.css" rel="stylesheet" type="text/css" /> 
  <meta name="robots" content="INDEX,NOFOLLOW,NOARCHIVE" /> 
  <meta name="description" content="Does Coca-Cola's script label show a person snorting cocaine?" /> 
  <meta name="keywords" content="Coca-Cola, Coke, cocaine, label, logo, snorting, urban legends archive, hoaxes, scams, rumors, urban myths, folklore, hoax slayer, mythbusters, misinformation, snopes" /> 
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> 
  <link rel="alternate" type="application/rss+xml" href="http://www.snopes.com/info/whatsnew.xml" title="RSS feed for snopes.com" /> 
  <link rel="image_src" href="http://www.snopes.com/logo-small.gif" /> 
  <script src="http://Q1MediaHydraPlatform.com/ads/video/unit_desktop_slider.php?eid=19503"></script>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script language="JavaScript">
if (window!=top){top.location.href=location.href;}
</script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40468225-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
 </head>     
 <body> 
  <script type="text/javascript">
<!--
var casaleD=new Date();var casaleR=(casaleD.getTime()%8673806982)+Math.random();
var casaleU=escape(window.location.href);
var casaleHost=' type="text/javascript" src="http://as.casalemedia.com/s?s=';
document.write('<scr'+'ipt'+casaleHost+'81847&amp;u=');
document.write(casaleU+'&amp;f=1&amp;id='+casaleR+'"><\/scr'+'ipt>');
//-->
</script> 
  <div class="siteWrapper"> 
   <table class="siteHeader" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="logo"><a href="http://www.snopes.com"><img src="/images/template/snopeslogo.gif" alt="snopes.com" /></a></td> 
      <td> 
       <table class="features"> 
        <tbody>
         <tr> 
          <td class="topAdvertisement"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '728x90';
    kmn_placement = 'cad674db7f73589c9a110884ce73bb72';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
         <tr> 
          <td class="searchArea"> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <!--
                    <TD VALIGN=TOP><IMG SRC="/images/search-hdr.gif" ALT="Search"></TD>
                    <TD VALIGN=TOP>

                      <FORM METHOD="GET" ACTION="http://search.atomz.com/search/"><INPUT TYPE=TEXT WIDTH=50 SIZE=60 ID="searchbox" NAME="sp-q">
                    </TD>
                    <TD>
                      <INPUT TYPE=IMAGE SRC="/images/search-btn-go.gif">
                      <INPUT TYPE=HIDDEN NAME="sp-a" VALUE="00062d45-sp00000000">
                      <INPUT TYPE=HIDDEN NAME="sp-advanced" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-p" VALUE="all">
                      <INPUT TYPE=HIDDEN NAME="sp-w-control" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-w" VALUE="alike">
                      <INPUT TYPE=HIDDEN NAME="sp-date-range" VALUE=-1>
                      <INPUT TYPE=HIDDEN NAME="sp-x" VALUE="any">
                      <INPUT TYPE=HIDDEN NAME="sp-c" VALUE=100>
                      <INPUT TYPE=HIDDEN NAME="sp-m" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-s" VALUE=0>
                      </FORM>
-->  
             </tr> 
            </tbody>
           </table> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <table class="siteMenu" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td><a href="/info/whatsnew.asp" onmouseover="window.status='What\'s New';return true" onmouseout="window.status='';return true"><img src="/images/menu-whats-new.gif" alt="What's New" /></a><a href="/info/random/random.asp"><img src="/images/menu-randomizer.gif" alt="Randomizer" /></a><a href="/info/top25uls.asp"><img src="/images/menu-hot-25.gif" alt="Hot 25" /></a><a href="/info/faq.asp"><img src="/images/menu-faq.gif" alt="FAQ" /></a><a href="/daily/"><img src="/images/menu-odd-news.gif" alt="Odd News" /></a><a href="/info/glossary.asp"><img src="/images/menu-glossary.gif" alt="Glossary" /></a><a href="/info/newsletter.asp"><img src="/images/menu-newsletter.gif" alt="Newsletter" /></a><a href="http://message.snopes.com" target="message"><img src="/images/menu-message-board.gif" alt="Message Board" /></a></td> 
     </tr> 
    </tbody>
   </table> 
   <noindex> 
    <table class="siteSubMenu" cellpadding="0" cellspacing="0"> 
     <tbody>
      <tr>
       <td> </td>
       <td width="534" class="breadcrumbs"><a href="/snopes.asp">Home</a> &gt; <a href="/cokelore/cokelore.asp#snort">Cokelore</a> &gt; Derisive Snort</td> 
       <td width="420" class="links"><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Contact Us';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-contact-us.gif" alt="Contact Us" /></a><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Submit a Rumor';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-submit-rumor.gif" alt="Submit a Rumor" /></a><a href="http://www.snopes.com/cgi-bin/comments/photovideo.asp" onmouseover="window.status='Submit a Photo/Video';return true" onmouseout="window.status='';return true" target="photo"><img src="/images/sub-menu-submit-photo.gif" alt="Submit a Photo/Video" /></a> </td> 
      </tr> 
     </tbody>
    </table> 
   </noindex> 
   <table class="siteContent" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="contentArea"> 
       <table class="threeColumn" cellpadding="0" cellspacing="0"> 
        <tbody>
         <tr valign="TOP"> 
          <td class="navColumn"> <a href="https://twitter.com/snopes" class="twitter-follow-button" data-show-count="false">Follow @snopes</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script> <p></p> 
           <div class="navHeader">
            CATEGORIES:
           </div> 
           <ul> 
            <li><a href="/autos/autos.asp">Autos</a></li> 
            <li><a href="/business/business.asp">Business</a></li> 
            <li><a href="/cokelore/cokelore.asp">Cokelore</a></li> 
            <li><a href="/college/college.asp">College</a></li> 
            <li><a href="/computer/computer.asp">Computers</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/crime/crime.asp">Crime</a></li> 
            <li><a href="/critters/critters.asp">Critter Country</a></li> 
            <li><a href="/disney/disney.asp">Disney</a></li> 
            <li><a href="/embarrass/embarrass.asp">Embarrassments</a></li> 
            <li><a href="/photos/photos.asp">Fauxtography</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/food/food.asp">Food</a></li> 
            <li><a href="/fraud/fraud.asp">Fraud &amp; Scams</a></li> 
            <li><a href="/glurge/glurge.asp">Glurge Gallery</a></li> 
            <li><a href="/history/history.asp">History</a></li> 
            <li><a href="/holidays/holidays.asp">Holidays</a></li> 
            <li><a href="/horrors/horrors.asp">Horrors</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/humor/humor.asp">Humor</a></li> 
            <li><a href="/inboxer/inboxer.asp">Inboxer Rebellion</a></li> 
            <li><a href="/language/language.asp">Language</a></li> 
            <li><a href="/legal/legal.asp">Legal</a></li> 
            <li><a href="/lost/lost.asp">Lost Legends</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/love/love.asp">Love</a></li> 
            <li><a href="/luck/luck.asp">Luck</a></li> 
            <li><a href="/media/media.asp">Media Matters</a></li> 
            <li><a href="/medical/medical.asp">Medical</a></li> 
            <li><a href="/military/military.asp">Military</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/movies/movies.asp">Movies</a></li> 
            <li><a href="/music/music.asp">Music</a></li> 
            <li><a href="/oldwives/oldwives.asp">Old Wives' Tales</a></li> 
            <li><a href="/politics/politics.asp">Politics</a></li> 
            <li><a href="/pregnant/pregnant.asp">Pregnancy</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/quotes/quotes.asp">Quotes</a></li> 
            <li><a href="/racial/racial.asp">Racial Rumors</a></li> 
            <li><a href="/radiotv/radiotv.asp">Radio &amp; TV</a></li> 
            <li><a href="/religion/religion.asp">Religion</a></li> 
            <li><a href="/risque/risque.asp">Risqu&eacute; Business</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/science/science.asp">Science</a></li> 
            <li><a href="/rumors/rumors.asp">September 11</a></li> 
            <li><a href="/sports/sports.asp">Sports</a></li> 
            <li><a href="/travel/travel.asp">Travel</a></li> 
            <li><a href="/weddings/weddings.asp">Weddings</a></li> 
           </ul> </td> 
          <td class="contentColumn" align="LEFT"> <br /> <a href="mailto:Insert address(es) here?subject=Coca-Cola Cocaine Snorting Logo&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/snort.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><img src="/images/envelope.gif" alt="E-mail this page" /></a> <a href="mailto:Insert address(es) here?subject=Coca-Cola Cocaine Snorting Logo&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/snort.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><font size="2" color="#0000FF">E-mail this</font></a> 
           <!--
 &nbsp; &nbsp; &nbsp; &nbsp; <div class="fb-like" data-href="https://www.facebook.com/pages/snopescom/241061082705085" data-send="false" data-width="110" data-show-faces="false"></div>
--> <br /><br /> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <td valign="TOP"><img src="/images/search-hdr.gif" alt="Search" /></td> 
              <td valign="TOP"> 
               <form method="GET" action="http://search.atomz.com/search/">
                <input type="TEXT" width="50" size="60" id="searchbox" name="sp-q" /> 
               </form></td> 
              <td> <input type="IMAGE" src="/images/search-btn-go.gif" /> <input type="HIDDEN" name="sp-a" value="00062d45-sp00000000" /> <input type="HIDDEN" name="sp-advanced" value="1" /> <input type="HIDDEN" name="sp-p" value="all" /> <input type="HIDDEN" name="sp-w-control" value="1" /> <input type="HIDDEN" name="sp-w" value="alike" /> <input type="HIDDEN" name="sp-date-range" value="-1" /> <input type="HIDDEN" name="sp-x" value="any" /> <input type="HIDDEN" name="sp-c" value="100" /> <input type="HIDDEN" name="sp-m" value="1" /> <input type="HIDDEN" name="sp-s" value="0" />  </td> 
             </tr> 
            </tbody>
           </table> <br /><br /> 
           <div class="utilityLinks"> 
            <!-- code goes here --> 
            <div class="pw-widget pw-counter-vertical"> 
             <a class="pw-button-googleplus pw-look-native"></a> 
             <a class="pw-button-tumblr pw-look-native"></a> 
             <a class="pw-button-pinterest pw-look-native"></a> 
             <a class="pw-button-reddit pw-look-native"></a> 
             <a class="pw-button-email pw-look-native"></a> 
             <a class="pw-button-facebook pw-look-native"></a> 
             <a class="pw-button-twitter pw-look-native"></a> 
            </div> 
            <script src="http://i.po.st/static/v3/post-widget.js#publisherKey=snopescom1152" type="text/javascript"></script> 
            <!-- code goes here --> 
           </div> <br /> <h1>Derisive Snort</h1> <style type="text/css">
<!--
 A:link, A:visited, A:active { text-decoration: underline; }
 A:link {color: #0000FF;}
 A:visited {color: #5fa505;}
 A:hover {color: #FF0000;} 
 .article_text {text-align: justify;} 
-->
</style> 
           <div class="article_text"> 
            <font color="#5FA505"><b>Claim:</b></font> &nbsp; When tipped on its side, Coca-Cola's distinctive script logo shows a figure snorting a line of cocaine, and this image was deliberately placed there by the artist as a sly reference to the product's containing cocaine. 
            <br />
            <br /> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <noindex>
             <table>
              <tbody>
               <tr>
                <td valign="CENTER"><img src="/images/red.gif" /></td>
                <td valign="TOP"><font size="5" color="#37628D"><b>FALSE</b></font></td>
               </tr>
              </tbody>
             </table>
            </noindex> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <br /> 
            <font color="#5FA505"><b>Origins:</b></font> &nbsp; Anything tipped on its side or viewed by hanging upside down like a bat can eventually appear to look like something other than what it is. Sailing ships and romping kittens are routinely seen in clouds that scud by, but these sights are 
            <img src="graphics/snort.jpg" align="LEFT" hspace="16" vspace="16" alt="Snort" width="200" height="263" border="1" /> attributed to the active imaginations of the cloud-watchers, not laid at the feet of a mythical artist who deliberately hid them there. The same should be said of images &quot;found&quot; on product labels 
            <nobr>
             &#x2014; they
            </nobr> say more about the imagination of the viewer than they do about what the company put there. Yet mistrust of corporations runs so high that the reasonable explanation of &quot;I'm just seeing that&quot; is quickly brushed aside in favor of the &quot;I found the secret message!&quot; one. 
            <br />
            <br /> 
            <nobr>
             Coca-Cola's
            </nobr> distinctive script logo was created by Frank Mason Robinson in 1886 when he wrote the first 
            <nobr>
             Coca-Cola
            </nobr> label in flowing Spencerian script. Robinson was a partner with pharmacist John Pemberton (who made the first 
            <nobr>
             Coca-Cola
            </nobr> syrup), and it was Robinson who gave the beverage its name. Robinson created the logo in 1886, at a time when 
            <a href="/cokelore/cocaine.asp" onmouseover="window.status='Cocaine-Cola';return true" onmouseout="window.status='';return true" target="cocaine">cocaine</a> was readily available in all manner of over-the-counter products. Unlike these days, however, when we think of cocaine as coming in the form of a fine, white powder that has to be snorted through a $100 bill, consumers of the 1880s knew the drug as a liquid. No one snorted cocaine in those days; they drank it. The idea that an artist would in 1886 incorporate into the logo a visual representation of a drug behavior that didn't then exist is a bit much to swallow. 
            <br />
            <br /> Barbara &quot;a nose for these things&quot; Mikkelson 
            <br />
            <br /> 
            <font color="#5FA505"><b>Last updated:</b></font> &nbsp; 8 June 2011 
            <!--
16 November 1999 - original
13 March 2007 - reformatted
8 June 2011
--> 
            <br />
            <br /> 
            <center>
             <font face="Verdana" size="2" color="#5FA505">Urban Legends Reference Pages &copy; 1995-2014 by snopes.com. <br /> This material may not be reproduced without permission. <br /> snopes and the snopes.com logo are registered service marks of snopes.com.</font>
            </center>
            <br /> 
           </div> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <table>
            <tbody>
             <tr>
              <td><img src="/images/template/icon-sources.gif" /></td>
              <td><h2>Sources:</h2></td>
             </tr>
            </tbody>
           </table> <font size="-1"> 
            <dl> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Allen, Frederick. &nbsp; 
               <i>Secret Formula.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; New York: HarperCollins, 1994. &nbsp; ISBN 0-88730-672-1.
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Pendergrast, Mark. &nbsp; 
               <i>For God, Country, and Coca-Cola.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; New York: Charles Scribner's Sons, 1993. &nbsp; ISBN 0-684-19347-7.
              </nobr>
             </dd> 
            </dl> </font> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <center>
            <center> 
             <script type="text/javascript">
<!--
google_ad_client = "pub-6608306193529351";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_type = "text";
google_ad_channel = "0054321535";
google_color_border = "000000";
//-->
</script> 
             <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            </center> 
           </center> </td> 
          <td class="adColumn"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '160x600';
    kmn_placement = 'f829c9263630394d4aa365109f5e0c0c';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <div class="siteBottom">
     &nbsp; 
   </div> 
  </div> 
  <div class="siteFooterGradient">
   &nbsp;
  </div>   
 </body>
</html>