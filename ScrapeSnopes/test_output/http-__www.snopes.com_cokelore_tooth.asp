Cokelore	Cokelore	More than a century after the creation of Coca-Cola, we're still as much in love with this most famous of soft drinks as our great-grandparents were. Hold up a Coke, and you proclaim all that's best about the American way of life: Coca-Cola is first dates and shy kisses, a platoon of war-weary GIs getting letters from home, and an old, rusted sign creaking in the wind outside the hometown diner. Coca-Cola is also one of the most successful companies the world has ever known; nothing can be that big and popular, so much a part of everyday life, without having legends spring up around it. Coca-Cola's uniquely influential position in our culture has led to a special set of legends we call 'Cokelore'; a collection of Coke trivia and tall tales sure to refresh even the most informationally-parched reader.	http://www.snopes.com/cokelore/cokelore.asp	http://www.snopes.com/graphics/icons/main/coke.gif

Tooth in Advertising	FALSE Tooth in Advertising A tooth left in a glass of Coca-Cola will dissolve overnight.	http://www.snopes.com/cokelore/tooth.asp	http://www.snopes.com//images/red.gif
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <title>snopes.com: Coca-Cola Dissolves Teeth</title> 
  <link href="/style.css" rel="stylesheet" type="text/css" /> 
  <meta name="robots" content="INDEX,NOFOLLOW,NOARCHIVE" /> 
  <meta name="description" content="Will a tooth left in a glass of Coca-Cola dissolve overnight?" /> 
  <meta name="keywords" content="Coca-Cola, Coke, tooth, teeth, dissolve, urban legends archive, hoaxes, scams, rumors, urban myths, folklore, hoax slayer, mythbusters, misinformation, snopes" /> 
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> 
  <link rel="alternate" type="application/rss+xml" href="http://www.snopes.com/info/whatsnew.xml" title="RSS feed for snopes.com" /> 
  <link rel="image_src" href="http://www.snopes.com/logo-small.gif" /> 
  <script src="http://Q1MediaHydraPlatform.com/ads/video/unit_desktop_slider.php?eid=19503"></script>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script language="JavaScript">
if (window!=top){top.location.href=location.href;}
</script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40468225-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
 </head>     
 <body> 
  <script type="text/javascript">
<!--
var casaleD=new Date();var casaleR=(casaleD.getTime()%8673806982)+Math.random();
var casaleU=escape(window.location.href);
var casaleHost=' type="text/javascript" src="http://as.casalemedia.com/s?s=';
document.write('<scr'+'ipt'+casaleHost+'81847&amp;u=');
document.write(casaleU+'&amp;f=1&amp;id='+casaleR+'"><\/scr'+'ipt>');
//-->
</script> 
  <div class="siteWrapper"> 
   <table class="siteHeader" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="logo"><a href="http://www.snopes.com"><img src="/images/template/snopeslogo.gif" alt="snopes.com" /></a></td> 
      <td> 
       <table class="features"> 
        <tbody>
         <tr> 
          <td class="topAdvertisement"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '728x90';
    kmn_placement = 'cad674db7f73589c9a110884ce73bb72';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
         <tr> 
          <td class="searchArea"> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <!--
                    <TD VALIGN=TOP><IMG SRC="/images/search-hdr.gif" ALT="Search"></TD>
                    <TD VALIGN=TOP>

                      <FORM METHOD="GET" ACTION="http://search.atomz.com/search/"><INPUT TYPE=TEXT WIDTH=50 SIZE=60 ID="searchbox" NAME="sp-q">
                    </TD>
                    <TD>
                      <INPUT TYPE=IMAGE SRC="/images/search-btn-go.gif">
                      <INPUT TYPE=HIDDEN NAME="sp-a" VALUE="00062d45-sp00000000">
                      <INPUT TYPE=HIDDEN NAME="sp-advanced" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-p" VALUE="all">
                      <INPUT TYPE=HIDDEN NAME="sp-w-control" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-w" VALUE="alike">
                      <INPUT TYPE=HIDDEN NAME="sp-date-range" VALUE=-1>
                      <INPUT TYPE=HIDDEN NAME="sp-x" VALUE="any">
                      <INPUT TYPE=HIDDEN NAME="sp-c" VALUE=100>
                      <INPUT TYPE=HIDDEN NAME="sp-m" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-s" VALUE=0>
                      </FORM>
-->  
             </tr> 
            </tbody>
           </table> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <table class="siteMenu" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td><a href="/info/whatsnew.asp" onmouseover="window.status='What\'s New';return true" onmouseout="window.status='';return true"><img src="/images/menu-whats-new.gif" alt="What's New" /></a><a href="/info/random/random.asp"><img src="/images/menu-randomizer.gif" alt="Randomizer" /></a><a href="/info/top25uls.asp"><img src="/images/menu-hot-25.gif" alt="Hot 25" /></a><a href="/info/faq.asp"><img src="/images/menu-faq.gif" alt="FAQ" /></a><a href="/daily/"><img src="/images/menu-odd-news.gif" alt="Odd News" /></a><a href="/info/glossary.asp"><img src="/images/menu-glossary.gif" alt="Glossary" /></a><a href="/info/newsletter.asp"><img src="/images/menu-newsletter.gif" alt="Newsletter" /></a><a href="http://message.snopes.com" target="message"><img src="/images/menu-message-board.gif" alt="Message Board" /></a></td> 
     </tr> 
    </tbody>
   </table> 
   <noindex> 
    <table class="siteSubMenu" cellpadding="0" cellspacing="0"> 
     <tbody>
      <tr>
       <td> </td>
       <td width="534" class="breadcrumbs"><a href="/snopes.asp">Home</a> &gt; <a href="/cokelore/cokelore.asp#tooth">Cokelore</a>  &gt; Tooth in Advertising</td> 
       <td width="420" class="links"><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Contact Us';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-contact-us.gif" alt="Contact Us" /></a><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Submit a Rumor';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-submit-rumor.gif" alt="Submit a Rumor" /></a><a href="http://www.snopes.com/cgi-bin/comments/photovideo.asp" onmouseover="window.status='Submit a Photo/Video';return true" onmouseout="window.status='';return true" target="photo"><img src="/images/sub-menu-submit-photo.gif" alt="Submit a Photo/Video" /></a> </td> 
      </tr> 
     </tbody>
    </table> 
   </noindex> 
   <table class="siteContent" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="contentArea"> 
       <table class="threeColumn" cellpadding="0" cellspacing="0"> 
        <tbody>
         <tr valign="TOP"> 
          <td class="navColumn"> <a href="https://twitter.com/snopes" class="twitter-follow-button" data-show-count="false">Follow @snopes</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script> <p></p> 
           <div class="navHeader">
            CATEGORIES:
           </div> 
           <ul> 
            <li><a href="/autos/autos.asp">Autos</a></li> 
            <li><a href="/business/business.asp">Business</a></li> 
            <li><a href="/cokelore/cokelore.asp">Cokelore</a></li> 
            <li><a href="/college/college.asp">College</a></li> 
            <li><a href="/computer/computer.asp">Computers</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/crime/crime.asp">Crime</a></li> 
            <li><a href="/critters/critters.asp">Critter Country</a></li> 
            <li><a href="/disney/disney.asp">Disney</a></li> 
            <li><a href="/embarrass/embarrass.asp">Embarrassments</a></li> 
            <li><a href="/photos/photos.asp">Fauxtography</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/food/food.asp">Food</a></li> 
            <li><a href="/fraud/fraud.asp">Fraud &amp; Scams</a></li> 
            <li><a href="/glurge/glurge.asp">Glurge Gallery</a></li> 
            <li><a href="/history/history.asp">History</a></li> 
            <li><a href="/holidays/holidays.asp">Holidays</a></li> 
            <li><a href="/horrors/horrors.asp">Horrors</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/humor/humor.asp">Humor</a></li> 
            <li><a href="/inboxer/inboxer.asp">Inboxer Rebellion</a></li> 
            <li><a href="/language/language.asp">Language</a></li> 
            <li><a href="/legal/legal.asp">Legal</a></li> 
            <li><a href="/lost/lost.asp">Lost Legends</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/love/love.asp">Love</a></li> 
            <li><a href="/luck/luck.asp">Luck</a></li> 
            <li><a href="/media/media.asp">Media Matters</a></li> 
            <li><a href="/medical/medical.asp">Medical</a></li> 
            <li><a href="/military/military.asp">Military</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/movies/movies.asp">Movies</a></li> 
            <li><a href="/music/music.asp">Music</a></li> 
            <li><a href="/oldwives/oldwives.asp">Old Wives' Tales</a></li> 
            <li><a href="/politics/politics.asp">Politics</a></li> 
            <li><a href="/pregnant/pregnant.asp">Pregnancy</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/quotes/quotes.asp">Quotes</a></li> 
            <li><a href="/racial/racial.asp">Racial Rumors</a></li> 
            <li><a href="/radiotv/radiotv.asp">Radio &amp; TV</a></li> 
            <li><a href="/religion/religion.asp">Religion</a></li> 
            <li><a href="/risque/risque.asp">Risqu&eacute; Business</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/science/science.asp">Science</a></li> 
            <li><a href="/rumors/rumors.asp">September 11</a></li> 
            <li><a href="/sports/sports.asp">Sports</a></li> 
            <li><a href="/travel/travel.asp">Travel</a></li> 
            <li><a href="/weddings/weddings.asp">Weddings</a></li> 
           </ul> </td> 
          <td class="contentColumn" align="LEFT"> <br /> <a href="mailto:Insert address(es) here?subject=Coca-Cola Dissolves Teeth&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/tooth.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><img src="/images/envelope.gif" alt="E-mail this page" /></a> <a href="mailto:Insert address(es) here?subject=Coca-Cola Dissolves Teeth&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/tooth.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><font size="2" color="#0000FF">E-mail this</font></a> 
           <!--
 &nbsp; &nbsp; &nbsp; &nbsp; <div class="fb-like" data-href="https://www.facebook.com/pages/snopescom/241061082705085" data-send="false" data-width="110" data-show-faces="false"></div>
--> <br /><br /> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <td valign="TOP"><img src="/images/search-hdr.gif" alt="Search" /></td> 
              <td valign="TOP"> 
               <form method="GET" action="http://search.atomz.com/search/">
                <input type="TEXT" width="50" size="60" id="searchbox" name="sp-q" /> 
               </form></td> 
              <td> <input type="IMAGE" src="/images/search-btn-go.gif" /> <input type="HIDDEN" name="sp-a" value="00062d45-sp00000000" /> <input type="HIDDEN" name="sp-advanced" value="1" /> <input type="HIDDEN" name="sp-p" value="all" /> <input type="HIDDEN" name="sp-w-control" value="1" /> <input type="HIDDEN" name="sp-w" value="alike" /> <input type="HIDDEN" name="sp-date-range" value="-1" /> <input type="HIDDEN" name="sp-x" value="any" /> <input type="HIDDEN" name="sp-c" value="100" /> <input type="HIDDEN" name="sp-m" value="1" /> <input type="HIDDEN" name="sp-s" value="0" />  </td> 
             </tr> 
            </tbody>
           </table> <br /><br /> 
           <div class="utilityLinks"> 
            <!-- code goes here --> 
            <div class="pw-widget pw-counter-vertical"> 
             <a class="pw-button-googleplus pw-look-native"></a> 
             <a class="pw-button-tumblr pw-look-native"></a> 
             <a class="pw-button-pinterest pw-look-native"></a> 
             <a class="pw-button-reddit pw-look-native"></a> 
             <a class="pw-button-email pw-look-native"></a> 
             <a class="pw-button-facebook pw-look-native"></a> 
             <a class="pw-button-twitter pw-look-native"></a> 
            </div> 
            <script src="http://i.po.st/static/v3/post-widget.js#publisherKey=snopescom1152" type="text/javascript"></script> 
            <!-- code goes here --> 
           </div> <br /> <h1>Tooth in Advertising</h1> <style type="text/css">
<!--
 A:link, A:visited, A:active { text-decoration: underline; }
 A:link {color: #0000FF;}
 A:visited {color: #5fa505;}
 A:hover {color: #FF0000;} 
 .article_text {text-align: justify;} 
-->
</style> 
           <div class="article_text"> 
            <font color="#5FA505"><b>Claim:</b></font> &nbsp; A tooth left in a glass of Coca-Cola will dissolve overnight. 
            <br />
            <br /> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <noindex>
             <table>
              <tbody>
               <tr>
                <td valign="CENTER"><img src="/images/red.gif" /></td>
                <td valign="TOP"><font size="5" color="#37628D"><b>FALSE</b></font></td>
               </tr>
              </tbody>
             </table>
            </noindex> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <br /> 
            <font color="#5FA505"><b>Origins:</b></font> &nbsp; I 
            <img src="graphics/tooth.gif" align="RIGHT" hspace="16" vspace="16" alt="Tooth" width="90" height="124" border="0" /> don't know of anyone who hasn't heard the rumor that too much 
            <nobr>
             Coca-Cola
            </nobr> rots your innards, and the proof of this can be determined by dropping a baby tooth into a glass of it, then going back the next morning to find most of it eaten away. If Coca-Cola can dissolve a tooth overnight, imagine what it must be doing to 
            <i>your</i> teeth, not to mention your stomach and digestive tract! 
            <br />
            <br /> All such claims ignore a few salient points: 
            <ul type="Disc"> 
             <li> Coca-Cola will not dissolve a tooth (or a nail, or a penny, or a piece of meat) overnight.</li> 
             <p></p> 
             <li> Coca-Cola contains acids (such as citric acid and phosphoric acid) which will <i>eventually</i> dissolve items such as teeth (given enough time), but so do plenty of other substances we commonly ingest (such as orange juice). The <i>concentration</i> of acid in these products is so low that our digestive systems are easily capable of coping with it with no harm to us.</li> 
             <p></p> 
             <li> The idea that any substance which can dissolve teeth must therefore damage our teeth if we drink it is nonsensical. We don't hold drinks in our mouths for days at a time 
              <nobr>
               &#x2014; any
              </nobr> liquids we drink simply wash over our teeth very briefly, and our teeth are further protected by their enamel coating and the ameliorating effects of saliva.</li> 
            </ul> Vince Staten describes the legendary version of this tale: 
            <br /> 
            <font face="Verdana" size="2"> 
             <div style="text-align: justify; margin-top: 20px; margin-bottom: 20px; margin-left: 40px; margin-right: 40px">
               Perhaps you've heard the story. It goes something like this: At Harvard they left a fly in a Coke overnight and the next morning, the fly had been completely dissolved. The name of the university changes and so does the item to be soaked overnight, but the result is always the same: Coke eats it. The lesson is that if it 
              <table align="RIGHT" cellpadding="12" border="0">
               <tbody>
                <tr>
                 <td> 
                  <center> 
                   <script type="text/javascript"><!--
    kmn_size = '300x250';
    kmn_placement = '1c8834441f20e22a0aef009c095a6cab';
//--></script> 
                   <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
                  </center> </td>
                </tr>
               </tbody>
              </table> does that to a fly, just think of what it does to your stomach. 
              <br />
              <br /> To test this theory I swatted two flies: a test fly and a control fly. I put the test fly in a cup of Coke and let it soak for twenty-four hours. I put the control fly in a cup of Roto-Rooter drain cleaner and let it soak an equal length of time. 
              <br />
              <br /> When I returned to the Coke fly the next day, I discovered, to my surprise, the fly floating around, unscathed. The Roto-Rooter fly, on the other hand, was dissolved down to a couple of tiny fly bits. The Roto-Rooter had also eaten through the bottom of the plastic cup. 
             </div> </font> Frederick Allen discussed the origins of this rumor in his book on 
            <nobr>
             Coca-Cola
            </nobr>: 
            <br /> 
            <font face="Verdana" size="2"> 
             <div style="text-align: justify; margin-top: 20px; margin-bottom: 20px; margin-left: 40px; margin-right: 40px">
               In the fall of 1950, a Cornell University professor named 
              <nobr>
               Clive M.
              </nobr> McCay testified before a select committee in the U.S. House of Representatives that the sugar in Coke caused cavities. And, he said, the phosphoric acid was a dangerous additive. Giving a vivid account that instantly became part of the national folklore, 
              <nobr>
               Dr. McCay
              </nobr> described how a tooth left in a glass of 
              <nobr>
               Coca-Cola
              </nobr> would soften and begin to dissolve in a period of two days. 
              <br />
              <br /> 
              <nobr>
               Coca-Cola
              </nobr>'s top chemist, Orville May, explained to Hobbs [then president of 
              <nobr>
               Coca-Cola
              </nobr>] and the company's other executives that 
              <i>anything</i> containing sugar and phosphoric acid 
              <nobr>
               &#x2014; fresh
              </nobr> orange juice, for example 
              <nobr>
               &#x2014; would
              </nobr> dissolve teeth over a period of time. The point was people did not hold food and beverages in their mouths for days on end. They swallowed, and their saliva washed away the sugar and acid before lasting damage was done. Otherwise the whole country would be toothless. 
             </div> </font> Mark Pendergrast tackled the same subject: 
            <br /> 
            <font face="Verdana" size="2"> 
             <div style="text-align: justify; margin-top: 20px; margin-bottom: 20px; margin-left: 40px; margin-right: 40px">
               McCay made headlines with his allegations that Coke would eat away the marble steps of the Capitol Building and soften teeth placed in a glass of the beverage. &quot;The molar teeth of rats were dissolved down to the gum line,&quot; McCay told the politicians, when &quot;given nothing to drink except cola beverages for a period of six months.&quot; 
              <br />
              <br /> In response, 
              <nobr>
               Coca-Cola
              </nobr>'s head chemist, Orville May, testified that McCay offered a &quot;distorted picture&quot; intended to frighten unsuspecting consumers. May pointed out that the 
              <nobr>
               .055 percent
              </nobr> level of phosphoric acid was far below the 
              <nobr>
               1.09 percent
              </nobr> acid content of an orange and that McCay's studies ignored the neutralizing effect of saliva. Finally, he noted that orange juice or lemonade would also dissolve ten-penny nails and eat holes in the Capitol steps. 
             </div> </font> We have to agree with Vince Staten's conclusion: 
            <br /> 
            <font face="Verdana" size="2"> 
             <div style="text-align: justify; margin-top: 20px; margin-bottom: 20px; margin-left: 40px; margin-right: 40px">
               I think there are two lessons here: Don't believe all those Coke stories you hear. And don't, for any reason, let a fly drink Roto-Rooter. 
             </div> </font> Barbara &quot;truth fairy&quot; Mikkelson 
            <br />
            <br /> 
            <font color="#5FA505"><b>Last updated:</b></font> &nbsp; 21 May 2011 
            <!--
27 February 2001 - original
16 March 2007 - reformatted
--> 
            <br />
            <br /> 
            <center>
             <font face="Verdana" size="2" color="#5FA505">Urban Legends Reference Pages &copy; 1995-2014 by snopes.com. <br /> This material may not be reproduced without permission. <br /> snopes and the snopes.com logo are registered service marks of snopes.com.</font>
            </center>
            <br /> 
           </div> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <table>
            <tbody>
             <tr>
              <td><img src="/images/template/icon-sources.gif" /></td>
              <td><h2>Sources:</h2></td>
             </tr>
            </tbody>
           </table> <font size="-1"> 
            <dl> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Allen, Frederick. &nbsp; 
               <i>Secret Formula.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; New York: HarperCollins, 1994. &nbsp; ISBN 0-88730-672-1.
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Pendergrast, Mark. &nbsp; 
               <i>For God, Country, and Coca-Cola.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; New York: Charles Scribner's Sons, 1993. &nbsp; ISBN 0-684-19347-7.
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Staten, Vince. &nbsp; 
               <i>Can You Trust a Tomato in January?</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; New York: Simon &amp; Schuster, 1993. &nbsp; ISBN 0-671-76941-3 &nbsp; (p. 165).
              </nobr>
             </dd> 
            </dl> </font> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <center>
            <center> 
             <script type="text/javascript">
<!--
google_ad_client = "pub-6608306193529351";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_type = "text";
google_ad_channel = "0054321535";
google_color_border = "000000";
//-->
</script> 
             <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            </center> 
           </center> </td> 
          <td class="adColumn"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '160x600';
    kmn_placement = 'f829c9263630394d4aa365109f5e0c0c';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <div class="siteBottom">
     &nbsp; 
   </div> 
  </div> 
  <div class="siteFooterGradient">
   &nbsp;
  </div>   
 </body>
</html>