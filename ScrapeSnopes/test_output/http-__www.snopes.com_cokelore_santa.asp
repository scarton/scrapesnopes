Cokelore	Cokelore	More than a century after the creation of Coca-Cola, we're still as much in love with this most famous of soft drinks as our great-grandparents were. Hold up a Coke, and you proclaim all that's best about the American way of life: Coca-Cola is first dates and shy kisses, a platoon of war-weary GIs getting letters from home, and an old, rusted sign creaking in the wind outside the hometown diner. Coca-Cola is also one of the most successful companies the world has ever known; nothing can be that big and popular, so much a part of everyday life, without having legends spring up around it. Coca-Cola's uniquely influential position in our culture has led to a special set of legends we call 'Cokelore'; a collection of Coke trivia and tall tales sure to refresh even the most informationally-parched reader.	http://www.snopes.com/cokelore/cokelore.asp	http://www.snopes.com/graphics/icons/main/coke.gif

The Claus That Refreshes	FALSE The Claus That Refreshes The modern image of Santa Claus was created by Coca-Cola.	http://www.snopes.com/cokelore/santa.asp	http://www.snopes.com//images/red.gif
<html>
 <head>
  <meta http-equiv="Refresh" content="0; URL=/holidays/christmas/santa/cocacola.asp" /> 
 </head>
 <body></body>
</html>