Cokelore	Cokelore	More than a century after the creation of Coca-Cola, we're still as much in love with this most famous of soft drinks as our great-grandparents were. Hold up a Coke, and you proclaim all that's best about the American way of life: Coca-Cola is first dates and shy kisses, a platoon of war-weary GIs getting letters from home, and an old, rusted sign creaking in the wind outside the hometown diner. Coca-Cola is also one of the most successful companies the world has ever known; nothing can be that big and popular, so much a part of everyday life, without having legends spring up around it. Coca-Cola's uniquely influential position in our culture has led to a special set of legends we call 'Cokelore'; a collection of Coke trivia and tall tales sure to refresh even the most informationally-parched reader.	http://www.snopes.com/cokelore/cokelore.asp	http://www.snopes.com/graphics/icons/main/coke.gif

Knew Coke	FALSE Knew Coke The New Coke fiasco was actually a clever marketing ploy.	http://www.snopes.com/cokelore/newcoke.asp	http://www.snopes.com//images/red.gif
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <title>snopes.com: New Coke Origins</title> 
  <link href="/style.css" rel="stylesheet" type="text/css" /> 
  <meta name="robots" content="INDEX,NOFOLLOW,NOARCHIVE" /> 
  <meta name="description" content="Was the 'New Coke' fiasco really a clever marketing ploy?" /> 
  <meta name="keywords" content="Coca-Cola, Coke, New Coke, Pepsi, urban legends archive, hoaxes, scams, rumors, urban myths, folklore, hoax slayer, mythbusters, misinformation, snopes" /> 
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> 
  <link rel="alternate" type="application/rss+xml" href="http://www.snopes.com/info/whatsnew.xml" title="RSS feed for snopes.com" /> 
  <link rel="image_src" href="http://www.snopes.com/logo-small.gif" /> 
  <script src="http://Q1MediaHydraPlatform.com/ads/video/unit_desktop_slider.php?eid=19503"></script>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script language="JavaScript">
if (window!=top){top.location.href=location.href;}
</script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40468225-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
 </head>     
 <body> 
  <script type="text/javascript">
<!--
var casaleD=new Date();var casaleR=(casaleD.getTime()%8673806982)+Math.random();
var casaleU=escape(window.location.href);
var casaleHost=' type="text/javascript" src="http://as.casalemedia.com/s?s=';
document.write('<scr'+'ipt'+casaleHost+'81847&amp;u=');
document.write(casaleU+'&amp;f=1&amp;id='+casaleR+'"><\/scr'+'ipt>');
//-->
</script> 
  <div class="siteWrapper"> 
   <table class="siteHeader" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="logo"><a href="http://www.snopes.com"><img src="/images/template/snopeslogo.gif" alt="snopes.com" /></a></td> 
      <td> 
       <table class="features"> 
        <tbody>
         <tr> 
          <td class="topAdvertisement"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '728x90';
    kmn_placement = 'cad674db7f73589c9a110884ce73bb72';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
         <tr> 
          <td class="searchArea"> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <!--
                    <TD VALIGN=TOP><IMG SRC="/images/search-hdr.gif" ALT="Search"></TD>
                    <TD VALIGN=TOP>

                      <FORM METHOD="GET" ACTION="http://search.atomz.com/search/"><INPUT TYPE=TEXT WIDTH=50 SIZE=60 ID="searchbox" NAME="sp-q">
                    </TD>
                    <TD>
                      <INPUT TYPE=IMAGE SRC="/images/search-btn-go.gif">
                      <INPUT TYPE=HIDDEN NAME="sp-a" VALUE="00062d45-sp00000000">
                      <INPUT TYPE=HIDDEN NAME="sp-advanced" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-p" VALUE="all">
                      <INPUT TYPE=HIDDEN NAME="sp-w-control" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-w" VALUE="alike">
                      <INPUT TYPE=HIDDEN NAME="sp-date-range" VALUE=-1>
                      <INPUT TYPE=HIDDEN NAME="sp-x" VALUE="any">
                      <INPUT TYPE=HIDDEN NAME="sp-c" VALUE=100>
                      <INPUT TYPE=HIDDEN NAME="sp-m" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-s" VALUE=0>
                      </FORM>
-->  
             </tr> 
            </tbody>
           </table> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <table class="siteMenu" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td><a href="/info/whatsnew.asp" onmouseover="window.status='What\'s New';return true" onmouseout="window.status='';return true"><img src="/images/menu-whats-new.gif" alt="What's New" /></a><a href="/info/random/random.asp"><img src="/images/menu-randomizer.gif" alt="Randomizer" /></a><a href="/info/top25uls.asp"><img src="/images/menu-hot-25.gif" alt="Hot 25" /></a><a href="/info/faq.asp"><img src="/images/menu-faq.gif" alt="FAQ" /></a><a href="/daily/"><img src="/images/menu-odd-news.gif" alt="Odd News" /></a><a href="/info/glossary.asp"><img src="/images/menu-glossary.gif" alt="Glossary" /></a><a href="/info/newsletter.asp"><img src="/images/menu-newsletter.gif" alt="Newsletter" /></a><a href="http://message.snopes.com" target="message"><img src="/images/menu-message-board.gif" alt="Message Board" /></a></td> 
     </tr> 
    </tbody>
   </table> 
   <noindex> 
    <table class="siteSubMenu" cellpadding="0" cellspacing="0"> 
     <tbody>
      <tr>
       <td> </td>
       <td width="534" class="breadcrumbs"><a href="/snopes.asp">Home</a> &gt; <a href="/cokelore/cokelore.asp#newcoke">Cokelore</a> &gt; Knew Coke</td> 
       <td width="420" class="links"><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Contact Us';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-contact-us.gif" alt="Contact Us" /></a><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Submit a Rumor';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-submit-rumor.gif" alt="Submit a Rumor" /></a><a href="http://www.snopes.com/cgi-bin/comments/photovideo.asp" onmouseover="window.status='Submit a Photo/Video';return true" onmouseout="window.status='';return true" target="photo"><img src="/images/sub-menu-submit-photo.gif" alt="Submit a Photo/Video" /></a> </td> 
      </tr> 
     </tbody>
    </table> 
   </noindex> 
   <table class="siteContent" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="contentArea"> 
       <table class="threeColumn" cellpadding="0" cellspacing="0"> 
        <tbody>
         <tr valign="TOP"> 
          <td class="navColumn"> <a href="https://twitter.com/snopes" class="twitter-follow-button" data-show-count="false">Follow @snopes</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script> <p></p> 
           <div class="navHeader">
            CATEGORIES:
           </div> 
           <ul> 
            <li><a href="/autos/autos.asp">Autos</a></li> 
            <li><a href="/business/business.asp">Business</a></li> 
            <li><a href="/cokelore/cokelore.asp">Cokelore</a></li> 
            <li><a href="/college/college.asp">College</a></li> 
            <li><a href="/computer/computer.asp">Computers</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/crime/crime.asp">Crime</a></li> 
            <li><a href="/critters/critters.asp">Critter Country</a></li> 
            <li><a href="/disney/disney.asp">Disney</a></li> 
            <li><a href="/embarrass/embarrass.asp">Embarrassments</a></li> 
            <li><a href="/photos/photos.asp">Fauxtography</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/food/food.asp">Food</a></li> 
            <li><a href="/fraud/fraud.asp">Fraud &amp; Scams</a></li> 
            <li><a href="/glurge/glurge.asp">Glurge Gallery</a></li> 
            <li><a href="/history/history.asp">History</a></li> 
            <li><a href="/holidays/holidays.asp">Holidays</a></li> 
            <li><a href="/horrors/horrors.asp">Horrors</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/humor/humor.asp">Humor</a></li> 
            <li><a href="/inboxer/inboxer.asp">Inboxer Rebellion</a></li> 
            <li><a href="/language/language.asp">Language</a></li> 
            <li><a href="/legal/legal.asp">Legal</a></li> 
            <li><a href="/lost/lost.asp">Lost Legends</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/love/love.asp">Love</a></li> 
            <li><a href="/luck/luck.asp">Luck</a></li> 
            <li><a href="/media/media.asp">Media Matters</a></li> 
            <li><a href="/medical/medical.asp">Medical</a></li> 
            <li><a href="/military/military.asp">Military</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/movies/movies.asp">Movies</a></li> 
            <li><a href="/music/music.asp">Music</a></li> 
            <li><a href="/oldwives/oldwives.asp">Old Wives' Tales</a></li> 
            <li><a href="/politics/politics.asp">Politics</a></li> 
            <li><a href="/pregnant/pregnant.asp">Pregnancy</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/quotes/quotes.asp">Quotes</a></li> 
            <li><a href="/racial/racial.asp">Racial Rumors</a></li> 
            <li><a href="/radiotv/radiotv.asp">Radio &amp; TV</a></li> 
            <li><a href="/religion/religion.asp">Religion</a></li> 
            <li><a href="/risque/risque.asp">Risqu&eacute; Business</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/science/science.asp">Science</a></li> 
            <li><a href="/rumors/rumors.asp">September 11</a></li> 
            <li><a href="/sports/sports.asp">Sports</a></li> 
            <li><a href="/travel/travel.asp">Travel</a></li> 
            <li><a href="/weddings/weddings.asp">Weddings</a></li> 
           </ul> </td> 
          <td class="contentColumn" align="LEFT"> <br /> <a href="mailto:Insert address(es) here?subject=New Coke Origins&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/newcoke.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><img src="/images/envelope.gif" alt="E-mail this page" /></a> <a href="mailto:Insert address(es) here?subject=New Coke Origins&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/newcoke.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><font size="2" color="#0000FF">E-mail this</font></a> 
           <!--
 &nbsp; &nbsp; &nbsp; &nbsp; <div class="fb-like" data-href="https://www.facebook.com/pages/snopescom/241061082705085" data-send="false" data-width="110" data-show-faces="false"></div>
--> <br /><br /> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <td valign="TOP"><img src="/images/search-hdr.gif" alt="Search" /></td> 
              <td valign="TOP"> 
               <form method="GET" action="http://search.atomz.com/search/">
                <input type="TEXT" width="50" size="60" id="searchbox" name="sp-q" /> 
               </form></td> 
              <td> <input type="IMAGE" src="/images/search-btn-go.gif" /> <input type="HIDDEN" name="sp-a" value="00062d45-sp00000000" /> <input type="HIDDEN" name="sp-advanced" value="1" /> <input type="HIDDEN" name="sp-p" value="all" /> <input type="HIDDEN" name="sp-w-control" value="1" /> <input type="HIDDEN" name="sp-w" value="alike" /> <input type="HIDDEN" name="sp-date-range" value="-1" /> <input type="HIDDEN" name="sp-x" value="any" /> <input type="HIDDEN" name="sp-c" value="100" /> <input type="HIDDEN" name="sp-m" value="1" /> <input type="HIDDEN" name="sp-s" value="0" />  </td> 
             </tr> 
            </tbody>
           </table> <br /><br /> 
           <div class="utilityLinks"> 
            <!-- code goes here --> 
            <div class="pw-widget pw-counter-vertical"> 
             <a class="pw-button-googleplus pw-look-native"></a> 
             <a class="pw-button-tumblr pw-look-native"></a> 
             <a class="pw-button-pinterest pw-look-native"></a> 
             <a class="pw-button-reddit pw-look-native"></a> 
             <a class="pw-button-email pw-look-native"></a> 
             <a class="pw-button-facebook pw-look-native"></a> 
             <a class="pw-button-twitter pw-look-native"></a> 
            </div> 
            <script src="http://i.po.st/static/v3/post-widget.js#publisherKey=snopescom1152" type="text/javascript"></script> 
            <!-- code goes here --> 
           </div> <br /> <h1>Knew Coke</h1> <style type="text/css">
<!--
 A:link, A:visited, A:active { text-decoration: underline; }
 A:link {color: #0000FF;}
 A:visited {color: #5fa505;}
 A:hover {color: #FF0000;} 
 .article_text {text-align: justify;} 
-->
</style> 
           <div class="article_text"> 
            <font color="#5FA505"><b>Claim:</b></font> &nbsp; Coca-Cola halted production of its flagship beverage in 1985 and introduced New Coke in its place as a marketing ploy to combat declining market share and rekindle interest in the original drink. 
            <br />
            <br /> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <noindex>
             <table>
              <tbody>
               <tr>
                <td valign="CENTER"><img src="/images/red.gif" /></td>
                <td valign="TOP"><font size="5" color="#37628D"><b>FALSE</b></font></td>
               </tr>
              </tbody>
             </table> 
            </noindex> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <br /> 
            <font color="#5FA505"><b>Origins:</b></font> &nbsp; As much as we'd like to believe that The 
            <nobr>
             Coca-Cola
            </nobr> Company is infallible, it proved in 1985 that it isn't. 
            <br />
            <br /> It's inconceivable to us mere mortals that a company of the size and scope of 
            <nobr>
             Coca-Cola
            </nobr> could make a mistake on the scale of 
            <img src="./graphics/newcoke.jpg" align="LEFT" hspace="16" vspace="16" alt="New Coke" width="200" height="262" border="1" /> the one it made in 1985. Although company officials have been honest (albeit a bit redfaced) about the blunder all along, the legend has arisen that New Coke was nothing more that a throwaway product created as part of a greater plan. People refused to believe any decision that colossally disastrous (and ultimately that colossally fortuitous) could have been the mere result of a very human miscalculation. 
            <br />
            <br /> And yet it was. It was also a very logical &#x2014; indeed, reasonable 
            <nobr>
             &#x2014; mistake
            </nobr> to make. 
            <br />
            <br /> The early 1980s found Coke teetering on the edge of losing the cola war to Pepsi. The previous fifteen years saw 
            <nobr>
             Coca-Cola
            </nobr>'s market share remain flat while Pepsi's continued to climb. Pepsi was winning in the supermarkets (where shoppers had free rein to choose either beverage), and it was only Coke's greater availability in restricted markets (such as soda vending machines and fast food outlets) that was keeping its numbers ahead of Pepsi's. (Coke's market share had been shrinking for decades, from 60% just after 
            <nobr>
             World War II
            </nobr> to under 24% in 1983.) 
            <br />
            <br /> Coke's market share problems were exacerbated by the relative success of other types of sodas, including some manfactured by 
            <nobr>
             Coca-Cola
            </nobr> and Pepsi themselves. The more consumers drinking diet, citrus, or caffeine-free beverages, the fewer sugar cola drinkers there were to sell to. The pie was getting smaller. This market segmentation should have been affecting Coke and Pepsi equally, yet only Coke had to fight to hang onto its share. Despite the competition, Pepsi was 
            <i>gaining</i> new customers. No doubt about it, people liked the taste of what the boys in blue were selling. 
            <br />
            <br /> Adding to 
            <nobr>
             Coca-Cola
            </nobr>'s segmentation problems was the runaway success of their own child, Diet Coke. Rather than replacing the sugars in the 
            <nobr>
             Coca-Cola
            </nobr> formula with artificial sweeteners and then attempting to bring the taste of the new beverage back to more closely resemble the original, the company formulated Diet Coke the other way around. An entirely new flavor was created 
            <nobr>
             &#x2014; one
            </nobr> that was smoother and had less bite to it but was still a cola. People loved it. 
            <br />
            <br /> Introduced in 1982, Diet Coke shot up the charts to become the uncontested #4 soft drink in America (with only Coke, Pepsi, and 
            <nobr>
             7-Up
            </nobr> ahead of it) by the end of 1983, and by 1984 it was comfortably nestled in the #3 spot. It was also helping to speed the ascendency of Pepsi over 
            <nobr>
             Coca-Cola
            </nobr>, because the more Diet Coke drinkers there were, the smaller the pool of available sugar cola drinkers 
            <table align="RIGHT" cellpadding="12" border="0">
             <tbody>
              <tr>
               <td> 
                <center> 
                 <script type="text/javascript"><!--
    kmn_size = '300x250';
    kmn_placement = '1c8834441f20e22a0aef009c095a6cab';
//--></script> 
                 <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
                </center> </td>
              </tr>
             </tbody>
            </table> became. 
            <br />
            <br /> Unless something was done to stem the tide, it was only a matter of time before Pepsi pulled ahead of 
            <nobr>
             Coca-Cola
            </nobr>. 
            <nobr>
             Coca-Cola
            </nobr> management couldn't allow this to happen because Pepsi could then honestly claim more people drank Pepsi than Coke, not just that people preferred the 
            <u>taste</u> of Pepsi to Coke (which they were already proclaiming in their &quot;Pepsi Challenge&quot; commercials). It was panic button time in Atlanta; time to figure out how to beat Pepsi. 
            <br />
            <br /> When all other factors were eliminated, it came down to a matter of flavor. Batteries of well-controlled taste tests showed folks liked the taste of Pepsi better. Seemingly confirming that original 
            <nobr>
             Coca-Cola
            </nobr> had a taste problem was the popularity of Diet Coke, a beverage formulated in such a way that it ended up with a flavor a lot closer to that of Pepsi than to its parent beverage. 
            <br />
            <br /> Enter New Coke. When traditional methods of developing a new taste failed, Coca-Cola pulled a reverse on the old method of creating diet soft drinks. Diet Coke was stripped of its artificial sweeteners, and high fructose corn syrup was added in their place. After a year of fiddling with the flavor balances, New Coke was finally as good as the company could make it. It tasted smoother and sweeter than original Coke, more like Pepsi. Sounds like a good idea so far, eh? Well, it sounded like an even better one when the results came in from a battery of taste tests utilizing the new formula. People said they liked the new Coke better than 
            <nobr>
             Coca-Cola
            </nobr> 
            <i>or</i> Pepsi, and by a significant factor, too. Taste for taste, it was a winner. 
            <br />
            <br /> The next hurdle was what to do with the original: continue to market it, or discontinue the product? The company was already seeing its sugar cola market shrink thanks to competing lines; it wasn't going to make its market share problems any worse by splitting its entry in the sugar cola category. Although New Coke and Classic Coke drinkers combined might outnumber Pepsi imbibers, it was a lead pipe cinch Pepsi would claim to have a more popular drink than one or both of them. This was too big a marketing advantage to hand to 
            <nobr>
             Coca-Cola
            </nobr>'s #1 competitor. Thus the decision was made to discontinue 
            <nobr>
             Coca-Cola
            </nobr> when New Coke was introduced. Again, this looked great on paper 
            <nobr>
             &#x2014; wage
            </nobr> the war between 
            <nobr>
             Coca-Cola
            </nobr> and Pepsi, not 
            <nobr>
             Coca-Cola
            </nobr> and itself. 
            <br />
            <br /> So what happened? When Coke went ahead with its plan, an immediate and very loud outcry was raised. Long before they'd tasted a sip of it, millions of Americans had decided they 
            <i>hated</i> New Coke. Yes, in blind taste tests people had consistently said they liked the new formula better. However, a soft drink is so much more than merely its flavor; a soda is also its marketing. Coke had spent more than a hundred years convincing the North American population that its product was an integral part of their lives, their very identities. Taste be damned: to do away with 
            <nobr>
             Coca-Cola
            </nobr> was to rip something vital from the American soul. Americans (never ones to peaceably go along with anything perceived as violating their identity) weren't going to stand for it, and they weren't shy about saying so. 
            <br />
            <br /> Although the company had known all along a segment of Coke drinkers weren't going to switch to the new product, they had no way to even roughly estimate how large this segment would be. The New Coke project had been kept secret for years; this secrecy wouldn't have been possible if company personnel had been questioning test subjects on how they'd feel about the new cola if it were to 
            <i>replace</i> the old one. That secrecy lay at the heart of the fiasco, for it prevented 
            <nobr>
             Coca-Cola
            </nobr> from asking the key question during its product tests. 
            <br />
            <br /> New Coke was introduced on 
            <nobr>
             23 April
            </nobr> 1985, and production of the original formulation ended that same week. The outrage of millions of Americans didn't take long to sink in, and not all that much longer to be redressed. At an 
            <nobr>
             11 July
            </nobr> 1985 press conference, two 
            <nobr>
             Coca-Cola
            </nobr> executives announced the return of the original formula. &quot;We have heard you,&quot; said Roberto Goizueta, then Chairman of 
            <nobr>
             Coca-Cola
            </nobr>. Donald Keough (then the company's President and Chief Operating Officer) said: 
            <br /> 
            <font face="Verdana" size="2"> 
             <div style="text-align: justify; margin-top: 20px; margin-bottom: 20px; margin-left: 40px; margin-right: 40px">
               There is a twist to this story which will please every humanist and will probably keep Harvard professors puzzled for years. The simple fact is that all the time and money and skill poured into consumer research on the new 
              <nobr>
               Coca-Cola
              </nobr> could not measure or reveal the deep and abiding emotional attachment to original 
              <nobr>
               Coca-Cola
              </nobr> felt by so many 
              <nobr>
               people . . .
              </nobr> 
              <br />
              <br /> The passion for original 
              <nobr>
               Coca-Cola
              </nobr> &#x2014; and that is the word for it, 
              <nobr>
               passion &#x2014; was
              </nobr> something that caught us by 
              <nobr>
               surprise . . . 
              </nobr> It is a wonderful American mystery, a lovely American enigma, and you cannot measure it any more than you can measure love, pride, or patriotism. 
             </div> </font> How important was this news of the beloved beverage's return? Vital enough that Peter Jennings of ABC News interrupted 
            <i>General Hospital</i> to break the story on national TV. Company insiders referred to the decision as &quot;the second coming,&quot; and that's how consumers reacted to it. Anger melted into forgiveness, and then turned to celebration. 
            <br />
            <br /> Having two sugar cola products on the market did indeed split the market share as 
            <nobr>
             Coca-Cola
            </nobr> had feared: market surveys at the end of 1985 showed Pepsi ahead of New Coke 
            <i>and</i> Classic Coke combined. Even so, a miracle was underway. Against all expectations, Classic Coke began outselling New Coke, and to everyone's surprise it kept gaining in popularity until it had reclaimed the sugar cola crown from Pepsi in early 1986. New Coke sort of faded away (it quickly settled to a 3% market share in its first year and was redubbed &quot;Coke II&quot; in 1990) and now holds onto a 0.1% market share. 
            <br />
            <br /> Those who'd liked New Coke (but wouldn't, one would suppose, be caught dead drinking it due to all the bad feelings associated with the beverage) gravitated to Diet Coke, the product that tasted most like what they really wanted. 
            <br />
            <br /> An interesting little claim sprang up in the wake of the introduction of Classic Coke, one having to do with its sweetener. People swore they detected a change in the flavor between Classic Coke and the original. This gave rise to the rumor that the product had been reformulated, dropping cane sugar in favor of high fructose corn syrup (HFCS). Depending upon whom you listened to, either the demand for the return of original 
            <nobr>
             Coca-Cola
            </nobr> afforded the company the opportunity to switch from cane sugar to corn syrup or the whole fiasco of taking original 
            <nobr>
             Coca-Cola
            </nobr> off the shelves and reintroducing it three months later as Classic Coke was all a brilliant scheme to mask the change in sweetener. According to whispered wisdom, the company had hoped to slip the modification past consumers by having it take place during the original beverage's absence from the shelves. People would be so darned glad to have Classic Coke back that they wouldn't notice it didn't taste the same as original 
            <nobr>
             Coca-Cola
            </nobr>. (Another twist to this rumor had it that New Coke had 
            <i>deliberately</i> been formulated to taste awful in order to facilitate the 
            <nobr>
             switch &#x2014; this
            </nobr> supposedly gave 
            <nobr>
             Coca-Cola
            </nobr> an excuse for pulling the original formula and then putting it back on the market after a brief absence, making it look all along as if they were simply responding to consumer demands.) 
            <br />
            <br /> The change in sweetener wasn't anything that diabolical. Corn syrup was cheaper than cane sugar; that's what it came down to. In 1980, five years before the introduction of New Coke, Coca-Cola had begun to allow bottlers to replace half the cane sugar in 
            <nobr>
             Coca-Cola
            </nobr> with HFCS. By six months prior to New Coke's knocking the original 
            <nobr>
             Coca-Cola
            </nobr> off the shelves, American 
            <nobr>
             Coca-Cola
            </nobr> bottlers were allowed to use 100% HFCS. Whether they knew it or not, many consumers were already drinking Coke that was 100% sweetened by HFCS. 
            <br />
            <br /> In the wake of the discontinuation of the original cola, the introduction of New Coke, and the return of the original a few months later as Classic Coke, persistent rumors sprang up that it had all been a gambit to rekindle 
            <nobr>
             Coca-Cola
            </nobr>'s declining sugar cola market share. Various people have claimed to have known someone who saw the original 
            <nobr>
             Coca-Cola
            </nobr> still being bottled and canned during the New Coke-only phase. Such claims are best considered in light of two things: freshness of product, and what was written on the can itself. 
            <br />
            <br /> It's inconceivable that 
            <nobr>
             Coca-Cola
            </nobr> could have been continuing to bottle the original during its discontinuation phase, stockpiling it in anticipation of its reintroduction into the market. Soft drinks don't stay fizzy and sharp-flavored forever. Utilizing product from such a stockpile to meet a sudden high demand for the original product would have required the company to risk its reputation on a supply that might have died in the can. 
            <nobr>
             Coca-Cola
            </nobr> wouldn't have willingly risked following one marketing disaster with a second, particularly at a time when confidence in the company itself had been severely shaken. 
            <br />
            <br /> The can itself is another point against this theory. Prior to New Coke, 
            <nobr>
             Coca-Cola
            </nobr>'s flagship product was bottled and canned under a label of &quot;
            <nobr>
             Coca-Cola
            </nobr>.&quot; After its reintroduction, the soda's labels read &quot;Classic Coke.&quot; Claims by those who heard the original was bottled under the 
            <nobr>
             Coca-Cola
            </nobr> label during this period have to be dismissed, because any product so marked wouldn't have been sellable after the second coming. Harder to dismiss would be claims that Classic Coke cans were being produced during this period. Oddly enough, this rumor is the one that 
            <i>doesn't</i> surface. Whenever anyone claims to know someone who saw the cans, it's always the original 
            <nobr>
             &quot;Coca-Cola&quot;
            </nobr> cans they claim to have seen. 
            <br />
            <br /> These rumors are an attempt to make sense of the unthinkable, that a company of the size and reputation of 
            <nobr>
             Coca-Cola
            </nobr> could have effected such a blunder. It's more comforting to cast it all off as a brilliant conspiracy than to live with the notion that a large company might not be infallible. We defend our icons, and 
            <nobr>
             Coca-Cola
            </nobr> is as American an icon as there's ever been. Far better to admire its verve than to recognize its feet of clay. 
            <br />
            <br /> The company's rebound from this disaster was nothing short of a miracle (hence the persistent belief that the whole thing must have been planned). Yet it's a very understandable miracle once you think about it. It took the loss of the beverage people had grown up with and fallen in love over to remind them how much it meant to them. No longer taken for granted, 
            <nobr>
             Coca-Cola
            </nobr> had been reaffirmed in their affections. As for the debacle's being a deliberate marketing ploy, Donald Keough said: &quot;Some critics will say 
            <nobr>
             Coca-Cola
            </nobr> made a marketing mistake. Some cynics will say that we planned the whole thing. The truth is we are not that dumb, and we are not that smart.&quot; 
            <br />
            <br /> Barbara &quot;kvetch the wave&quot; Mikkelson 
            <br />
            <br /> 
            <font color="#5FA505"><b>Last updated:</b></font> &nbsp; 19 May 2011 
            <!--
2 May 1999 - original
13 March 2007 - reformatted
--> 
            <br />
            <br /> 
            <center>
             <font face="Verdana" size="2" color="#5FA505">Urban Legends Reference Pages &copy; 1995-2014 by snopes.com. <br /> This material may not be reproduced without permission. <br /> snopes and the snopes.com logo are registered service marks of snopes.com.</font>
            </center>
            <br /> 
           </div> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <table>
            <tbody>
             <tr>
              <td><img src="/images/template/icon-sources.gif" /></td>
              <td><h2>Sources:</h2></td>
             </tr>
            </tbody>
           </table> <font size="-1"> 
            <dl> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Allen, Frederick. &nbsp; 
               <i>Secret Formula.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; New York: HarperCollins, 1994. &nbsp; ISBN 0-88730-672-1 &nbsp; (pp. 388-425).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Gorman, John. &nbsp; &quot;Classic Case of Formula Forethought?&quot; 
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; 
               <i>Chicago Tribune.</i> &nbsp; 14 July 1985 &nbsp; (p. C3).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Greising, David. &nbsp; 
               <i>I'd Like the World to Buy a Coke.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; New York: John Wiley &amp; Sons, 1998. &nbsp; ISBN 0-471-19408-5.
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Pendergrast, Mark. &nbsp; 
               <i>For God, Country, and Coca-Cola.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; New York: Charles Scribner's Sons, 1993. &nbsp; ISBN 0-684-19347-7 &nbsp; (pp. 354-371).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Potts, Mark. &nbsp; &quot;Coca-Cola: Pausing to Refresh Formula?&quot; 
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; 
               <i>The Washington Post.</i> &nbsp; 23 April 1985 &nbsp; (p. C1).
              </nobr>
             </dd> 
            </dl> </font> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <center>
            <center> 
             <script type="text/javascript">
<!--
google_ad_client = "pub-6608306193529351";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_type = "text";
google_ad_channel = "0054321535";
google_color_border = "000000";
//-->
</script> 
             <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            </center> 
           </center> </td> 
          <td class="adColumn"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '160x600';
    kmn_placement = 'f829c9263630394d4aa365109f5e0c0c';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <div class="siteBottom">
     &nbsp; 
   </div> 
  </div> 
  <div class="siteFooterGradient">
   &nbsp;
  </div>   
 </body>
</html>