Cokelore	Cokelore	More than a century after the creation of Coca-Cola, we're still as much in love with this most famous of soft drinks as our great-grandparents were. Hold up a Coke, and you proclaim all that's best about the American way of life: Coca-Cola is first dates and shy kisses, a platoon of war-weary GIs getting letters from home, and an old, rusted sign creaking in the wind outside the hometown diner. Coca-Cola is also one of the most successful companies the world has ever known; nothing can be that big and popular, so much a part of everyday life, without having legends spring up around it. Coca-Cola's uniquely influential position in our culture has led to a special set of legends we call 'Cokelore'; a collection of Coke trivia and tall tales sure to refresh even the most informationally-parched reader.	http://www.snopes.com/cokelore/cokelore.asp	http://www.snopes.com/graphics/icons/main/coke.gif

The Reich Stuff	FALSE The Reich Stuff Fanta was invented by the Nazis.	http://www.snopes.com/cokelore/fanta.asp	http://www.snopes.com//images/red.gif
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <title>snopes.com: Fanta and the Nazis</title> 
  <link href="/style.css" rel="stylesheet" type="text/css" /> 
  <meta name="robots" content="INDEX,NOFOLLOW,NOARCHIVE" /> 
  <meta name="description" content="Was Fanta invented by the Nazis?" /> 
  <meta name="keywords" content="Fanta, Coke, Coca-Cola, Nazi, Hitler, Germany, urban legends archive, hoaxes, scams, rumors, urban myths, folklore, hoax slayer, mythbusters, misinformation, snopes" /> 
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> 
  <link rel="alternate" type="application/rss+xml" href="http://www.snopes.com/info/whatsnew.xml" title="RSS feed for snopes.com" /> 
  <link rel="image_src" href="http://www.snopes.com/logo-small.gif" /> 
  <script src="http://Q1MediaHydraPlatform.com/ads/video/unit_desktop_slider.php?eid=19503"></script>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script language="JavaScript">
if (window!=top){top.location.href=location.href;}
</script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40468225-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
 </head>     
 <body> 
  <script type="text/javascript">
<!--
var casaleD=new Date();var casaleR=(casaleD.getTime()%8673806982)+Math.random();
var casaleU=escape(window.location.href);
var casaleHost=' type="text/javascript" src="http://as.casalemedia.com/s?s=';
document.write('<scr'+'ipt'+casaleHost+'81847&amp;u=');
document.write(casaleU+'&amp;f=1&amp;id='+casaleR+'"><\/scr'+'ipt>');
//-->
</script> 
  <div class="siteWrapper"> 
   <table class="siteHeader" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="logo"><a href="http://www.snopes.com"><img src="/images/template/snopeslogo.gif" alt="snopes.com" /></a></td> 
      <td> 
       <table class="features"> 
        <tbody>
         <tr> 
          <td class="topAdvertisement"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '728x90';
    kmn_placement = 'cad674db7f73589c9a110884ce73bb72';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
         <tr> 
          <td class="searchArea"> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <!--
                    <TD VALIGN=TOP><IMG SRC="/images/search-hdr.gif" ALT="Search"></TD>
                    <TD VALIGN=TOP>

                      <FORM METHOD="GET" ACTION="http://search.atomz.com/search/"><INPUT TYPE=TEXT WIDTH=50 SIZE=60 ID="searchbox" NAME="sp-q">
                    </TD>
                    <TD>
                      <INPUT TYPE=IMAGE SRC="/images/search-btn-go.gif">
                      <INPUT TYPE=HIDDEN NAME="sp-a" VALUE="00062d45-sp00000000">
                      <INPUT TYPE=HIDDEN NAME="sp-advanced" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-p" VALUE="all">
                      <INPUT TYPE=HIDDEN NAME="sp-w-control" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-w" VALUE="alike">
                      <INPUT TYPE=HIDDEN NAME="sp-date-range" VALUE=-1>
                      <INPUT TYPE=HIDDEN NAME="sp-x" VALUE="any">
                      <INPUT TYPE=HIDDEN NAME="sp-c" VALUE=100>
                      <INPUT TYPE=HIDDEN NAME="sp-m" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-s" VALUE=0>
                      </FORM>
-->  
             </tr> 
            </tbody>
           </table> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <table class="siteMenu" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td><a href="/info/whatsnew.asp" onmouseover="window.status='What\'s New';return true" onmouseout="window.status='';return true"><img src="/images/menu-whats-new.gif" alt="What's New" /></a><a href="/info/random/random.asp"><img src="/images/menu-randomizer.gif" alt="Randomizer" /></a><a href="/info/top25uls.asp"><img src="/images/menu-hot-25.gif" alt="Hot 25" /></a><a href="/info/faq.asp"><img src="/images/menu-faq.gif" alt="FAQ" /></a><a href="/daily/"><img src="/images/menu-odd-news.gif" alt="Odd News" /></a><a href="/info/glossary.asp"><img src="/images/menu-glossary.gif" alt="Glossary" /></a><a href="/info/newsletter.asp"><img src="/images/menu-newsletter.gif" alt="Newsletter" /></a><a href="http://message.snopes.com" target="message"><img src="/images/menu-message-board.gif" alt="Message Board" /></a></td> 
     </tr> 
    </tbody>
   </table> 
   <noindex> 
    <table class="siteSubMenu" cellpadding="0" cellspacing="0"> 
     <tbody>
      <tr>
       <td> </td>
       <td width="534" class="breadcrumbs"><a href="/snopes.asp">Home</a> &gt; <a href="/cokelore/cokelore.asp#fanta">Cokelore</a>  &gt; The Reich Stuff?</td> 
       <td width="420" class="links"><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Contact Us';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-contact-us.gif" alt="Contact Us" /></a><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Submit a Rumor';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-submit-rumor.gif" alt="Submit a Rumor" /></a><a href="http://www.snopes.com/cgi-bin/comments/photovideo.asp" onmouseover="window.status='Submit a Photo/Video';return true" onmouseout="window.status='';return true" target="photo"><img src="/images/sub-menu-submit-photo.gif" alt="Submit a Photo/Video" /></a> </td> 
      </tr> 
     </tbody>
    </table> 
   </noindex> 
   <table class="siteContent" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="contentArea"> 
       <table class="threeColumn" cellpadding="0" cellspacing="0"> 
        <tbody>
         <tr valign="TOP"> 
          <td class="navColumn"> <a href="https://twitter.com/snopes" class="twitter-follow-button" data-show-count="false">Follow @snopes</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script> <p></p> 
           <div class="navHeader">
            CATEGORIES:
           </div> 
           <ul> 
            <li><a href="/autos/autos.asp">Autos</a></li> 
            <li><a href="/business/business.asp">Business</a></li> 
            <li><a href="/cokelore/cokelore.asp">Cokelore</a></li> 
            <li><a href="/college/college.asp">College</a></li> 
            <li><a href="/computer/computer.asp">Computers</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/crime/crime.asp">Crime</a></li> 
            <li><a href="/critters/critters.asp">Critter Country</a></li> 
            <li><a href="/disney/disney.asp">Disney</a></li> 
            <li><a href="/embarrass/embarrass.asp">Embarrassments</a></li> 
            <li><a href="/photos/photos.asp">Fauxtography</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/food/food.asp">Food</a></li> 
            <li><a href="/fraud/fraud.asp">Fraud &amp; Scams</a></li> 
            <li><a href="/glurge/glurge.asp">Glurge Gallery</a></li> 
            <li><a href="/history/history.asp">History</a></li> 
            <li><a href="/holidays/holidays.asp">Holidays</a></li> 
            <li><a href="/horrors/horrors.asp">Horrors</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/humor/humor.asp">Humor</a></li> 
            <li><a href="/inboxer/inboxer.asp">Inboxer Rebellion</a></li> 
            <li><a href="/language/language.asp">Language</a></li> 
            <li><a href="/legal/legal.asp">Legal</a></li> 
            <li><a href="/lost/lost.asp">Lost Legends</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/love/love.asp">Love</a></li> 
            <li><a href="/luck/luck.asp">Luck</a></li> 
            <li><a href="/media/media.asp">Media Matters</a></li> 
            <li><a href="/medical/medical.asp">Medical</a></li> 
            <li><a href="/military/military.asp">Military</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/movies/movies.asp">Movies</a></li> 
            <li><a href="/music/music.asp">Music</a></li> 
            <li><a href="/oldwives/oldwives.asp">Old Wives' Tales</a></li> 
            <li><a href="/politics/politics.asp">Politics</a></li> 
            <li><a href="/pregnant/pregnant.asp">Pregnancy</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/quotes/quotes.asp">Quotes</a></li> 
            <li><a href="/racial/racial.asp">Racial Rumors</a></li> 
            <li><a href="/radiotv/radiotv.asp">Radio &amp; TV</a></li> 
            <li><a href="/religion/religion.asp">Religion</a></li> 
            <li><a href="/risque/risque.asp">Risqu&eacute; Business</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/science/science.asp">Science</a></li> 
            <li><a href="/rumors/rumors.asp">September 11</a></li> 
            <li><a href="/sports/sports.asp">Sports</a></li> 
            <li><a href="/travel/travel.asp">Travel</a></li> 
            <li><a href="/weddings/weddings.asp">Weddings</a></li> 
           </ul> </td> 
          <td class="contentColumn" align="LEFT"> <br /> <a href="mailto:Insert address(es) here?subject=Fanta and the Nazis&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/fanta.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><img src="/images/envelope.gif" alt="E-mail this page" /></a> <a href="mailto:Insert address(es) here?subject=Fanta and the Nazis&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/fanta.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><font size="2" color="#0000FF">E-mail this</font></a> 
           <!--
 &nbsp; &nbsp; &nbsp; &nbsp; <div class="fb-like" data-href="https://www.facebook.com/pages/snopescom/241061082705085" data-send="false" data-width="110" data-show-faces="false"></div>
--> <br /><br /> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <td valign="TOP"><img src="/images/search-hdr.gif" alt="Search" /></td> 
              <td valign="TOP"> 
               <form method="GET" action="http://search.atomz.com/search/">
                <input type="TEXT" width="50" size="60" id="searchbox" name="sp-q" /> 
               </form></td> 
              <td> <input type="IMAGE" src="/images/search-btn-go.gif" /> <input type="HIDDEN" name="sp-a" value="00062d45-sp00000000" /> <input type="HIDDEN" name="sp-advanced" value="1" /> <input type="HIDDEN" name="sp-p" value="all" /> <input type="HIDDEN" name="sp-w-control" value="1" /> <input type="HIDDEN" name="sp-w" value="alike" /> <input type="HIDDEN" name="sp-date-range" value="-1" /> <input type="HIDDEN" name="sp-x" value="any" /> <input type="HIDDEN" name="sp-c" value="100" /> <input type="HIDDEN" name="sp-m" value="1" /> <input type="HIDDEN" name="sp-s" value="0" />  </td> 
             </tr> 
            </tbody>
           </table> <br /><br /> 
           <div class="utilityLinks"> 
            <!-- code goes here --> 
            <div class="pw-widget pw-counter-vertical"> 
             <a class="pw-button-googleplus pw-look-native"></a> 
             <a class="pw-button-tumblr pw-look-native"></a> 
             <a class="pw-button-pinterest pw-look-native"></a> 
             <a class="pw-button-reddit pw-look-native"></a> 
             <a class="pw-button-email pw-look-native"></a> 
             <a class="pw-button-facebook pw-look-native"></a> 
             <a class="pw-button-twitter pw-look-native"></a> 
            </div> 
            <script src="http://i.po.st/static/v3/post-widget.js#publisherKey=snopescom1152" type="text/javascript"></script> 
            <!-- code goes here --> 
           </div> <br /> <h1>The Reich Stuff?</h1> <style type="text/css">
<!--
 A:link, A:visited, A:active { text-decoration: underline; }
 A:link {color: #0000FF;}
 A:visited {color: #5fa505;}
 A:hover {color: #FF0000;} 
 .article_text {text-align: justify;} 
-->
</style> 
           <div class="article_text"> 
            <font color="#5FA505"><b>Claim:</b></font> &nbsp; Fanta was invented by the Nazis. 
            <br />
            <br /> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <noindex>
             <table>
              <tbody>
               <tr>
                <td valign="CENTER"><img src="/images/red.gif" /></td>
                <td valign="TOP"><font size="5" color="#37628D"><b>FALSE</b></font></td>
               </tr>
              </tbody>
             </table>
            </noindex> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <br /> 
            <font color="#5FA505"><b>Example:</b> &nbsp; <i>[Collected on the Internet, 2002]</i></font> 
            <br />
            <br /> 
            <div class="quoteBlock">
              I've heard a number of times that Fanta (which I think is owned by Coke) was created by Coca Cola in order to enable the company to sell soft drinks to the Nazis during World 
             <nobr>
              War II
             </nobr> without having to worry about anyone seeing Hitler or Goebbels &quot;having a coke and a smile.&quot; 
            </div> 
            <div class="quoteBlockEnd">
              &nbsp; 
            </div> 
            <br /> 
            <font color="#5FA505"><b>Origins:</b></font> &nbsp; The histories of a number of popular consumer items have been rumored to have at least tenuous connections with certain unsavory elements. Contemporary lore is 
            <img src="graphics/fanta.jpg" alt="Fanta" title="Fanta" width="175" height="176" border="0" align="LEFT" hspace="16" vspace="16" /> rife with product rumors that assert ties to the Ku Klux Klan (e.g., 
            <a href="/business/alliance/marlboro.asp" onmouseover="window.status='Welcome to Marlboro 
KKKountry';return true" onmouseout="window.status='';return true" target="marlboro">Marlboro</a> cigarettes, 
            <a href="/business/alliance/snapple.asp" onmouseover="window.status='Snapple Dragoon';return true" onmouseout="window.status='';return true" target="snapple">Snapple</a> fruit drinks, 
            <a href="/business/alliance/sanders.asp" onmouseover="window.status='No Colonel of Truth';return true" onmouseout="window.status='';return true" target="kfc">KFC</a>, 
            <a href="/business/alliance/troop.asp" onmouseover="window.status='Troop Loops';return true" onmouseout="window.status='';return true" target="troop">Troop</a> clothing, 
            <a href="/racial/business/tropical.asp" onmouseover="window.status='Tropical Storm';return true" onmouseout="window.status='';return true" target="tropical">Tropical Fantasy</a> fruit drink) and the Nazis (e.g., 
            <a href="/business/alliance/coors.asp" onmouseover="window.status='Coors';return true" onmouseout="window.status='';return true" target="coors">Coors</a> beer), groups mainstream American society views as evil. Such rumors are wholly without substance. 
            <br />
            <br /> Of all the product rumors of this class, only those associating the soft drink Fanta with Nazi Germany have anything to them, and even then, the truth of the matter is far more innocuous than the whispers. 
            <br />
            <br /> We've seen the Fanta/Nazi rumor rendered a number of ways, including: 
            <ul type="DISC"> 
             <li> Fanta was invented <u>by</u> the Third Reich, because other soft drinks (including 
              <nobr>
               Coca-Cola)
              </nobr> were no longer available in Germany &#x2014; the Nazis longed for something fizzy to drink and so had to come up with something on their own.</li> 
             <p></p> 
             <li> Fanta was formulated by Coca-Cola <u>for</u> the Nazis, because the political climate of those days made it akin to corporate suicide to attempt to supply the Allies' enemy with the same drink the Allies were gulping down.</li> 
             <p></p> 
             <li> Back in those war-torn days, Fanta and 
              <nobr>
               Coca-Cola
              </nobr> were actually the same beverage, but were labeled and distributed under different names so as to keep the Allies from knowing the Nazis were guzzling the same product.</li> 
             <p></p> 
             <li> Fanta was invented in Germany when the war made it difficult to get 
              <nobr>
               Coca-Cola
              </nobr> syrup from the USA to Germany.</li> 
            </ul> It is the last that is closest to the truth; the other three are naught but canard. 
            <br />
            <br /> Prior to the outbreak of the second world war, Coca-Cola's only unqualified success on the international scene was its bottling operations in Nazi Germany. Sales records were being set year after year in that venue, and by 1939 
            <nobr>
             Coca-Cola
            </nobr> had 
            <nobr>
             43 bottling
            </nobr> plants and more than 
            <nobr>
             600 local
            </nobr> distributors in that country. 
            <br />
            <br /> However, the war was about to change that. As the inevitable clash loomed ever closer, obtaining the key ingredients necessary for the production of Coca-Cola syrup became increasingly difficult in Germany, grinding production towards a standstill. 
            <br />
            <br /> In 1938, the man in charge of Coca-Cola's operations in Germany, American-born Ray Powers, died of injuries received in an automobile accident. His right-hand man, German-born Max Keith, took over: 
            <br /> 
            <font face="Verdana" size="2"> 
             <div style="text-align: justify; margin-top: 20px; margin-bottom: 20px; margin-left: 40px; margin-right: 40px"> 
              <font color="#5FA505" face=""><i>[Allen, 1994]</i></font> 
              <br />
              <br /> Meanwhile, the German government placed Max Keith in charge of 
              <nobr>
               Coca-Cola's
              </nobr> properties in the occupied countries, and he sent word through 
              <nobr>
               Coca-Cola's
              </nobr> bottler in neutral Switzerland that he would try to keep the enterprises alive. But with no means of getting ingredients, Keith stopped making 
              <nobr>
               Coca-Cola
              </nobr> and began marketing an entirely new soft drink he called Fanta, a light-colored beverage that resembled ginger ale. 
             </div> </font> Fanta came by its name thanks to Keith's instructions to employees during the contest to christen the beverage &#x2014; he told them to let their 
            <i>Fantasie</i> [Geman for 
            <i>fantasy</i>] run wild. Upon hearing that, veteran salesman Joe Knipp immediately blurted out 
            <i>Fanta</i>. 
            <br />
            <br /> This new soda was often made from the leavings of other food industries. (Remember, Germany did have a bit of an import problem at that time.) Whey (a cheese 
            <nobr>
             by-product)
            </nobr> and apple fiber from cider presses found their way into the drink. As for which fruits were used in the formulation, it all depended on what was available at the time. In its earliest incarnations, the drink was sweetened with saccharin, but by 1941 its concocters were permitted to use 
            <nobr>
             3.5 percent
            </nobr> beet 
            <table align="RIGHT" cellpadding="12" border="0">
             <tbody>
              <tr>
               <td> 
                <center> 
                 <script type="text/javascript"><!--
    kmn_size = '300x250';
    kmn_placement = '1c8834441f20e22a0aef009c095a6cab';
//--></script> 
                 <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
                </center> </td>
              </tr>
             </tbody>
            </table> sugar. 
            <br />
            <br /> Fanta sold well enough to keep the plants operating and 
            <nobr>
             Coca-Cola
            </nobr> people employed. In 1943, 
            <nobr>
             3 million
            </nobr> cases of Fanta were vended, but not all were imbibed &#x2014; some were used to flavor soups and stews. (Sugar rationing inspired many a housewife to look to unusual sources for that which could no longer be bought outright in large enough quantities to satisfy.) 
            <br />
            <br /> Until the end of the war, Coca-Cola executives in Atlanta did not know if Keith was working for the company or for the Nazis, because communication with him was impossible. Their misgivings aside, Keith was safeguarding 
            <nobr>
             Coca-Cola
            </nobr> interests and people during that period of no contact. It was thanks largely to his efforts that 
            <nobr>
             Coca-Cola
            </nobr> was able to 
            <nobr>
             re-establish
            </nobr> production in Germany virtually immediately after World 
            <nobr>
             War II.
            </nobr> 
            <br />
            <br /> According to a report prepared by an investigator commissioned by 
            <nobr>
             Coca-Cola
            </nobr> to examine Max Keith's actions during that unsupervised period, Keith had never been a Nazi, even though he'd been repeatedly pressured to become one and indeed had endured hardships because of his refusal. He also could have made a fortune for himself by bottling and selling Fanta under his own name. Instead, in the face of having to work for the German government, he kept the 
            <nobr>
             Coca-Cola
            </nobr> plants in Germany running and various 
            <nobr>
             Coca-Cola
            </nobr> men alive throughout the war. At the end of the conflict, he welcomed the 
            <nobr>
             Coca-Cola
            </nobr> company back to its German operations and handed over both the profits from the war years and the new soft drink. 
            <br />
            <br /> So where does all this leave the question of who or what invented Fanta and why? The truth is simple, even if it doesn't run trippingly off the tongue: Fanta was the creation of a German-born 
            <nobr>
             Coca-Cola
            </nobr> man who was acting without direction from Atlanta. This man wasn't a Nazi, nor did he invent the drink at the direction of the Third Reich. Rather, in an effort to preserve Coca-Cola company assets and protect its people by way of keeping local plants operating, he formulated a new soft drink when it became impossible to produce the company's flagship product. 
            <br />
            <br /> Fanta is still a Coca-Cola product, and today it comes in seventy different flavors (though only some are available within each of the 
            <nobr>
             188 countries
            </nobr> it is sold in). 
            <br />
            <br /> Barbara &quot;soda coda&quot; Mikkelson 
            <br />
            <br /> 
            <font color="#5FA505"><b>Last updated:</b></font> &nbsp; 29 April 2011 
            <!--
13 September 2004 - original
29 April 2011: reformatted
--> 
            <br />
            <br /> 
            <center>
             <font face="Verdana" size="2" color="#5FA505">Urban Legends Reference Pages &copy; 1995-2014 by snopes.com. <br /> This material may not be reproduced without permission. <br /> snopes and the snopes.com logo are registered service marks of snopes.com.</font>
            </center>
            <br /> 
           </div> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <table>
            <tbody>
             <tr>
              <td><img src="/images/template/icon-sources.gif" /></td>
              <td><h2>Sources:</h2></td>
             </tr>
            </tbody>
           </table> <font size="-1"> 
            <dl> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Allen, Frederick. &nbsp; 
               <i>Secret Formula.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; New York: HarperCollins, 1994. &nbsp; ISBN 0-88730-672-1 &nbsp; (pp. 254, 263-265).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Pendergrast, Mark. &nbsp; 
               <i>For God, Country, and Coca-Cola.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; New York: Charles Scribner's Sons, 1993. &nbsp; ISBN 0-684-19347-7 &nbsp; (pp. 213-226).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Roush, Chris. &nbsp; &quot;And in Third Place ... It's Fanta.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; 
               <i>The Atlanta Journal and Constitution.</i> &nbsp; 26 July 1996.
              </nobr>
             </dd> 
            </dl> </font> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <center>
            <center> 
             <script type="text/javascript">
<!--
google_ad_client = "pub-6608306193529351";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_type = "text";
google_ad_channel = "0054321535";
google_color_border = "000000";
//-->
</script> 
             <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            </center> 
           </center> </td> 
          <td class="adColumn"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '160x600';
    kmn_placement = 'f829c9263630394d4aa365109f5e0c0c';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <div class="siteBottom">
     &nbsp; 
   </div> 
  </div> 
  <div class="siteFooterGradient">
   &nbsp;
  </div>   
 </body>
</html>