Cokelore	Cokelore	More than a century after the creation of Coca-Cola, we're still as much in love with this most famous of soft drinks as our great-grandparents were. Hold up a Coke, and you proclaim all that's best about the American way of life: Coca-Cola is first dates and shy kisses, a platoon of war-weary GIs getting letters from home, and an old, rusted sign creaking in the wind outside the hometown diner. Coca-Cola is also one of the most successful companies the world has ever known; nothing can be that big and popular, so much a part of everyday life, without having legends spring up around it. Coca-Cola's uniquely influential position in our culture has led to a special set of legends we call 'Cokelore'; a collection of Coke trivia and tall tales sure to refresh even the most informationally-parched reader.	http://www.snopes.com/cokelore/cokelore.asp	http://www.snopes.com/graphics/icons/main/coke.gif

Design Err Shape	PARTLY TRUE Design Err Shape The shape of the Coca-Cola bottle was based on the cacao tree seed pod.	http://www.snopes.com/cokelore/bottle.asp	http://www.snopes.com//images/mostlytrue.gif
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <title>snopes.com: Coca-Cola Bottle Shape</title> 
  <link href="/style.css" rel="stylesheet" type="text/css" /> 
  <meta name="robots" content="INDEX,NOFOLLOW,NOARCHIVE" /> 
  <meta name="description" content="Was the shape of the Coca-Cola bottle mistakenly based on the cacoa tree seed pod?" /> 
  <meta name="keywords" content="Coca-Cola, Coke, bottle, shape, cacao, design, hobble skirt, urban legends archive, hoaxes, scams, rumors, urban myths, folklore, hoax slayer, mythbusters, misinformation, snopes" /> 
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> 
  <link rel="alternate" type="application/rss+xml" href="http://www.snopes.com/info/whatsnew.xml" title="RSS feed for snopes.com" /> 
  <link rel="image_src" href="http://www.snopes.com/logo-small.gif" /> 
  <script src="http://Q1MediaHydraPlatform.com/ads/video/unit_desktop_slider.php?eid=19503"></script>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script language="JavaScript">
if (window!=top){top.location.href=location.href;}
</script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40468225-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
 </head>     
 <body> 
  <script type="text/javascript">
<!--
var casaleD=new Date();var casaleR=(casaleD.getTime()%8673806982)+Math.random();
var casaleU=escape(window.location.href);
var casaleHost=' type="text/javascript" src="http://as.casalemedia.com/s?s=';
document.write('<scr'+'ipt'+casaleHost+'81847&amp;u=');
document.write(casaleU+'&amp;f=1&amp;id='+casaleR+'"><\/scr'+'ipt>');
//-->
</script> 
  <div class="siteWrapper"> 
   <table class="siteHeader" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="logo"><a href="http://www.snopes.com"><img src="/images/template/snopeslogo.gif" alt="snopes.com" /></a></td> 
      <td> 
       <table class="features"> 
        <tbody>
         <tr> 
          <td class="topAdvertisement"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '728x90';
    kmn_placement = 'cad674db7f73589c9a110884ce73bb72';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
         <tr> 
          <td class="searchArea"> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <!--
                    <TD VALIGN=TOP><IMG SRC="/images/search-hdr.gif" ALT="Search"></TD>
                    <TD VALIGN=TOP>

                      <FORM METHOD="GET" ACTION="http://search.atomz.com/search/"><INPUT TYPE=TEXT WIDTH=50 SIZE=60 ID="searchbox" NAME="sp-q">
                    </TD>
                    <TD>
                      <INPUT TYPE=IMAGE SRC="/images/search-btn-go.gif">
                      <INPUT TYPE=HIDDEN NAME="sp-a" VALUE="00062d45-sp00000000">
                      <INPUT TYPE=HIDDEN NAME="sp-advanced" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-p" VALUE="all">
                      <INPUT TYPE=HIDDEN NAME="sp-w-control" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-w" VALUE="alike">
                      <INPUT TYPE=HIDDEN NAME="sp-date-range" VALUE=-1>
                      <INPUT TYPE=HIDDEN NAME="sp-x" VALUE="any">
                      <INPUT TYPE=HIDDEN NAME="sp-c" VALUE=100>
                      <INPUT TYPE=HIDDEN NAME="sp-m" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-s" VALUE=0>
                      </FORM>
-->  
             </tr> 
            </tbody>
           </table> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <table class="siteMenu" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td><a href="/info/whatsnew.asp" onmouseover="window.status='What\'s New';return true" onmouseout="window.status='';return true"><img src="/images/menu-whats-new.gif" alt="What's New" /></a><a href="/info/random/random.asp"><img src="/images/menu-randomizer.gif" alt="Randomizer" /></a><a href="/info/top25uls.asp"><img src="/images/menu-hot-25.gif" alt="Hot 25" /></a><a href="/info/faq.asp"><img src="/images/menu-faq.gif" alt="FAQ" /></a><a href="/daily/"><img src="/images/menu-odd-news.gif" alt="Odd News" /></a><a href="/info/glossary.asp"><img src="/images/menu-glossary.gif" alt="Glossary" /></a><a href="/info/newsletter.asp"><img src="/images/menu-newsletter.gif" alt="Newsletter" /></a><a href="http://message.snopes.com" target="message"><img src="/images/menu-message-board.gif" alt="Message Board" /></a></td> 
     </tr> 
    </tbody>
   </table> 
   <noindex> 
    <table class="siteSubMenu" cellpadding="0" cellspacing="0"> 
     <tbody>
      <tr>
       <td> </td>
       <td width="534" class="breadcrumbs"><a href="/snopes.asp">Home</a> &gt; <a href="/cokelore/cokelore.asp#bottle">Cokelore</a> &gt; Design Err Shape</td> 
       <td width="420" class="links"><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Contact Us';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-contact-us.gif" alt="Contact Us" /></a><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Submit a Rumor';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-submit-rumor.gif" alt="Submit a Rumor" /></a><a href="http://www.snopes.com/cgi-bin/comments/photovideo.asp" onmouseover="window.status='Submit a Photo/Video';return true" onmouseout="window.status='';return true" target="photo"><img src="/images/sub-menu-submit-photo.gif" alt="Submit a Photo/Video" /></a> </td> 
      </tr> 
     </tbody>
    </table> 
   </noindex> 
   <table class="siteContent" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="contentArea"> 
       <table class="threeColumn" cellpadding="0" cellspacing="0"> 
        <tbody>
         <tr valign="TOP"> 
          <td class="navColumn"> <a href="https://twitter.com/snopes" class="twitter-follow-button" data-show-count="false">Follow @snopes</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script> <p></p> 
           <div class="navHeader">
            CATEGORIES:
           </div> 
           <ul> 
            <li><a href="/autos/autos.asp">Autos</a></li> 
            <li><a href="/business/business.asp">Business</a></li> 
            <li><a href="/cokelore/cokelore.asp">Cokelore</a></li> 
            <li><a href="/college/college.asp">College</a></li> 
            <li><a href="/computer/computer.asp">Computers</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/crime/crime.asp">Crime</a></li> 
            <li><a href="/critters/critters.asp">Critter Country</a></li> 
            <li><a href="/disney/disney.asp">Disney</a></li> 
            <li><a href="/embarrass/embarrass.asp">Embarrassments</a></li> 
            <li><a href="/photos/photos.asp">Fauxtography</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/food/food.asp">Food</a></li> 
            <li><a href="/fraud/fraud.asp">Fraud &amp; Scams</a></li> 
            <li><a href="/glurge/glurge.asp">Glurge Gallery</a></li> 
            <li><a href="/history/history.asp">History</a></li> 
            <li><a href="/holidays/holidays.asp">Holidays</a></li> 
            <li><a href="/horrors/horrors.asp">Horrors</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/humor/humor.asp">Humor</a></li> 
            <li><a href="/inboxer/inboxer.asp">Inboxer Rebellion</a></li> 
            <li><a href="/language/language.asp">Language</a></li> 
            <li><a href="/legal/legal.asp">Legal</a></li> 
            <li><a href="/lost/lost.asp">Lost Legends</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/love/love.asp">Love</a></li> 
            <li><a href="/luck/luck.asp">Luck</a></li> 
            <li><a href="/media/media.asp">Media Matters</a></li> 
            <li><a href="/medical/medical.asp">Medical</a></li> 
            <li><a href="/military/military.asp">Military</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/movies/movies.asp">Movies</a></li> 
            <li><a href="/music/music.asp">Music</a></li> 
            <li><a href="/oldwives/oldwives.asp">Old Wives' Tales</a></li> 
            <li><a href="/politics/politics.asp">Politics</a></li> 
            <li><a href="/pregnant/pregnant.asp">Pregnancy</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/quotes/quotes.asp">Quotes</a></li> 
            <li><a href="/racial/racial.asp">Racial Rumors</a></li> 
            <li><a href="/radiotv/radiotv.asp">Radio &amp; TV</a></li> 
            <li><a href="/religion/religion.asp">Religion</a></li> 
            <li><a href="/risque/risque.asp">Risqu&eacute; Business</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/science/science.asp">Science</a></li> 
            <li><a href="/rumors/rumors.asp">September 11</a></li> 
            <li><a href="/sports/sports.asp">Sports</a></li> 
            <li><a href="/travel/travel.asp">Travel</a></li> 
            <li><a href="/weddings/weddings.asp">Weddings</a></li> 
           </ul> </td> 
          <td class="contentColumn" align="LEFT"> <br /> <a href="mailto:Insert address(es) here?subject=Coca-Cola Bottle Shape&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/bottle.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><img src="/images/envelope.gif" alt="E-mail this page" /></a> <a href="mailto:Insert address(es) here?subject=Coca-Cola Bottle Shape&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/bottle.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><font size="2" color="#0000FF">E-mail this</font></a> 
           <!--
 &nbsp; &nbsp; &nbsp; &nbsp; <div class="fb-like" data-href="https://www.facebook.com/pages/snopescom/241061082705085" data-send="false" data-width="110" data-show-faces="false"></div>
--> <br /><br /> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <td valign="TOP"><img src="/images/search-hdr.gif" alt="Search" /></td> 
              <td valign="TOP"> 
               <form method="GET" action="http://search.atomz.com/search/">
                <input type="TEXT" width="50" size="60" id="searchbox" name="sp-q" /> 
               </form></td> 
              <td> <input type="IMAGE" src="/images/search-btn-go.gif" /> <input type="HIDDEN" name="sp-a" value="00062d45-sp00000000" /> <input type="HIDDEN" name="sp-advanced" value="1" /> <input type="HIDDEN" name="sp-p" value="all" /> <input type="HIDDEN" name="sp-w-control" value="1" /> <input type="HIDDEN" name="sp-w" value="alike" /> <input type="HIDDEN" name="sp-date-range" value="-1" /> <input type="HIDDEN" name="sp-x" value="any" /> <input type="HIDDEN" name="sp-c" value="100" /> <input type="HIDDEN" name="sp-m" value="1" /> <input type="HIDDEN" name="sp-s" value="0" />  </td> 
             </tr> 
            </tbody>
           </table> <br /><br /> 
           <div class="utilityLinks"> 
            <!-- code goes here --> 
            <div class="pw-widget pw-counter-vertical"> 
             <a class="pw-button-googleplus pw-look-native"></a> 
             <a class="pw-button-tumblr pw-look-native"></a> 
             <a class="pw-button-pinterest pw-look-native"></a> 
             <a class="pw-button-reddit pw-look-native"></a> 
             <a class="pw-button-email pw-look-native"></a> 
             <a class="pw-button-facebook pw-look-native"></a> 
             <a class="pw-button-twitter pw-look-native"></a> 
            </div> 
            <script src="http://i.po.st/static/v3/post-widget.js#publisherKey=snopescom1152" type="text/javascript"></script> 
            <!-- code goes here --> 
           </div> <br /> <h1>Design Err Shape</h1> <style type="text/css">
<!--
 A:link, A:visited, A:active { text-decoration: underline; }
 A:link {color: #0000FF;}
 A:visited {color: #5fa505;}
 A:hover {color: #FF0000;} 
 .article_text {text-align: justify;} 
-->
</style> 
           <div class="article_text"> 
            <font color="#5FA505"><b>Claim:</b></font> &nbsp; The shape of the Coca-Cola bottle was mistakenly based on the cacao tree seed pod. 
            <br />
            <br /> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <noindex>
             <table>
              <tbody>
               <tr>
                <td valign="CENTER"><img src="/images/mixture.gif" /></td>
                <td valign="TOP"><font size="5" color="#37628D"><b>MIXTURE OF TRUE AND FALSE INFORMATION:</b></font></td>
               </tr>
              </tbody>
             </table> 
             <br /> 
             <table width="90%" align="CENTER"> 
              <tbody>
               <tr> 
                <td valign="CENTER"><img src="/images/green.gif" /></td>
                <td valign="TOP"><font color="#37628D"><b>TRUE:</b></font> The design of the &quot;hobble skirt&quot; Coca-Cola bottle was based on the cacao pod.</td> 
               </tr> 
               <tr>
                <td> &nbsp; </td>
               </tr> 
               <tr> 
                <td valign="CENTER"><img src="/images/red.gif" /></td>
                <td valign="TOP"><font color="#37628D"><b>FALSE:</b></font> The cacao pod was chosen by mistake after someone ended up on the wrong page of the encyclopedia.</td> 
               </tr> 
              </tbody>
             </table> 
            </noindex> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <br /> 
            <font color="#5FA505"><b>Origins:</b></font> &nbsp; The unmistakable curved shape of its bottle has been part of Coca-Cola's image since the original glass contour bottle was introduced throughout the United States in 1916. To this day people wonder where the design came from, and some odd theories about its origins have surfaced over the years, including one about its being modeled after a Victorian hooped dress. 
            <br />
            <br /> (One popular misconception credits the bottle's unique form to famed industrial designer Raymond Loewy. That one is easily dismissed as Loewy didn't arrive in America until 1919, three years 
            <u>after</u> what came to be called the &quot;hobble skirt&quot; bottle was in wide distribution. Loewy later designed products for 
            <nobr>
             Coca-Cola
            </nobr> such as cans, coolers, and vending machines, which likely led to this misconception. He also called the Coke bottle &quot;the most perfectly designed package in the world,&quot; and it's unlikely he'd have been that arrogant if he were describing his own design.) 
            <br />
            <br /> None of the false theories is any more odd than what the Coca-Cola company itself claims as the correct origin, however. 
            <br />
            <br /> Back in 1915, soda bottles were pretty much the same shape no matter which beverage they contained. What differentiated one unopened soda from another was its label. That was a workable system, except for one problem: paper labels slid off when the bottles they were affixed to became wet. 
            <br />
            <br /> In those long-ago days, keeping soft drinks chilled was typically accomplished by leaving them in tubs of ice water. This led to confusion and frustration as customers blindly fished around in cold water for the brands they wanted. Finally, a light bulb went off over someone's head: what if 
            <nobr>
             Coca-Cola's
            </nobr> bottle had an unusual shape? The days of brand 
            <table align="RIGHT" cellpadding="12" border="0">
             <tbody>
              <tr>
               <td> 
                <center> 
                 <script type="text/javascript"><!--
    kmn_size = '300x250';
    kmn_placement = '1c8834441f20e22a0aef009c095a6cab';
//--></script> 
                 <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
                </center> </td>
              </tr>
             </tbody>
            </table> confusion would be over, because a customer could easily pick out an ice-cold Coke by feel alone. 
            <br />
            <br /> So much for the decision to shape the bottle differently; now on to where this particular shape came from. Though I grant this is a hard one to swallow, 
            <nobr>
             Coca-Cola
            </nobr> swears it happened like this: 
            <br />
            <br /> A heat wave shut down operations at the Root Glass Company of Terre Haute, Indiana, one of 
            <nobr>
             Coca-Cola's
            </nobr> bottle suppliers. As a result, there was free time to be frittered away on odd projects. Plant manager Alex Samuelson was puzzling over the Coke bottle design problem when he was struck by an inspiration. What if the bottle were made to resemble the shape of either a kola nut or a coca leaf, the two ingredients the flagship product had been named for? 
            <br />
            <br /> He dispatched one of his employees (a fellow by the name of Clyde Edwards) off to the city library to look up information about those two items. A misunderstanding occurred, leading Edwards to the wrong page of the Encyclopedia Britannica. The sketches he returned with were turned into the bottle design we know and love so much today, but the vertical striations and curved, bulging middle bore no resemblance whatsoever to either the coca leaf or the kola nut. Instead, they were a darned good rendition of a cacao tree seed pod. Yes, cacao. As in chocolate. 
            <br />
            <br /> While this story has been related in books, and with my own ears I've heard it told as gospel at both the 
            <nobr>
             Coca-Cola
            </nobr> Museum in 
            <nobr>
             Las Vegas
            </nobr> and its parent museum in Atlanta, it's not the actual truth. Instead, the preceding account is a highly colored version of what actually occurred. (Mystique overshadows truth in other 
            <nobr>
             Coca-Cola
            </nobr> origin tales, such as the company's claim that the beverage was 
            <a href="/cokelore/carbonate.asp" onmouseover="window.status='Fortuitous Fizz';return true" onmouseout="window.status='';return true" target="fizz">carbonated</a> by accident.) 
            <br />
            <br /> Instead, acutely aware of the need for a better container for its product, in 1915 
            <nobr>
             Coca-Cola
            </nobr> sponsored a design competition among its bottle suppliers. The contest listed only two requirements: the bottle had to be distinctive and had to fit existing equipment. While the Root Glass Company of Terre Haute's plant manager Alex Samuelson and plant employee 
            <img src="graphics/bottle.jpg" align="RIGHT" hspace="16" vspace="16" alt="Cacao pods" width="311" height="200" border="1" /> Clyde Edwards were tangentially involved in the process that led to the design of what became the quintessential Coca-Cola bottle, the actual designer and the man who ultimately turned his own artistic concept into the prototype bottle was 
            <nobr>
             Earl R.
            </nobr> Dean, the supervisor of the bottle molding room at that facility. Moreover, there was no &quot;happy accident&quot; of mistaking one page of the encyclopedia for another. Dean, inspired by Samuelson's query about what made up 
            <nobr>
             Coca-Cola,
            </nobr> posited that there might be a bottle design idea to be found in the look of either the coca plant or the kola nut (the two ingredients Coca-Cola is named for), and set out for the local library to unearth information about those two ingredients with Clyde Edwards in tow. (As the auditor at Root Glass, Edwards knew his way around the library and therefore was thought likely to be able to assist Dean in his search.) 
            <br />
            <br /> There was no information to be found about either the coca leaf or the kola nut at the local library, which scotched that plan. However, a picture of a cacao pod in the Encyclopedia Britannica gave Dean a further jolt of inspiration: he realized that the striations on that rather odd-looking gourd-like thing might serve as the jumping-off point for a workable bottle design. He sketched a copy of the drawing, took it back to the plant, worked up a bottle design based upon it, presented it to 
            <nobr>
             C.J. Root
            </nobr> (the plant's owner), gained his approval, created a mold for the prototype bottle, and produced one. That bottle became the widely-recognized symbol of 
            <nobr>
             Coca-Cola.
            </nobr> 
            <br />
            <br /> In the actual story of the bottle, there are no fortuitous accidents that work to impel Fate (i.e., no heat wave that shuts down plant operations, leaving folks with idle time in which to dream up a better bottle), and no dumb mistakes that turn out well (i.e., no dunderheaded confusion of one page of the encyclopedia for another). The tale is instead one of a design concept that didn't quite pan out (dearth of information about the coca leaf or the kola nut, causing the idea of basing the bottle on either of them to be scrapped), leaving the 
            <nobr>
             would-be
            </nobr> designer hunting for something else that might work, and in the process happening upon the look of the cacao pod. 
            <br />
            <br /> Barbara &quot;milked chocolate&quot; Mikkelson 
            <br />
            <br /> 
            <font color="#5FA505"><b>Last updated:</b></font> &nbsp; 3 February 2011 
            <!--
2 May 1999 - original
13 March 2007 - reformatted
26 December 2010  -  Earl R. Dean Collection info
2 January 2011 - more information and change of status
--> 
            <br />
            <br /> 
            <center>
             <font face="Verdana" size="2" color="#5FA505">Urban Legends Reference Pages &copy; 1995-2014 by snopes.com. <br /> This material may not be reproduced without permission. <br /> snopes and the snopes.com logo are registered service marks of snopes.com.</font>
            </center>
            <br /> 
           </div> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <table>
            <tbody>
             <tr>
              <td><img src="/images/template/icon-sources.gif" /></td>
              <td><h2>Sources:</h2></td>
             </tr>
            </tbody>
           </table> <font size="-1"> 
            <dl> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Allen, Frederick. &nbsp; 
               <i>Secret Formula.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; New York: HarperCollins, 1994. &nbsp; ISBN 0-88730-672-1 &nbsp; (pp. 112).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Dean, Norman. &nbsp; 
               <i>The Man Behind the Bottle.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; Bloomington, IN: Xlibris, 2010. &nbsp; ISBN978-1-4500-5404-1.
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Lundy, Betty Mussell. &nbsp; &quot;The Bottle.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>American Heritage.</i> &nbsp; June-July 1986 &nbsp; (pp. 98-101).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Stattmann, Ed. &nbsp; &quot;Coke Successfully Bottled Its Image 70 Years Ago.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>United Press International.</i> &nbsp; 11 July 1985.
              </nobr>
             </dd> 
            </dl> </font> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <center>
            <center> 
             <script type="text/javascript">
<!--
google_ad_client = "pub-6608306193529351";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_type = "text";
google_ad_channel = "0054321535";
google_color_border = "000000";
//-->
</script> 
             <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            </center> 
           </center> </td> 
          <td class="adColumn"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '160x600';
    kmn_placement = 'f829c9263630394d4aa365109f5e0c0c';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <div class="siteBottom">
     &nbsp; 
   </div> 
  </div> 
  <div class="siteFooterGradient">
   &nbsp;
  </div>   
 </body>
</html>