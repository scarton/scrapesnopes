Cokelore	Cokelore	More than a century after the creation of Coca-Cola, we're still as much in love with this most famous of soft drinks as our great-grandparents were. Hold up a Coke, and you proclaim all that's best about the American way of life: Coca-Cola is first dates and shy kisses, a platoon of war-weary GIs getting letters from home, and an old, rusted sign creaking in the wind outside the hometown diner. Coca-Cola is also one of the most successful companies the world has ever known; nothing can be that big and popular, so much a part of everyday life, without having legends spring up around it. Coca-Cola's uniquely influential position in our culture has led to a special set of legends we call 'Cokelore'; a collection of Coke trivia and tall tales sure to refresh even the most informationally-parched reader.	http://www.snopes.com/cokelore/cokelore.asp	http://www.snopes.com/graphics/icons/main/coke.gif

Killer Sperm	FALSE Killer Sperm Coca-Cola is an effective contraceptive.	http://www.snopes.com/cokelore/sperm.asp	http://www.snopes.com//images/red.gif
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <title>snopes.com: Coca-Cola Spermicide</title> 
  <link href="/style.css" rel="stylesheet" type="text/css" /> 
  <meta name="robots" content="INDEX,NOFOLLOW,NOARCHIVE" /> 
  <meta name="description" content="Is Coca-Cola is an effective spermicide?" /> 
  <meta name="keywords" content="Coca-Cola, Coke, sperm, spermicide, urban legends archive, hoaxes, scams, rumors, urban myths, folklore, hoax slayer, mythbusters, misinformation, snopes" /> 
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> 
  <link rel="alternate" type="application/rss+xml" href="http://www.snopes.com/info/whatsnew.xml" title="RSS feed for snopes.com" /> 
  <link rel="image_src" href="http://www.snopes.com/logo-small.gif" /> 
  <script src="http://Q1MediaHydraPlatform.com/ads/video/unit_desktop_slider.php?eid=19503"></script>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script language="JavaScript">
if (window!=top){top.location.href=location.href;}
</script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40468225-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
 </head>     
 <body> 
  <script type="text/javascript">
<!--
var casaleD=new Date();var casaleR=(casaleD.getTime()%8673806982)+Math.random();
var casaleU=escape(window.location.href);
var casaleHost=' type="text/javascript" src="http://as.casalemedia.com/s?s=';
document.write('<scr'+'ipt'+casaleHost+'81847&amp;u=');
document.write(casaleU+'&amp;f=1&amp;id='+casaleR+'"><\/scr'+'ipt>');
//-->
</script> 
  <div class="siteWrapper"> 
   <table class="siteHeader" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="logo"><a href="http://www.snopes.com"><img src="/images/template/snopeslogo.gif" alt="snopes.com" /></a></td> 
      <td> 
       <table class="features"> 
        <tbody>
         <tr> 
          <td class="topAdvertisement"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '728x90';
    kmn_placement = 'cad674db7f73589c9a110884ce73bb72';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
         <tr> 
          <td class="searchArea"> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <!--
                    <TD VALIGN=TOP><IMG SRC="/images/search-hdr.gif" ALT="Search"></TD>
                    <TD VALIGN=TOP>

                      <FORM METHOD="GET" ACTION="http://search.atomz.com/search/"><INPUT TYPE=TEXT WIDTH=50 SIZE=60 ID="searchbox" NAME="sp-q">
                    </TD>
                    <TD>
                      <INPUT TYPE=IMAGE SRC="/images/search-btn-go.gif">
                      <INPUT TYPE=HIDDEN NAME="sp-a" VALUE="00062d45-sp00000000">
                      <INPUT TYPE=HIDDEN NAME="sp-advanced" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-p" VALUE="all">
                      <INPUT TYPE=HIDDEN NAME="sp-w-control" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-w" VALUE="alike">
                      <INPUT TYPE=HIDDEN NAME="sp-date-range" VALUE=-1>
                      <INPUT TYPE=HIDDEN NAME="sp-x" VALUE="any">
                      <INPUT TYPE=HIDDEN NAME="sp-c" VALUE=100>
                      <INPUT TYPE=HIDDEN NAME="sp-m" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-s" VALUE=0>
                      </FORM>
-->  
             </tr> 
            </tbody>
           </table> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <table class="siteMenu" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td><a href="/info/whatsnew.asp" onmouseover="window.status='What\'s New';return true" onmouseout="window.status='';return true"><img src="/images/menu-whats-new.gif" alt="What's New" /></a><a href="/info/random/random.asp"><img src="/images/menu-randomizer.gif" alt="Randomizer" /></a><a href="/info/top25uls.asp"><img src="/images/menu-hot-25.gif" alt="Hot 25" /></a><a href="/info/faq.asp"><img src="/images/menu-faq.gif" alt="FAQ" /></a><a href="/daily/"><img src="/images/menu-odd-news.gif" alt="Odd News" /></a><a href="/info/glossary.asp"><img src="/images/menu-glossary.gif" alt="Glossary" /></a><a href="/info/newsletter.asp"><img src="/images/menu-newsletter.gif" alt="Newsletter" /></a><a href="http://message.snopes.com" target="message"><img src="/images/menu-message-board.gif" alt="Message Board" /></a></td> 
     </tr> 
    </tbody>
   </table> 
   <noindex> 
    <table class="siteSubMenu" cellpadding="0" cellspacing="0"> 
     <tbody>
      <tr>
       <td> </td>
       <td width="534" class="breadcrumbs"><a href="/snopes.asp">Home</a> &gt; <a href="/cokelore/cokelore.asp#sperm">Cokelore</a> &gt; Killer Sperm</td> 
       <td width="420" class="links"><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Contact Us';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-contact-us.gif" alt="Contact Us" /></a><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Submit a Rumor';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-submit-rumor.gif" alt="Submit a Rumor" /></a><a href="http://www.snopes.com/cgi-bin/comments/photovideo.asp" onmouseover="window.status='Submit a Photo/Video';return true" onmouseout="window.status='';return true" target="photo"><img src="/images/sub-menu-submit-photo.gif" alt="Submit a Photo/Video" /></a> </td> 
      </tr> 
     </tbody>
    </table> 
   </noindex> 
   <table class="siteContent" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="contentArea"> 
       <table class="threeColumn" cellpadding="0" cellspacing="0"> 
        <tbody>
         <tr valign="TOP"> 
          <td class="navColumn"> <a href="https://twitter.com/snopes" class="twitter-follow-button" data-show-count="false">Follow @snopes</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script> <p></p> 
           <div class="navHeader">
            CATEGORIES:
           </div> 
           <ul> 
            <li><a href="/autos/autos.asp">Autos</a></li> 
            <li><a href="/business/business.asp">Business</a></li> 
            <li><a href="/cokelore/cokelore.asp">Cokelore</a></li> 
            <li><a href="/college/college.asp">College</a></li> 
            <li><a href="/computer/computer.asp">Computers</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/crime/crime.asp">Crime</a></li> 
            <li><a href="/critters/critters.asp">Critter Country</a></li> 
            <li><a href="/disney/disney.asp">Disney</a></li> 
            <li><a href="/embarrass/embarrass.asp">Embarrassments</a></li> 
            <li><a href="/photos/photos.asp">Fauxtography</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/food/food.asp">Food</a></li> 
            <li><a href="/fraud/fraud.asp">Fraud &amp; Scams</a></li> 
            <li><a href="/glurge/glurge.asp">Glurge Gallery</a></li> 
            <li><a href="/history/history.asp">History</a></li> 
            <li><a href="/holidays/holidays.asp">Holidays</a></li> 
            <li><a href="/horrors/horrors.asp">Horrors</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/humor/humor.asp">Humor</a></li> 
            <li><a href="/inboxer/inboxer.asp">Inboxer Rebellion</a></li> 
            <li><a href="/language/language.asp">Language</a></li> 
            <li><a href="/legal/legal.asp">Legal</a></li> 
            <li><a href="/lost/lost.asp">Lost Legends</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/love/love.asp">Love</a></li> 
            <li><a href="/luck/luck.asp">Luck</a></li> 
            <li><a href="/media/media.asp">Media Matters</a></li> 
            <li><a href="/medical/medical.asp">Medical</a></li> 
            <li><a href="/military/military.asp">Military</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/movies/movies.asp">Movies</a></li> 
            <li><a href="/music/music.asp">Music</a></li> 
            <li><a href="/oldwives/oldwives.asp">Old Wives' Tales</a></li> 
            <li><a href="/politics/politics.asp">Politics</a></li> 
            <li><a href="/pregnant/pregnant.asp">Pregnancy</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/quotes/quotes.asp">Quotes</a></li> 
            <li><a href="/racial/racial.asp">Racial Rumors</a></li> 
            <li><a href="/radiotv/radiotv.asp">Radio &amp; TV</a></li> 
            <li><a href="/religion/religion.asp">Religion</a></li> 
            <li><a href="/risque/risque.asp">Risqu&eacute; Business</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/science/science.asp">Science</a></li> 
            <li><a href="/rumors/rumors.asp">September 11</a></li> 
            <li><a href="/sports/sports.asp">Sports</a></li> 
            <li><a href="/travel/travel.asp">Travel</a></li> 
            <li><a href="/weddings/weddings.asp">Weddings</a></li> 
           </ul> </td> 
          <td class="contentColumn" align="LEFT"> <br /> <a href="mailto:Insert address(es) here?subject=Coca-Cola Spermicide&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/sperm.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><img src="/images/envelope.gif" alt="E-mail this page" /></a> <a href="mailto:Insert address(es) here?subject=Coca-Cola Spermicide&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/sperm.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><font size="2" color="#0000FF">E-mail this</font></a> 
           <!--
 &nbsp; &nbsp; &nbsp; &nbsp; <div class="fb-like" data-href="https://www.facebook.com/pages/snopescom/241061082705085" data-send="false" data-width="110" data-show-faces="false"></div>
--> <br /><br /> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <td valign="TOP"><img src="/images/search-hdr.gif" alt="Search" /></td> 
              <td valign="TOP"> 
               <form method="GET" action="http://search.atomz.com/search/">
                <input type="TEXT" width="50" size="60" id="searchbox" name="sp-q" /> 
               </form></td> 
              <td> <input type="IMAGE" src="/images/search-btn-go.gif" /> <input type="HIDDEN" name="sp-a" value="00062d45-sp00000000" /> <input type="HIDDEN" name="sp-advanced" value="1" /> <input type="HIDDEN" name="sp-p" value="all" /> <input type="HIDDEN" name="sp-w-control" value="1" /> <input type="HIDDEN" name="sp-w" value="alike" /> <input type="HIDDEN" name="sp-date-range" value="-1" /> <input type="HIDDEN" name="sp-x" value="any" /> <input type="HIDDEN" name="sp-c" value="100" /> <input type="HIDDEN" name="sp-m" value="1" /> <input type="HIDDEN" name="sp-s" value="0" />  </td> 
             </tr> 
            </tbody>
           </table> <br /><br /> 
           <div class="utilityLinks"> 
            <!-- code goes here --> 
            <div class="pw-widget pw-counter-vertical"> 
             <a class="pw-button-googleplus pw-look-native"></a> 
             <a class="pw-button-tumblr pw-look-native"></a> 
             <a class="pw-button-pinterest pw-look-native"></a> 
             <a class="pw-button-reddit pw-look-native"></a> 
             <a class="pw-button-email pw-look-native"></a> 
             <a class="pw-button-facebook pw-look-native"></a> 
             <a class="pw-button-twitter pw-look-native"></a> 
            </div> 
            <script src="http://i.po.st/static/v3/post-widget.js#publisherKey=snopescom1152" type="text/javascript"></script> 
            <!-- code goes here --> 
           </div> <br /> <h1>Killer Sperm</h1> <style type="text/css">
<!--
 A:link, A:visited, A:active { text-decoration: underline; }
 A:link {color: #0000FF;}
 A:visited {color: #5fa505;}
 A:hover {color: #FF0000;} 
 .article_text {text-align: justify;} 
-->
</style> 
           <div class="article_text"> 
            <font color="#5FA505"><b>Claim:</b></font> &nbsp; Coca-Cola is an effective contraceptive. 
            <br />
            <br /> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <noindex>
             <table>
              <tbody>
               <tr>
                <td valign="CENTER"><img src="/images/red.gif" /></td>
                <td valign="TOP"><font size="5" color="#37628D"><b>FALSE</b></font></td>
               </tr>
              </tbody>
             </table> 
            </noindex> 
            <center>
             <img src="/images/content-divider.gif" />
            </center> 
            <br /> 
            <font color="#5FA505"><b>Origins:</b></font> &nbsp;Somehow I doubt this is what 
            <nobr>
             Coca-Cola
            </nobr> meant by 'the pause that refreshes.' 
            <br />
            <br /> Here's a short newspaper summary from 1985 about the first study undertaken to verify claims about Coke's contraceptive properties: 
            <br /> 
            <font face="Verdana" size="2"> 
             <div style="text-align: justify; margin-top: 20px; margin-bottom: 20px; margin-left: 40px; margin-right: 40px">
               'OLD COKE' BETTER THAN 'NEW COKE?' 
              <br />
              <br /> Common commodities such as honey and sodium bicarbonate, acidic fruit juices and oils have been used through history as spermicides. Three Harvard researchers note that 
              <nobr>
               Coca-Cola
              </nobr> is said to be favored for this purpose in some developing countries and was touted in American 
              <img src="graphics/sperm.gif" align="LEFT" hspace="16" vspace="16" alt="Sperm" width="293" height="150" border="0" /> folklore as a contraceptive aid in years gone by. No documentation of the soft drink's spermicidal capabilities was found, so 
              <nobr>
               Dr. Sharee
              </nobr> Umpierre and two colleagues decided to test Coke in some of its various formulations in their lab. They found that Diet Coke was a most effective spermicide and the original formula Coke was also quite effective, five times more so than the reformulated &quot;new&quot; Coke. &quot;Although not recommended for postcoital contraception, partly because sperm can be found in the oviducts within minutes after intercourse, 
              <nobr>
               Coca-Cola
              </nobr> products do appear to have a spermicidal effect,&quot; the researchers said in a letter to the 
              <i>New England Journal of Medicine</i>. &quot;Furthermore, our data indicate that at least in the area of spermicidal effect, 'Classic' Coke is it.&quot; 
              <br />
              <br /> [T]he researchers said they found marked differences in the ability of four different 
              <nobr>
               Coca-Cola
              </nobr> formulations to act as a spermicide. At the same time, they warned against the use of soft drinks of any kind as douches after intercourse to prevent pregnancy. While there are differences among soft drinks, all fail as effective contraceptives, the researchers noted. 
              <br />
              <br /> To test the sperm-killing abilities of various 
              <nobr>
               Coca-Cola
              </nobr> products, the three researchers prepared test tubes containing small samples of carefully preserved sperm and poured in small amounts of Diet Coke, New Coke, caffeine-free New Coke and Classic 
              <nobr>
               Coke &#x2014; carefully
              </nobr> repeating the test three times for each soda. 
              <br />
              <br /> All of them killed some sperm, but New Coke turned out to be least effective, with Diet Coke having the most pronounced effect overall and Classic Coke recording a five times greater sperm-killing rate than its upstart rival. &quot;
              <nobr>
               Coca-Cola
              </nobr> products do appear to have a spermicidal effect,&quot; the study deadpanned. &quot;Furthermore, our data indicate that, at least in the area of spermicidal effect, 'Classic Coke' is it.&quot; 
              <br />
              <br /> 
              <nobr>
               Coca-Cola
              </nobr> saw little humor in the Harvard project. A spokesman said the company hadn't seen the new report, but &quot;our position is we do not promote any of our products for any medical use.&quot;
              <font size="-1"><sup>1</sup></font> 
             </div> </font> However, other researchers were unable to verify these results in later experiments. Subsequent trials performed by medical researchers in Taiwan (using several varities of both 
            <nobr>
             Coca-Cola
            </nobr> and 
            <nobr>
             Pepsi-Cola
            </nobr>) led them to the opposite conclusion, that &quot;cola has little if any spermicidal effect&quot;: 
            <br /> 
            <font face="Verdana" size="2"> 
             <div style="text-align: justify; margin-top: 20px; margin-bottom: 20px; margin-left: 40px; margin-right: 40px">
               The inhibitory effect of Old Coke, caffeine-free New Coke, New Coke, Diet Coke and Pepsi-Cola on human sperm motility was studied with a trans-membrane migration method. None of them could decrease sperm motility to less than 70% of control within one hour. A previous study which claimed a marked variation of spermicidal potencies among different formulations of Coca-Cola could not be confirmed. Even if cola has a spermicidal effect, its potency is relatively weak as compared with other well-known spermicidal agents. 
              <br />
              <br /> Testing of various cola formulas on sperm motility using a trans-membrane procedure did not decrease motility to less than 70% control in a 
              <nobr>
               1-hour
              </nobr> period. Diet Coca-Cola had the strongest spermicidal effect followed by Classic Coca-Cola, Caffeine-free Coca-Cola and New Coca-Cola. Since there are no known substances in cola that effect cellular membranes, the results of these tests were not unusual. Other tests have been done using higher dilution of cola which could effect sperm motility and give different results for spermicidal potencies. The results show that cola has little if any spermicidal effect. Its use in postcoital douching is ineffective and could cause complications such as infection.
              <font size="-1"><sup>2</sup></font> 
             </div> </font> Whichever set of results one wants to believe, those tempted to grab a six-pack and head down to the beach should keep in mind that spermicides 
            <i>themselves</i> aren't all that effective at preventing pregnancy. Saying Diet Coke kills sperm is like saying a rubber glove makes a decent 
            <nobr>
             balloon &#x2014; you
            </nobr> shouldn't interpret either as meaning that using them creatively will keep you safe from parenthesis. (That's when you wind up pregnant and end up writing a book about it.) 
            <br />
            <br /> Coca-Cola may not be promoting its product as a spermicidal douche, but the idea ain't new. Coke (and 
            <nobr>
             Dr Pepper
            </nobr> in the southern States) douches have been part of contraceptive lore at least since the 1950s, with the common belief being that the carbonic acid in Coke killed the sperm and the sugar 
            <table align="RIGHT" cellpadding="12" border="0">
             <tbody>
              <tr>
               <td> </td>
              </tr>
             </tbody>
            </table> &quot;exploded&quot; the sperm cells, while the carbonation of the drink forced the jet of liquid into the vagina. Back in the 1950s and 1960s, this method of parenthood prevention proved somewhat popular because not only was it cheap and universally available at a time when reliable birth control methods were hard to come by, but it also came in its own handy &quot;shake and shoot&quot; disposable applicator. After intercourse, the girl would uncap a warm Coke, put her thumb over the mouth of the bottle, shake up the beverage, then insert the neck of the bottle in her vagina and move her thumb out of the way. The warm well-shaken Coke became an effervescent spermicidal douche, with the traditional (at that time) six-ounce bottle providing what was deemed to be just the right amount for one application. 
            <br />
            <br /> As stated above, this method wasn't all that effective at preventing pregnancy. Though Coca-Cola might have been a (slight) step up from plain water, douching with any liquid is far too often a case of attempting to close the barn door after the horse has got loose. By the time the douche is fired off, 100,000 or thereabouts sperm are swimming around in the uterus, already out of reach of any douche, even a fizzy one. Women should be reluctant to turn to soda pop douches for another reason besides their ineffectiveness at preventing 
            <nobr>
             Momhood &#x2014; the
            </nobr> sugars in them being let loose in that part of a woman's anatomy can lead to yeast infections, an annoying, difficult-to-cure condition. 
            <br />
            <br /> Oddly, a 1992 study conducted by Nigerian researchers found that, although 
            <nobr>
             Coca-Cola
            </nobr> and 
            <nobr>
             Pepsi-Cola
            </nobr> were ineffective as spermicides, Krest bitter lemon drink fared quite well in this regard, and they recommended additional study to determine whether it might have &quot;great potential as such a contraceptive&quot;: 
            <br /> 
            <font face="Verdana" size="2"> 
             <div style="text-align: justify; margin-top: 20px; margin-bottom: 20px; margin-left: 40px; margin-right: 40px">
               This study investigated the effects of increase in temperature and in pH of Coca-Cola, Afri-Cola, Pepsi-Cola and Krest bitter lemon drinks (&quot;soft drinks&quot;) produced in Nigeria on the in vitro motility of spermatozoa. Of the drinks, Krest bitter lemon (unadjusted) immobilized all spermatozoa within 
              <nobr>
               1 minute
              </nobr> of addition. Conclusion: i) alkalinity decreases the spermicidal action of all drinks except Coca-Cola, and ii) Krest bitter lemon may achieve very high efficacy if used as post-coital douche, especially in the impoverished, densely populated Third World. 
              <br />
              <br /> Other than Krest bitter lemon, the significant decreases in sperm motility were not enough to prevent pregnancy. These findings indicated that researchers should test Krest bitter lemon for effectiveness as a postcoital contraceptive. If indeed it proves effective, it has great potential as such a contraceptive among the poor in the densely population developed countries since it is readily available and inexpensive.
              <font size="-1"><sup>3</sup></font> 
             </div> </font> The bottom line? The best contraceptive use for a Coke is to use it wash down your daily birth control pills. 
            <br />
            <br /> Barbara &quot;cherry cola&quot; Mikkelson 
            <br />
            <br /> 
            <font color="#5FA505"><b>Last updated:</b></font> &nbsp; 19 May 2011 
            <!--
17 April 2000 - original
16 March 2007 - reformatted
--> 
            <br />
            <br /> 
            <center>
             <font face="Verdana" size="2" color="#5FA505">Urban Legends Reference Pages &copy; 1995-2014 by snopes.com. <br /> This material may not be reproduced without permission. <br /> snopes and the snopes.com logo are registered service marks of snopes.com.</font>
            </center>
            <br /> 
           </div> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <table>
            <tbody>
             <tr>
              <td><img src="/images/template/icon-sources.gif" /></td>
              <td><h2>Sources:</h2></td>
             </tr>
            </tbody>
           </table> <font size="-1"> 
            <dl> 
             <dt>
              <nobr>
                &nbsp; &nbsp; 2. &nbsp; Hong, C.Y., et al. &nbsp; &quot;The Spermicidal Potency of Coca-Cola and Pepsi-Cola.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; 
               <i>Human Toxicology.</i> &nbsp; September 1987 &nbsp; (pp. 395-396).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
               &nbsp; &nbsp; 3. &nbsp; Nwoha, P.U. &nbsp; &quot;The Immobilization of All Spermatozoa in Vitro by Bitter Lemon Drink and the Effect of Alkaline pH.&quot;
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; 
               <i>Contraception.</i> &nbsp; December 1992 &nbsp; (pp. 537-542).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Oveson, Lars. &nbsp; &quot;Coke Is Not It.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; 
               <i>European Journal of Cancer Prevention.</i> &nbsp; June 2001 &nbsp; (pp. 297-298).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; 1. &nbsp; Parachini, Allan. &nbsp; &quot;Use of Coke to Kill Sperm Prompts Tests.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; 
               <i>Los Angeles Times.</i> &nbsp; 21 November 1985 &nbsp; (p. E1).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Reuben, David. &nbsp; 
               <font face="Arial"><i>Everything You Always Wanted to Know About Sex.</i></font>
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; New York: David McKay Company, 1970 &nbsp; (p. 233).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Umpierre, S.A., et al. &nbsp; &quot;Effect of 'Coke' on Sperm Motility.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; 
               <i>New England Journal of Medicine.</i> &nbsp; November 1985 &nbsp; (p. 1351).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
                &nbsp; &nbsp; Van, John. &nbsp; &quot;New Cement Found to Fix Teeth, Bones.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
                &nbsp; &nbsp; 
               <i>Chicago Tribune.</i> &nbsp; 8 December 1985 &nbsp; (Tomorrow; p. 7).
              </nobr>
             </dd> 
            </dl> </font> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <center>
            <center> 
             <script type="text/javascript">
<!--
google_ad_client = "pub-6608306193529351";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_type = "text";
google_ad_channel = "0054321535";
google_color_border = "000000";
//-->
</script> 
             <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            </center> 
           </center> </td> 
          <td class="adColumn"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '160x600';
    kmn_placement = 'f829c9263630394d4aa365109f5e0c0c';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <div class="siteBottom">
     &nbsp; 
   </div> 
  </div> 
  <div class="siteFooterGradient">
   &nbsp;
  </div>   
 </body>
</html>