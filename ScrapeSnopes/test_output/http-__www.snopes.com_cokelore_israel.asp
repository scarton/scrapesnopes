Cokelore	Cokelore	More than a century after the creation of Coca-Cola, we're still as much in love with this most famous of soft drinks as our great-grandparents were. Hold up a Coke, and you proclaim all that's best about the American way of life: Coca-Cola is first dates and shy kisses, a platoon of war-weary GIs getting letters from home, and an old, rusted sign creaking in the wind outside the hometown diner. Coca-Cola is also one of the most successful companies the world has ever known; nothing can be that big and popular, so much a part of everyday life, without having legends spring up around it. Coca-Cola's uniquely influential position in our culture has led to a special set of legends we call 'Cokelore'; a collection of Coke trivia and tall tales sure to refresh even the most informationally-parched reader.	http://www.snopes.com/cokelore/cokelore.asp	http://www.snopes.com/graphics/icons/main/coke.gif

Red, White, and Jew	TRUE Red, White, and Jew Coca-Cola was once considered anti-Semitic for refusing to do business in Israel.	http://www.snopes.com/cokelore/israel.asp	http://www.snopes.com//images/green.gif
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head> 
  <title>snopes.com: Coca-Cola and Israel</title> 
  <link href="/style.css" rel="stylesheet" type="text/css" /> 
  <meta name="robots" content="INDEX,NOFOLLOW,NOARCHIVE" /> 
  <meta name="description" content="Did Coca-Cola once refuse to do business with Israel?" /> 
  <meta name="keywords" content="Coca-Cola, Coke, Pepsi, anti-Semitic, arab, Israel, bottling plant, boycott, b'nai b'rith, jew, jewish, Michael Jackson, urban legends archive, hoaxes, scams, rumors, urban myths, folklore, hoax slayer, mythbusters, misinformation, snopes" /> 
  <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" /> 
  <link rel="alternate" type="application/rss+xml" href="http://www.snopes.com/info/whatsnew.xml" title="RSS feed for snopes.com" /> 
  <link rel="image_src" href="http://www.snopes.com/logo-small.gif" /> 
  <script src="http://Q1MediaHydraPlatform.com/ads/video/unit_desktop_slider.php?eid=19503"></script>
  <script type="text/javascript" src="http://apis.google.com/js/plusone.js"></script>
  <script language="JavaScript">
if (window!=top){top.location.href=location.href;}
</script>
  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-40468225-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
 </head>     
 <body> 
  <script type="text/javascript">
<!--
var casaleD=new Date();var casaleR=(casaleD.getTime()%8673806982)+Math.random();
var casaleU=escape(window.location.href);
var casaleHost=' type="text/javascript" src="http://as.casalemedia.com/s?s=';
document.write('<scr'+'ipt'+casaleHost+'81847&amp;u=');
document.write(casaleU+'&amp;f=1&amp;id='+casaleR+'"><\/scr'+'ipt>');
//-->
</script> 
  <div class="siteWrapper"> 
   <table class="siteHeader" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="logo"><a href="http://www.snopes.com"><img src="/images/template/snopeslogo.gif" alt="snopes.com" /></a></td> 
      <td> 
       <table class="features"> 
        <tbody>
         <tr> 
          <td class="topAdvertisement"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '728x90';
    kmn_placement = 'cad674db7f73589c9a110884ce73bb72';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
         <tr> 
          <td class="searchArea"> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <!--
                    <TD VALIGN=TOP><IMG SRC="/images/search-hdr.gif" ALT="Search"></TD>
                    <TD VALIGN=TOP>

                      <FORM METHOD="GET" ACTION="http://search.atomz.com/search/"><INPUT TYPE=TEXT WIDTH=50 SIZE=60 ID="searchbox" NAME="sp-q">
                    </TD>
                    <TD>
                      <INPUT TYPE=IMAGE SRC="/images/search-btn-go.gif">
                      <INPUT TYPE=HIDDEN NAME="sp-a" VALUE="00062d45-sp00000000">
                      <INPUT TYPE=HIDDEN NAME="sp-advanced" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-p" VALUE="all">
                      <INPUT TYPE=HIDDEN NAME="sp-w-control" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-w" VALUE="alike">
                      <INPUT TYPE=HIDDEN NAME="sp-date-range" VALUE=-1>
                      <INPUT TYPE=HIDDEN NAME="sp-x" VALUE="any">
                      <INPUT TYPE=HIDDEN NAME="sp-c" VALUE=100>
                      <INPUT TYPE=HIDDEN NAME="sp-m" VALUE=1>
                      <INPUT TYPE=HIDDEN NAME="sp-s" VALUE=0>
                      </FORM>
-->  
             </tr> 
            </tbody>
           </table> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <table class="siteMenu" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td><a href="/info/whatsnew.asp" onmouseover="window.status='What\'s New';return true" onmouseout="window.status='';return true"><img src="/images/menu-whats-new.gif" alt="What's New" /></a><a href="/info/random/random.asp"><img src="/images/menu-randomizer.gif" alt="Randomizer" /></a><a href="/info/top25uls.asp"><img src="/images/menu-hot-25.gif" alt="Hot 25" /></a><a href="/info/faq.asp"><img src="/images/menu-faq.gif" alt="FAQ" /></a><a href="/daily/"><img src="/images/menu-odd-news.gif" alt="Odd News" /></a><a href="/info/glossary.asp"><img src="/images/menu-glossary.gif" alt="Glossary" /></a><a href="/info/newsletter.asp"><img src="/images/menu-newsletter.gif" alt="Newsletter" /></a><a href="http://message.snopes.com" target="message"><img src="/images/menu-message-board.gif" alt="Message Board" /></a></td> 
     </tr> 
    </tbody>
   </table> 
   <noindex> 
    <table class="siteSubMenu" cellpadding="0" cellspacing="0"> 
     <tbody>
      <tr>
       <td> </td>
       <td width="534" class="breadcrumbs"><a href="/snopes.asp">Home</a> &gt; <a href="/cokelore/cokelore.asp#israel">Cokelore</a> &gt; Red, White, and Jew</td> 
       <td width="420" class="links"><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Contact Us';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-contact-us.gif" alt="Contact Us" /></a><a href="http://www.snopes.com/cgi-bin/comments/webmail.asp" onmouseover="window.status='Submit a Rumor';return true" onmouseout="window.status='';return true" target="mail"><img src="/images/sub-menu-submit-rumor.gif" alt="Submit a Rumor" /></a><a href="http://www.snopes.com/cgi-bin/comments/photovideo.asp" onmouseover="window.status='Submit a Photo/Video';return true" onmouseout="window.status='';return true" target="photo"><img src="/images/sub-menu-submit-photo.gif" alt="Submit a Photo/Video" /></a> </td> 
      </tr> 
     </tbody>
    </table> 
   </noindex> 
   <table class="siteContent" cellpadding="0" cellspacing="0"> 
    <tbody>
     <tr> 
      <td class="contentArea"> 
       <table class="threeColumn" cellpadding="0" cellspacing="0"> 
        <tbody>
         <tr valign="TOP"> 
          <td class="navColumn"> <a href="https://twitter.com/snopes" class="twitter-follow-button" data-show-count="false">Follow @snopes</a> <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script> <p></p> 
           <div class="navHeader">
            CATEGORIES:
           </div> 
           <ul> 
            <li><a href="/autos/autos.asp">Autos</a></li> 
            <li><a href="/business/business.asp">Business</a></li> 
            <li><a href="/cokelore/cokelore.asp">Cokelore</a></li> 
            <li><a href="/college/college.asp">College</a></li> 
            <li><a href="/computer/computer.asp">Computers</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/crime/crime.asp">Crime</a></li> 
            <li><a href="/critters/critters.asp">Critter Country</a></li> 
            <li><a href="/disney/disney.asp">Disney</a></li> 
            <li><a href="/embarrass/embarrass.asp">Embarrassments</a></li> 
            <li><a href="/photos/photos.asp">Fauxtography</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/food/food.asp">Food</a></li> 
            <li><a href="/fraud/fraud.asp">Fraud &amp; Scams</a></li> 
            <li><a href="/glurge/glurge.asp">Glurge Gallery</a></li> 
            <li><a href="/history/history.asp">History</a></li> 
            <li><a href="/holidays/holidays.asp">Holidays</a></li> 
            <li><a href="/horrors/horrors.asp">Horrors</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/humor/humor.asp">Humor</a></li> 
            <li><a href="/inboxer/inboxer.asp">Inboxer Rebellion</a></li> 
            <li><a href="/language/language.asp">Language</a></li> 
            <li><a href="/legal/legal.asp">Legal</a></li> 
            <li><a href="/lost/lost.asp">Lost Legends</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/love/love.asp">Love</a></li> 
            <li><a href="/luck/luck.asp">Luck</a></li> 
            <li><a href="/media/media.asp">Media Matters</a></li> 
            <li><a href="/medical/medical.asp">Medical</a></li> 
            <li><a href="/military/military.asp">Military</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/movies/movies.asp">Movies</a></li> 
            <li><a href="/music/music.asp">Music</a></li> 
            <li><a href="/oldwives/oldwives.asp">Old Wives' Tales</a></li> 
            <li><a href="/politics/politics.asp">Politics</a></li> 
            <li><a href="/pregnant/pregnant.asp">Pregnancy</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/quotes/quotes.asp">Quotes</a></li> 
            <li><a href="/racial/racial.asp">Racial Rumors</a></li> 
            <li><a href="/radiotv/radiotv.asp">Radio &amp; TV</a></li> 
            <li><a href="/religion/religion.asp">Religion</a></li> 
            <li><a href="/risque/risque.asp">Risqu&eacute; Business</a></li> 
           </ul> 
           <div class="navSpacer">
             &nbsp; 
           </div> 
           <ul> 
            <li><a href="/science/science.asp">Science</a></li> 
            <li><a href="/rumors/rumors.asp">September 11</a></li> 
            <li><a href="/sports/sports.asp">Sports</a></li> 
            <li><a href="/travel/travel.asp">Travel</a></li> 
            <li><a href="/weddings/weddings.asp">Weddings</a></li> 
           </ul> </td> 
          <td class="contentColumn" align="LEFT"> <br /> <a href="mailto:Insert address(es) here?subject=Coca-Cola and Israel&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/israel.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><img src="/images/envelope.gif" alt="E-mail this page" /></a> <a href="mailto:Insert address(es) here?subject=Coca-Cola and Israel&amp;body=I thought you might find the following article from snopes.com interesting: http://www.snopes.com/cokelore/israel.asp" onmouseover="window.status='E-mail this page';return true" onmouseout="window.status='';return true"><font size="2" color="#0000FF">E-mail this</font></a> 
           <!--
 &nbsp; &nbsp; &nbsp; &nbsp; <div class="fb-like" data-href="https://www.facebook.com/pages/snopescom/241061082705085" data-send="false" data-width="110" data-show-faces="false"></div>
--> <br /><br /> 
           <table class="searchBox" align="RIGHT"> 
            <tbody>
             <tr> 
              <td valign="TOP"><img src="/images/search-hdr.gif" alt="Search" /></td> 
              <td valign="TOP"> 
               <form method="GET" action="http://search.atomz.com/search/">
                <input type="TEXT" width="50" size="60" id="searchbox" name="sp-q" /> 
               </form></td> 
              <td> <input type="IMAGE" src="/images/search-btn-go.gif" /> <input type="HIDDEN" name="sp-a" value="00062d45-sp00000000" /> <input type="HIDDEN" name="sp-advanced" value="1" /> <input type="HIDDEN" name="sp-p" value="all" /> <input type="HIDDEN" name="sp-w-control" value="1" /> <input type="HIDDEN" name="sp-w" value="alike" /> <input type="HIDDEN" name="sp-date-range" value="-1" /> <input type="HIDDEN" name="sp-x" value="any" /> <input type="HIDDEN" name="sp-c" value="100" /> <input type="HIDDEN" name="sp-m" value="1" /> <input type="HIDDEN" name="sp-s" value="0" />  </td> 
             </tr> 
            </tbody>
           </table> <br /><br /> 
           <div class="utilityLinks"> 
            <!-- code goes here --> 
            <div class="pw-widget pw-counter-vertical"> 
             <a class="pw-button-googleplus pw-look-native"></a> 
             <a class="pw-button-tumblr pw-look-native"></a> 
             <a class="pw-button-pinterest pw-look-native"></a> 
             <a class="pw-button-reddit pw-look-native"></a> 
             <a class="pw-button-email pw-look-native"></a> 
             <a class="pw-button-facebook pw-look-native"></a> 
             <a class="pw-button-twitter pw-look-native"></a> 
            </div> 
            <script src="http://i.po.st/static/v3/post-widget.js#publisherKey=snopescom1152" type="text/javascript"></script> 
            <!-- code goes here --> 
           </div> <br /> <h1>Red, White, and Jew</h1> <style type="text/css">
<!--
 A:link, A:visited, A:active { text-decoration: underline; }
 A:link {color: #0000FF;}
 A:visited {color: #5fa505;}
 A:hover {color: #FF0000;} 
 .article_text {text-align: justify;} 
-->
</style> 
           <div class="article_text"> 
            <font color="#5FA505"><b>Claim:</b></font> &nbsp; Coca-Cola was once considered anti-Semitic for refusing to do business in Israel. 
            <br />
            <br /> 
            <font color="#5FA505"><b>Origins:</b></font> &nbsp; The last forty-odd years have seen allegations of anti-Semitism hurled at both 
            <nobr>
             Coca-Cola
            </nobr> and Pepsi, and for both companies the charges stemmed from their one-time reluctance to do business with Israel. 
            <br />
            <br /> Successfully 
            <img src="./graphics/israel.jpg" align="LEFT" hspace="16" vspace="16" alt="Israel flag" width="140" height="211" border="0" /> doing business in the Middle East often depended upon not doing business in Israel. The Arab League was quick to boycott, and multinational concerns were forced to choose between the smaller market of Israel and the much larger market of the combined Arab states. For firms caught in the middle, it was a &quot;no win&quot; situation. 
            <br />
            <br /> Coca-Cola's turn in the harsh spotlight of public opinion came in 1966. 
            <br />
            <br /> April 1 1966: At a press conference in 
            <nobr>
             Tel Aviv,
            </nobr> businessman Moshe Bornstein accused 
            <nobr>
             Coca-Cola
            </nobr> of refusing to do business in Israel out of fear of reprisals and loss of profits in the Arab soft drink market. A week later in 
            <nobr>
             New York,
            </nobr> the Anti-Defamation League of the B'nai B'rith released a statement backing up the charges, triggering headlines across the U.S.A. 
            <nobr>
             Coca-Cola
            </nobr> was in hot water, and the American public was demanding answers. It was also rejecting the answers it was getting. 
            <br />
            <br /> In 1949 
            <nobr>
             Coca-Cola
            </nobr> had attempted to open a bottling plant in Israel, but its efforts had been blocked by the Israeli government. As long as no one questioned the company too closely, the failure of this one stab at the Israeli market appeared to provide a satisfactory answer for 
            <nobr>
             Coca-Cola's
            </nobr> conspicuous absence from the Israeli market. In the meanwhile, 
            <nobr>
             Coca-Cola
            </nobr> was content to continue quietly serving the much larger Arab market, a market it was likely to lose if it began operating in Israel. 
            <br />
            <br /> In 1961 an incident in Cairo involving civil servant Mohammad Abu Shadi momentarily shattered the quiet. Shadi had come into possession of a 
            <nobr>
             Coca-Cola
            </nobr> bottle manufactured in Ethiopia, mistaken the Amharic lettering on its label for Hebrew, and publicly accused 
            <nobr>
             Coca-Cola
            </nobr> of doing business with Israel. 
            <br />
            <br /> The manager of 
            <nobr>
             Coca-Cola
            </nobr>'s Egyptian bottling operations wasted no time (and little thought) in assuring the press that 
            <nobr>
             Coca-Cola
            </nobr> would never allow the Israelis a franchise. With their hands forced by their bottler's impolitic statement, company officials quickly invented the explanation that Israel was too small to support a franchise and gave their reasons for staying away as purely economic, not political. For the time being, this seemed to keep a lid on the brewing 
            <table align="RIGHT" cellpadding="12" border="0">
             <tbody>
              <tr>
               <td> 
                <center> 
                 <script type="text/javascript"><!--
    kmn_size = '300x250';
    kmn_placement = '1c8834441f20e22a0aef009c095a6cab';
//--></script> 
                 <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
                </center> </td>
              </tr>
             </tbody>
            </table> storm. 
            <br />
            <br /> It wasn't until 1966 that people began to wonder openly why it was that nearby Cyprus had no difficulty supporting its 
            <nobr>
             Coca-Cola
            </nobr> franchise despite their having only one-tenth the population of Israel. The comfortable aura of quiet was shattered by Bornstein's charges and the subsequent uproar they raised in the U.S.A. 
            <br />
            <br /> When these issues came to light in 1966, they proved highly embarrassing to 
            <nobr>
             Coca-Cola.
            </nobr> The administrators of Mount Sinai Hospital in Manhattan announced they would stop serving Coke, and the owners of Nathan's Famous Hot Dog Emporium on Coney Island followed suit. Faced with the prospect of a Jewish boycott in America, the company attempted to right the tipped canoe by announcing it would open a bottling plant in 
            <nobr>
             Tel Aviv.
            </nobr> (Such is the price of business: Israel with the fury of America behind it became a much more attractive market than it ever had been all on its own.) The Arab League struck back by placing 
            <nobr>
             Coca-Cola
            </nobr> on its boycott list. The boycott began in August 1968 and lasted until May 1991 (or until 1979 in Egypt, where they made their own rules). 
            <br />
            <br /> Pepsi's entry into Israel in 1992 did not go smoothly: the evolution theme of its &quot;Choice of a New Generation&quot; ad campaign (in which man was portrayed as evolving from a monkey into a Pepsi drinker) angered the strictly observant haredi community. Though Pepsi pulled the campaign from Israel, it found itself in more hot water over a 1993 Michael Jackson tour. Jackson's unthinking flashbulb-popping arrival on a Sabbath was viewed by many observant Jews as a desecration. For a time Pepsi lost its kashrut (kosher) certificate because it was deemed to be promoting a culture that would corrupt the nation's youth through rock music concerts and advertisements featuring scantily-clad women. 
            <br />
            <br /> Prior to 1992, Pepsi had backed the other horse, choosing to service the lucrative 
            <nobr>
             Coke-less
            </nobr> Arab markets in the boycott days. For its decision to stay out of Israel (and thus itself avoid being placed on the Arab League's blacklist), Pepsi faced continued criticism in the United States. In certain circles it was considered politically incorrect to be seen drinking Pepsi. 
            <br />
            <br /> The Anti-Defamation League of the B'nai B'rith investigated claims that Pepsi was participating in the boycott of Israel. U.S. law prohibited American companies from taking part in this boycott, but the law was vague, and outright violations were hard to pin down. Nothing ever came of the investigations, and Pepsi was never placed on the American government's list of violators. 
            <br />
            <br /> Pepsi always denied it was the fear of losing their Arab markets that kept them out of Israel. Like 
            <nobr>
             Coca-Cola
            </nobr> in 1961, Pepsi fell back upon the claim of Israel's being too small to support a franchise. At least this time the excuse was a bit more believable; 
            <nobr>
             Coca-Cola's
            </nobr> already holding down the lion's share of the Israeli soft drink market gave this claim a bit more plausibility. Even so, Pepsi was doing business in many other small markets and much more often than not competing head-to-head against 
            <nobr>
             Coca-Cola.
            </nobr> If these conditions were keeping them out of Israel, then why weren't they equally keeping them out of these other markets? 
            <br />
            <br /> Many people in the United States believed Pepsi was going along with the boycott, whether it was proveable in the eyes of U.S. law or not. Those lucrative Arab markets did not come without a price, and Pepsi paid it in loss of goodwill in the U.S. A significant number of American cola drinkers grew up suspecting Pepsi of being anti-Israel and refrained from buying their product. By contrast, 
            <nobr>
             Coca-Cola
            </nobr> appeared heroic. 
            <br />
            <br /> This appearance failed to take into account 
            <nobr>
             Coca-Cola's
            </nobr> fast stepping to shake off similar charges in the 1960s. Pepsi's mud-spattered skirts were but 
            <nobr>
             Coca-Cola's
            </nobr> 
            <nobr>
             hand-me-downs &#x2014; same
            </nobr> skirt, just a bit older. 
            <br />
            <br /> Today you can get either Coke or Pepsi in anywhere in the Middle East, and the days of the boycott have faded into memory. Even so, there are still those who observe the stricture of &quot;Coke is for Jews; Pepsi is for Arabs.&quot; Old wounds are not necessarily healed wounds. 
            <br />
            <br /> Barbara &quot;Pepsi challenge&quot; Mikkelson 
            <br />
            <br /> 
            <font color="#5FA505"><b>Last updated:</b></font> &nbsp; 15 May 2011 
            <!--
2 May 1999 - original
13 March 2007 - reformatted
15 May 2011: reformatted
--> 
            <br />
            <br /> 
            <center>
             <font face="Verdana" size="2" color="#5FA505">Urban Legends Reference Pages &copy; 1995-2014 by snopes.com. <br /> This material may not be reproduced without permission. <br /> snopes and the snopes.com logo are registered service marks of snopes.com.</font>
            </center>
            <br /> 
           </div> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <table>
            <tbody>
             <tr>
              <td><img src="/images/template/icon-sources.gif" /></td>
              <td><h2>Sources:</h2></td>
             </tr>
            </tbody>
           </table> <font size="-1"> 
            <dl> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Allen, Frederick. &nbsp; 
               <i>Secret Formula.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; New York: HarperCollins, 1994. &nbsp; ISBN 0-88730-672-1 &nbsp; (pp. 339-341).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Beeston, Richard. &nbsp; &quot;Jackson Enrages Orthodox Jews.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>The [London] Times.</i> &nbsp; 20 September 1993.
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Bulloch, John. &nbsp; &quot;The Coca-Cola War.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>The [London] Independent.</i> &nbsp; 9 September 1990 &nbsp; (p. 28).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Cody, Edward. &nbsp; &quot;Egypt Gets Coca-Cola; Arab Blacklist Ignored.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>The Washington Post.</i> &nbsp; 17 July 1979 &nbsp; (p. A7).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Hellman, Ziv. &nbsp; &quot;Getting in Tempo with Pepsi-Cola.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>The Jerusalem Post.</i> &nbsp; 5 July 1991.
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Holmes, Charles W. &nbsp; &quot;Pop Go the Cola Wars in Strife-Torn Region.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>The Houston Chronicle.</i> &nbsp; 25 July 1993 &nbsp; (p. 1).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Hundley, Tom. &nbsp; &quot;Israel Braces for New Conflict: The Soda War.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>Chicago Tribune.</i> &nbsp; 19 May 1992 &nbsp; (p. 1).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Lippman, Thomas W. &nbsp; &quot;Thirsty Egyptians Rush to Buy 7-Up.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>The Washington Post.</i> &nbsp; 5 September 1978 &nbsp; (p. A10).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; McGlynn, Brian. &nbsp; &quot;The Strategy That Refreshes?&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>Forbes.</i> &nbsp; 27 November 1978 &nbsp; (p. 81).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Meyer, Eugene L. &nbsp; &quot;Deli-Bakery Is a Roll Model.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>The Washington Post.</i> &nbsp; 25 September 1987 &nbsp; (p. E1).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Parks, Michael. &nbsp; &quot;Pepsi Is No Longer 'Right One' in Israel.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>Los Angeles Times.</i> &nbsp; 4 June 1993 &nbsp; (p. 1).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Pendergrast, Mark. &nbsp; 
               <i>For God, Country, and Coca-Cola.</i>
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; New York: Charles Scribner's Sons, 1993. &nbsp; ISBN 0-684-19347-7.
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Sommer, Allison Kaplan. &nbsp; &quot;Achieving Success with the 'Real Thing.'&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>The Jerusalem Post.</i> &nbsp; 5 July 1993.
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Sommer, Allison Kaplan. &nbsp; &quot;Pepsi-Cola's Anti-Israel Boycott Comes Back to Haunt Them&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>The Jerusalem Post.</i> &nbsp; 12 August 1994.
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; Zadka, Saul. &nbsp; &quot;Coca-Cola Comes off Arab Blacklist.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 
               <i>The [London] Independent.</i> &nbsp; 15 September 1991 &nbsp; (p. 8).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; 
               <i>Agence France Presse.</i> &nbsp; &quot;Pepsi-Cola Faces Kosher War.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 15 May 1992.
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; 
               <i>Business Week.</i> &nbsp; &quot;Coke's Breakthrough into the Arab World.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 10 April 1978 &nbsp; (p. 40).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; 
               <i>Fortune.</i> &nbsp; &quot;Pepsi for Israel?&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 26 December 1983 &nbsp; (p. 12).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; 
               <i>Fortune.</i> &nbsp; &quot;Pepsico to Enter Israeli Market.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 16 September 1990 &nbsp; (p. 8).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; 
               <i>The Orange County Register.</i> &nbsp; &quot;Briefly Business.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 12 January 1989 &nbsp; (p. E3).
              </nobr>
             </dd> 
             <p></p> 
             <dt>
              <nobr>
               &nbsp; &nbsp; 
               <i>Reuters.</i> &nbsp; &quot;Pepsi Withdraws Evolution Ad Campaign in Israel.&quot;
              </nobr>
             </dt> 
             <dd>
              <nobr>
               &nbsp; &nbsp; 17 May 1992.
              </nobr>
             </dd> 
            </dl> </font> 
           <center>
            <img src="/images/content-divider.gif" />
           </center> 
           <center>
            <center> 
             <script type="text/javascript">
<!--
google_ad_client = "pub-6608306193529351";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_type = "text";
google_ad_channel = "0054321535";
google_color_border = "000000";
//-->
</script> 
             <script type="text/javascript" src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script> 
            </center> 
           </center> </td> 
          <td class="adColumn"> 
           <center> 
            <script type="text/javascript"><!--
    kmn_size = '160x600';
    kmn_placement = 'f829c9263630394d4aa365109f5e0c0c';
//--></script> 
            <script type="text/javascript" src="http://cdn.komoona.com/scripts/kmn_sa.js"></script> 
           </center> </td> 
         </tr> 
        </tbody>
       </table> </td> 
     </tr> 
    </tbody>
   </table> 
   <div class="siteBottom">
     &nbsp; 
   </div> 
  </div> 
  <div class="siteFooterGradient">
   &nbsp;
  </div>   
 </body>
</html>